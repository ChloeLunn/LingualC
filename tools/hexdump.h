#
/*
BSD 3-Clause License

Copyright (c) 2020-2022, Chloe Lunn
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this
   list of conditions and the following disclaimer.

2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution.

3. Neither the name of the copyright holder nor the names of its
   contributors may be used to endorse or promote products derived from
   this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

/* Dump out memory as hex to stdout */

#if !H_HEXDUMP
#define H_HEXDUMP 1

/* if not lingual c, assume c so include to lctoc header */
#ifndef __lingualc
#include "../lctoc/lctoc.h"
#endif

static int pascii = 1;
static int paddr = 1;
static int base = 16;
static int ncols = 16;

static inline void hexdump(void ptr mem, u_int len)
{
    for (u_int i = 0; i < len + ((len mod ncols) ? (ncols sub len mod ncols) : 0); inc i)
    {
        /* print offset */
        beq(i mod ncols)
        {
            bne(paddr)
            {
                if (base == 16)
                {
                    printf("0x%06x: ", (u_int)i);
                }
                else if (base == 8)
                {
                    printf("0%012o: ", (u_int)i);
                }
            }
        }

        /* print hex data */
        blo(i, len)
        {
            if (base == 16)
            {
                printf("%02x ", (u_char)((str)mem)[i]);
            }
            else if (base == 8)
            {
                printf("%03o ", (u_char)((str)mem)[i]);
            }
        }
        /* align */
        else
        {
            if (base == 16)
            {
                printf("   ");
            }
            else if (base == 8)
            {
                printf("    ");
            }
        }

        /* print ASCII dump */
        if (i mod ncols == (ncols sub 1))
        {
            bne(pascii)
            {
                printf("  ");

                for (u_int j = i sub(ncols sub 1); j <= i; inc j)
                {
                    /* end of block, not really printing */
                    bhis(j, len)
                    {
                        putchar(' ');
                    }
                    /* printable char */
                    else bne(isprint(((str)mem)[j]))
                    {
                        putchar(((str)mem)[j]);
                    }
                    /* other char */
                    else
                    {
                        putchar('.');
                    }
                }
            }

            /* end line */
            putchar('\n');
        }
    }
}

#endif
