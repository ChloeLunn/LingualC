	.rodata
STRING0:
	.dc 	"Hello, World!\r\n"
STRING1:
	.dc		"%s"
	.data
; multi line  	made from 	single ; line

; semi-colon
# hash comment
	.text
	.globl	main
	.type	@function
main:
	mov		r0, STRING1	
	mov		r1, STRING0	
	call	printf		
	mov		r0, 0d 		
	call 	exit		
	ret					
