/* THIS FILE GENERATES SOME DATA INTO BINARY FORMAT USING ASSEMBLY */
    .file data.s ; start of file
    .flatpack    ; flat binary
    .align 1     ; align to 1 byte
    
STRING: ; create a string
    .dc "This is a test string\r\n"
    .dl 4
STRING_END:

    ; add the length of the string in another string
    .db "The test string is "
    .db STRING_END
    .dc " bytes long.\r\n"

    ; end file
    .end
