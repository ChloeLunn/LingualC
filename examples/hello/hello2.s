	.flatpack
	/* jump to main */
	mov		pc, main
STRING0:
	.dc 	"Hello, World!\r\n"
STRING1:
	.dc		"%s"

	.globl	main
	.type	@function
main:
	mov  	r0, STRING1	
	mov		r1, STRING0	
	call	printf		
	mov		r0, 0d 		
	call 	exit		
	ret					
