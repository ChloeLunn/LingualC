	.flatpack

	.rodata
STRING0:
	.dc 	"Hello, World!\r\n"

STRING1:
	.dc		"%s"

	.data

; multi line  \
	made from \
	single ; line

/* ANSI */
// C line \
/ UNIX line (this may not be allowed depending on flags!)
; semi-colon
# hash comment

/* multi
line
ansi
comment
*/

	.text

	.globl	main
	.type	@function
main:
	mov  	r0, STRING1	/* put address string 1 into the first argument */
	mov		r1, STRING0	/* put address string 0 into the second argument */
	call	printf		/* call printf with arg stack */
	mov		r0, 0d 		/* clear r0 to 0 (r1 cleared by syscall) */
	call 	exit		/* call exit with argument == 0 */
	ret					/* return if exit came back with error (leave r0 with exit's return value) */
