	.rodata
STRING0:
	.dc	"The constant 'e' is 2 point ..."

	.data
VAR1: /* n */
	.rl		1
VAR2: /* i */
	.rl		1
VAR3: /* c */
	.rl		1
VAR4: /* col */
	.rl		1
VAR5: /* a */
	.rl		1
VAR6: /* ptr to a place in v */
	.rl		1
VAR0: /* v[2000] */
	.rl		2000
	
	.text

	.globl	main
	.type	@function
main:
	/* set v ptr to v */
	mov		VAR6, VAR0 /* move value of label VAR0 into memory address VAR6 */
	/* n = 2000/* */
	mov		VAR1, 2000 /* move value 2000 into memory at VAR1 */
	/* puts("The constant 'e' is 2 point ...")/* */
	mov		r0, STRING0 /* push address STRING0 into arg stack */
	call	puts /* call puts, we use r0 for our return register, but we never actually read it */
	/* col = 0/* */
	mov		VAR4, r1 /* move value 0 into memory address VAR4 */
	/* i = 0/* */
	mov		VAR2, r1

	/* while (i < n) */
WHILE0:
	/* v[i] = 1/* */
	push	[VAR2] /* push value of i onto stack */
	mul		[sp], 4 /* multiply by 4 (size of int) */
	add		[sp], VAR0 /* add the pointer offset */
	pop		VAR6 /* pop back off into pointer */
	mov		[VAR6], 1 /* set value at pointer to 1 */
	/* inc i/* */
	inc		VAR2
	/* (i < n) */
	cmp		[VAR2], [VAR1]
	/* branch if less than */
	blo		WHILE0

	/* while (col < (2 mul n)) */
WHILE1:
	/* a = n add 1/* */
	push	[VAR1]	/* push value of n onto stack */
	inc		[sp]	/* increment value at stack pointer */
	pop		VAR5	/* pop back into a */
	/* i = 0/* */
	mov		VAR2, r1
	/* c = i/* */
	mov		VAR3, [VAR2]

	/* while (i < n) */
WHILE2:
	/* c = c add (v[i] mul 10) */
	push	[VAR2] /* push value of i onto stack */
	mul		[sp], 4 /* multiply by 4 (size of int) */
	add		[sp], VAR0 /* add the pointer offset */
	pop		VAR6 /* pop back off into pointer */
	push	[VAR6] /* pop value at pointer onto stack */
	mul		[sp], 10
	add		[sp], VAR3
	pop		VAR3
	/* v[i] = c mod a */
	push	[VAR3]
	mod		[sp], [VAR5]
	pop		[VAR6]
	/* inc i/* */
	inc		VAR2
	/* c = c div a */
	div		VAR3, [VAR5]
	/* dec a/* */
	dec		VAR5
	/* (i < n) */
	cmp		[VAR2], [VAR1]
	/* branch if less than */
	blo		WHILE2

	/* putchar(c add '0') */
	push	[VAR3]
	add		[sp], '0'
	pop		r0
	call	putchar

	/*inc col/* */
	inc		VAR4

	/* if (not(col mod 5)) */
	push	[VAR4] /* col onto stack */
	mod		[sp], 5 /* mod value by 5 */
	not		[sp] /* logical not */
	beq		ELSE0 /* if 0 (false) leave if */
IF0:
	/* if (col mod 50) */
	push	[VAR4] /* col onto stack */
	mod		[sp], 50 /* mod value by 50 */
	beq		ELSE1 /* if 0 (false) do else */
IF1:
	/* putchar(' ')/* */
	mov		r0, ' '
	call	putchar
	br		ENDIF1
ELSE1:
	/* putchar('\n')/* */
	mov		r0, '\n'
	call	putchar
ENDIF1:
	pop		r1 /* pop old comparison value away */
	clr		r1
	br		ENDIF0
ELSE0:
ENDIF0:
	pop		r1 /* pop old comparison value away */
	clr		r1
	
	/* check on: while (col < (2 mul n)) */
	mov		r2, 2
	mul		r2, [VAR1]
	cmp		VAR4, r2
	blo		WHILE1

	/* putchar('\n')/* */
	mov		r0, '\n'
	call	putchar

	mov		r0,	0

	/* return */
	ret

	.flatpack
