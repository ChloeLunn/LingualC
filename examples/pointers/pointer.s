	.flatpack
	.rodata
STRING0:
	.dc 	"Test 0: (no initialisation) \r\n"
STRING1:
	.dc		"    a = %lu, b = %lu, c = %lu\r\n"
STRING2:
	.dc		"    adr a = %lu, adr b = %lu, val c = %lu\r\n"
STRING3:
	.dc		"Test 1: (a = 0; b = 0) \r\n"
STRING4:
	.dc		"Test 2: (c = adr a) \r\n"
STRING5:
	.dc		"Test 3: (a = 5; b = 6) \r\n"
STRING6:
	.dc		"Test 4: (b += a) \r\n"
STRING7:
	.dc		"Test 6: (a += b) \r\n"
STRING8:
	.dc		"Test 7: (a = adr b; b = adr c) \r\n"

	.data
VAR0:
	.rl		1
VAR1:
	.rl		1
VAR2:
	.rl		1

	.text

	.globl	main
main:
	; printf("Test 0: (no initialisation) \r\n");
	mov		r0,	STRING0 ; put the address of STRING0 onto the argument stack
	call	printf  ; 
	; printf("    a = %lu, b = %lu, c = %lu\r\n", a, b, c);
	mov		r0, STRING1
	mov		r1,	[VAR0] ; [] means value in VAR0, not VAR0 as address
	mov		r2,	[VAR1]
	mov		r3,	[VAR2]
	call	printf
	; printf("    adr a = %lu, adr b = %lu, val c = %lu\r\n", adr a, adr b, val c);
	mov		r0, STRING2
	mov		r1,	VAR0
	mov		r2, VAR1
	push	[VAR2]
	mov		r3,	[sp]
	dec		sp
	call	printf
	; a = 0;
	mov		VAR0, 0
	; b = 0;
	mov		VAR1, 0
	; printf("Test 1: (a = 0; b = 0) \r\n");
	mov		r0,	STRING3
	call	printf
	; printf("    a = %lu, b = %lu, c = %lu\r\n", a, b, c);
	mov		r0,	STRING1
	mov		r1,	[VAR0]
	mov		r2,	[VAR1]
	mov		r3,	[VAR2]
	call	printf
	; printf("    adr a = %lu, adr b = %lu, val c = %lu\r\n", adr a, adr b, val c);
	mov		r0,	STRING2
	mov		r1,	VAR0
	mov		r2,	VAR1
	push	[VAR2]
	mov		r3,	[sp]
	dec		sp
	call	printf
	; c = adr a;
	mov		VAR2, VAR0
	; printf("Test 2: (c = adr a) \r\n");
	mov		r0,	STRING4
	call	printf
	; printf("    a = %lu, b = %lu, c = %lu\r\n", a, b, c);
	mov		r0,	STRING1
	mov		r1,	[VAR0]
	mov		r2,	[VAR1]
	mov		r3,	[VAR2]
	call	printf
	; printf("    adr a = %lu, adr b = %lu, val c = %lu\r\n", adr a, adr b, val c);
	mov		r0,	STRING2
	mov		r1,	VAR0
	mov		r2,	VAR1
	push	[VAR2]
	mov		r3,	[sp]
	dec		sp
	call	printf
	; a = 5;
	mov		VAR0, 5
	; b = 6;
	mov		VAR1, 6
	; printf("Test 3: (a = 5; b = 6) \r\n");
	mov		r0,	STRING5
	call	printf
	; printf("    a = %lu, b = %lu, c = %lu\r\n", a, b, c);
	mov		r0,	STRING1
	mov		r1,	[VAR0]
	mov		r2,	[VAR1]
	mov		r3,	[VAR2]
	call	printf
	; printf("    adr a = %lu, adr b = %lu, val c = %lu\r\n", adr a, adr b, val c);
	mov		r0,	STRING2
	mov		r1,	VAR0
	mov		r2,	VAR1
	push	[VAR2]
	mov		r3,	[sp]
	dec		sp
	call	printf
	; b = b add a;
	push	[VAR1]
	add		[VAR0], [sp]
	dec		sp
	; printf("Test 4: (b += a) \r\n");
	mov		r0,	STRING6
	call	printf
	; printf("    a = %lu, b = %lu, c = %lu\r\n", a, b, c);
	mov		r0,	STRING1
	mov		r1,	[VAR0]
	mov		r2,	[VAR1]
	mov		r3,	[VAR2]
	call	printf
	; printf("    adr a = %lu, adr b = %lu, val c = %lu\r\n", adr a, adr b, val c);

	mov		r0,	STRING2
	mov		r1,	VAR0
	mov		r2,	VAR1
	push	[VAR2]
	mov		r3,	[sp]
	dec		sp
	call	printf
	; a = a add b;
	push	[VAR0]
	add		[VAR1], [sp]
	dec		sp
	; printf("Test 6: (a += b) \r\n");
	mov		r0,	STRING7
	call	printf
	; printf("    a = %lu, b = %lu, c = %lu\r\n", a, b, c);
	mov		r0,	STRING1
	mov		r1,	[VAR0]
	mov		r2,	[VAR1]
	mov		r3,	[VAR2]
	call	printf
	; printf("    adr a = %lu, adr b = %lu, val c = %lu\r\n", adr a, adr b, val c);
	mov		r0,	STRING2
	mov		r1,	VAR0
	mov		r2,	VAR1
	push	[VAR2]
	mov		r3,	[sp]
	dec		sp
	call	printf
	; a = adr b;
	mov		VAR0, VAR1
	; b = adr c;
	mov		VAR1, VAR2
	; printf("Test 7: (a = adr b; b = adr c) \r\n");
	mov		r0,	STRING8
	call	printf
	; printf("    a = %lu, b = %lu, c = %lu\r\n", a, b, c);
	mov		r0,	STRING1
	mov		r1,	[VAR0]
	mov		r2,	[VAR1]
	mov		r3,	[VAR2]
	call	printf
	; printf("    adr a = %lu, adr b = %lu, val c = %lu\r\n", adr a, adr b, val c);
	mov		r0,	STRING2
	mov		r1,	VAR0
	mov		r2,	VAR1
	push	[VAR2]
	mov		r3,	[sp]
	dec		sp
	call	printf
	; call exit
	mov		r0,	0
	call 	exit

	; return (we likely already left with exit, but just in case
	mov		r0, 0
	ret		
