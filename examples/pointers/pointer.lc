/*
BSD 3-Clause License

Copyright (c) 2020-2022, Chloe Lunn
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this
   list of conditions and the following disclaimer.

2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution.

3. Neither the name of the copyright holder nor the names of its
   contributors may be used to endorse or promote products derived from
   this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

/* Pointer testing */

/* if not lingual c, assume c so include to lctoc header */
#ifndef __lingualc
#include "../../lctoc/lctoc.h"
#endif

int main(int argc, str ptr argv)
{
    /* create some big ints */
    local big a;
    local big b;

    /* create a pointer */
    local int ptr c;

    printf("Test 0: (no initialisation) \r\n");
    printf("    a = %llu, b = %llu, c = %llu\r\n", a, b, c);
    printf("    adr a = %lu, adr b = %lu, val c = %lu\r\n", adr a, adr b, val c);

    a = 0;
    b = 0;

    printf("Test 1: (a = 0; b = 0) \r\n");
    printf("    a = %lu, b = %lu, c = %lu\r\n", a, b, c);
    printf("    adr a = %lu, adr b = %lu, val c = %lu\r\n", adr a, adr b, val c);

    /* set ptr c to adr of a */
    c = adr a;

    printf("Test 2: (c = adr a) \r\n");
    printf("    a = %lu, b = %lu, c = %lu\r\n", a, b, c);
    printf("    adr a = %lu, adr b = %lu, val c = %lu\r\n", adr a, adr b, val c);

    a = 5;
    b = 6;

    printf("Test 3: (a = 5; b = 6) \r\n");
    printf("    a = %lu, b = %lu, c = %lu\r\n", a, b, c);
    printf("    adr a = %lu, adr b = %lu, val c = %lu\r\n", adr a, adr b, val c);

    b = b add a;

    printf("Test 4: (b += a) \r\n");
    printf("    a = %lu, b = %lu, c = %lu\r\n", a, b, c);
    printf("    adr a = %lu, adr b = %lu, val c = %lu\r\n", adr a, adr b, val c);

    a = a add b;

    printf("Test 6: (a += b) \r\n");
    printf("    a = %lu, b = %lu, c = %lu\r\n", a, b, c);
    printf("    adr a = %lu, adr b = %lu, val c = %lu\r\n", adr a, adr b, val c);

    a = adr b;
    b = adr c;

    printf("Test 7: (a = adr b; b = adr c) \r\n");
    printf("    a = %lu, b = %lu, c = %lu\r\n", a, b, c);
    printf("    adr a = %lu, adr b = %lu, val c = %lu\r\n", adr a, adr b, val c);

    return 0;
}