# Lingual C Programming Tools

The Lingual C Programming tools are primarily designed around the use of the new C dialect I've called Lingual C.

This is a hybrid between explicit inline assembly names and classic C programming. The goal is to produce portable C code, with implicit typing, instruction, and memory control for fast, lightweight and simple compiling and assembling.

It follows the UNIX-like principles of trying to keep each part of the code parsing and build until the very end machine binary output as separate programs which can be run independently, be ported to other languages or platforms, or be chained to appear as a single process (aka a build tool-chain).

This includes the Lingual C Compiler (LCC), Lingual Pre-Processor (LPP), Lingual Assembler (LAS), and Lingual Library Linker (LLL) along with a few other 'helper' programs.

The ultimate goal is to produce portable, self-hosting build tools that can not only target the Lingual C language, LCVM, and my personal OS development, but also to become viable options for a simple cross-platform compiler and tool-chain. But the current target is to finish off an assembler and linker which can produce code to run on the Lingual C Virtual Machine (LCVM) to allow a cross-platform userspace.

---

<img src="./media/Lingual_C.png" align="left" width="12%" height="12%">

Lingual C is the focus of this project, and a lot of the code here is written in Lingual C for the purpose of eventually becoming self-hosting. However at the moment projects are compiled using the default CC compiler on any *NIX system and through a header which includes some basic macros and defines for doing Lingual C to Classic C translation.

There is a start of a converter program, but it currently doesn't do much or even work that well...

Lingual C is essentially Classic C (C99-style) but with different keywords, types, and the operations are written out to avoid over-use of symbol tokens. 

For example, instead of '*' doing four different things in classic C, it does none in Lingual C, we instead have descriptive word-based operators.

Full information can be found in [./lcc/Readme.md](./lcc/Readme.md)

---

<img src="./media/LCVM.png" align="left" width="12%" height="12%"> 

The LCVM is the Lingual C Virtual Machine. It is a simple virtual 32-bit processor which will eventually tap into the LCC and LAS projects to allow executing Lingual C as JIT programs. 

It also has very simple instructions and processing, and can call the standard system calls of your system thus allows programs compiled to run on the LCVM instead of natively to run on any machine which implements the LCVM and the system calls required. 

The LCVM was designed to run on top of an operating system I've been writing that will be used on hardware incapable of some of the User/Kernel split modes and task switching required for any kind of multitasking, multiuser OS. The OS will be kernel only, with userspace implementations running on the LCVM (at least for now, maybe I'll port it to something a bit more multitasking friendly later).

So that the LCVM can run its code on any machine offers me the option of debugging code on a desktop, before running it on a dev board.

---

<img src="./media/LAS.png" align="left" width="12%" height="12%">

LAS is an assembler primarily targeting the LCVM for use in the process of Lingual C compiling as the Lingual C Compiler (LCC) will output this assembly dialect for compilation to LCVM machine code.

The project is however looking increasingly portable to a wide variety of platforms, so is kind of it's own thing. The biggest challenge with portability will be making sure that the binary files outputted will work with not just LCVM and my own linkers, but others should it be needed. 

This is *not* however a goal of the project, so for now it should be assumed that they will not.

More information can be found in [./las/Readme.md](./las/Readme.md) and the related documents.

 ---

 ## Simple Examples:

### Hello World

#### In Lingual C:
```
int main(int argc, str ptr argv)
{
    local str hello_string = "Hello, World!\r\n";
    printf("%s", hello_string);
    exit(0);
}
```

This likely looks very familiar to anyone who has programmed in classic C, the big difference is the removal of symbols/tokens to denote various combinations of pointers and types, and the introduction of the str and ptr keywords.

The "local" keyword here is a compiler hint that tells it that the symbol is included in the file and local to the code block only. 
In this case it won't make much difference, however if it is a numerical value that would be changed, then it would mean that it would try to keep the number in registers or stack instead of putting it in data or bss sections.

#### In LAS assembly:
```
    .rodata
STRING1:
    .dc  "Hello, World!\r\n"
STRING0:
    .dc  "%s"

    .text
    .globl  main
    .type   @function
main:
    mov   r0, STRING0
    mov   r1, STRING1
    call  printf     
    mov   r0, #0
    call  exit       
```

Note that the call exit and printf are shown by name and not syscall number. This allows the assembler to find the system call number upon compile rather than hard-code it into the compiler, it also means that if the call does not go to the system calls list, that the compiler can then try to link it to a library or function within the program, replacing the call with a jsr operation.
