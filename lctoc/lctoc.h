/*
BSD 3-Clause License

Copyright (c) 2020-2022, Chloe Lunn
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this
   list of conditions and the following disclaimer.

2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution.

3. Neither the name of the copyright holder nor the names of its
   contributors may be used to endorse or promote products derived from
   this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

/* Header to allow you to write Lingual C and parse/compile with C
   It's a little janky, because obviously there are differences
   but it kinda works */

#if !H_LCTOC && !defined(__lingualc)
#define H_LCTOC 1

#ifdef __APPLE__
#include <TargetConditionals.h>
#endif

#include <ctype.h>
#include <errno.h>
#include <fcntl.h>
#include <signal.h>
#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#if !defined(_WIN32) && !defined(WIN32)
#include <termios.h>
#include <unistd.h>
#endif

/* ~~~~ TYPES ~~~~ */

/* ptr type */
#define ptr *  // NOLINT

/* str */
#define str char*

/* char */
#define char char

/* unsigned integer */
#define small short

/* standard integer type */
#define int int

/* double-size integer type */
#define big long long

/* unsigned char */
#define u_char unsigned char

/* unsigned integer */
#define u_small unsigned short

/* unsigned integer */
#define u_int unsigned int

/* double-size integer type */
#define u_big unsigned long long

/* ~~~~ TYPE MODIFIERS ~~~~ */

/* var should be stored in a register */
#define register register

/* var should be local */
#define local

/* var is in external source */
#define ext extern

/* var is in a library */
#define lib extern

/* var is in the OS */
#define sys extern

/* get address of var */
#define adr &  // NOLINT

/* get value at ptr */
#define val *  // NOLINT

/* ~~~~ SPECIAL CONSTANTS ~~~~ */

#ifndef true
#define true 1
#endif

#ifndef false
#define false 0
#endif

/* ~~~~ LINGUAL OPERATORS ~~~~ */

/* maths */
#define add +   // NOLINT
#define sub -   // NOLINT
#define div /   // NOLINT
#define mod %   // NOLINT
#define mul *   // NOLINT
#define inc ++  // NOLINT
#define dec --  // NOLINT
#define asl <<  // NOLINT
#define asr >>  // NOLINT

/* bitwise */
#define not !   // NOLINT
#define inv ~   // NOLINT
#define and &   // NOLINT
#define or |    // NOLINT
#define xor ^   // NOLINT
#define bsl <<  // NOLINT
#define bsr >>  // NOLINT

/* Branches (single op) */
#define bne(x) if ((x) != 0)
#define beq(x) if ((x) == 0)
#define bge(x) if ((x) == 0 || (x) > 0)
#define blt(x) if ((x) < 0 && (x) != 0)
#define bgt(x) if ((x) > 0 && (x) != 0)
#define ble(x) if ((x) < 0 || (x) == 0)
#define bpl(x) if ((x) > 0)
#define bmi(x) if ((x) < 0)

/* Branches (two op) */
#define bhi(x, y)  if ((x) > (y))
#define blos(x, y) if ((x) <= (y))
#define blo(x, y)  if ((x) < (y))
#define bhis(x, y) if ((x) >= (y))

#define mov(x, y) (x) = (y)
#define clr(x)    (x) = 0

#define neg(x) (-(x))

/* just for completeness... */
#define else else
#define endif

/* We are now lingual C compatible! So define it */
#define __lingualc

#endif
