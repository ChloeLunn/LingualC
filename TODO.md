1. Write LAS assembler (to print binary dump for now)
2. Test assembler and use output in LCVM testbin.c
3. Generate ELF from assembler
4. Create ELF loader for LCVM
5. Test that program still runs correctly
6. Create dynamic  linker (and LID) 
7. Finish LCVM instructions
8. Write test program that tests instructions and syscalls
9. Finish more syscall wrappers
10. Write LCC compiler (output LCVM assembly only for no)
11. Write static linker
12. More testing
13. Create some userspace apps in LingualC
14. Duplicate Vcore into V.OS along with supporting programs
15. Check that tools made above can be self-hosting and run in LCVM (except LCVM)
16. Add floating points
