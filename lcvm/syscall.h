/*
BSD 3-Clause License

Copyright (c) 2020-2022, Chloe Lunn
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this
   list of conditions and the following disclaimer.

2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution.

3. Neither the name of the copyright holder nor the names of its
   contributors may be used to endorse or promote products derived from
   this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

/* LCVM Calls from code */

#if !H_LCVM_CALLS
#define H_LCVM_CALLS 1

#include <stddef.h>

/* do a system call from virtual registers */
int __syscall(void);

int sys_printf(void);
int sys_exit(void);

#define CALLABLE (1 << 0)
#define UNEXPECT (1 << 1)
#define ERROR    (1 << 2)

/*
 * Function flags:
 * AOK can be called and can be expected to hang around
 * OLD can be called but may have unexpected behaviour and may be removed at any time
 * OBS can be called and will have expected behaviour, but may be removed at any time
 * REM cannot be called, the function has been removed and will cause an error
 * NOF cannot be called, the function hasn't been implemented and will cause an error
 * Any other value then there will be an error
 */
#define AOK (CALLABLE)            /* function is current and can be called */
#define OLD (CALLABLE | UNEXPECT) /* function is old call number */
#define OBS (CALLABLE)            /* function is obsolete call number */
#define REM (ERROR)               /* function has been removed */
#define NOF (ERROR)               /* function wasn't implemented */

struct sysent {
    int s_asize;        /* max number of expected args (size in in ints) */
    int s_rsize;        /* function returns (size in ints) */
    int s_stat;         /* ABI implementation status */
    const char* s_name; /* name of call (used for compile and error reporting) */
    int (*s_fun)(void); /* function to handle call */
};

extern const struct sysent sysent[];
extern const int nsysent;

#define SYSCALL_INT_VEC 0x80
#define SYSCALL_REG_NUM 7

#endif
