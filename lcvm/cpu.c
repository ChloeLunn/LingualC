/*
BSD 3-Clause License

Copyright (c) 2020-2022, Chloe Lunn
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this
   list of conditions and the following disclaimer.

2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution.

3. Neither the name of the copyright holder nor the names of its
   contributors may be used to endorse or promote products derived from
   this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#include "cpu.h"

#include "itab.h"
#include "mach.h"
#include "syscall.h"
#include "vcore.h"

#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>

/* ~~~~~~ READ/WRITE THE BUS BITS ~~~~~~ */

/* read a 8-bit number from the data space */
int dwrite8(uint32_t addr, uint8_t value)
{
    return (int)!!((*(uint8_t*)&vmem.dspace[addr]) = value);
}

/* read a 8-bit number from the data space */
uint8_t dread8(uint32_t addr)
{
    unsigned int r = (*(uint8_t*)&vmem.dspace[addr]);
    if (DEBUG_RWFETCH)
        printf("dread8: a = %i, r = 0x%02x\r\n", addr, r);
    return r;
}

/* read a 16-bit number from the data space */
int dwrite16(uint32_t addr, uint16_t value)
{
    return (int)!!((*(uint16_t*)&vmem.dspace[addr]) = value);
}

/* read a 16-bit number from the data space */
uint16_t dread16(uint32_t addr)
{
    unsigned int r = (*(uint16_t*)&vmem.dspace[addr]);
    if (DEBUG_RWFETCH)
        printf("dread16: a = %i, r = 0x%04x\r\n", addr, r);
    return r;
}

/* read a 32-bit number from the data space */
int dwrite32(uint32_t addr, uint32_t value)
{
    return (int)!!((*(uint32_t*)&vmem.dspace[addr]) = value);
}

/* read a 32-bit number from the data space */
uint32_t dread32(uint32_t addr)
{
    unsigned int r = (*(uint32_t*)&vmem.dspace[addr]);
    if (DEBUG_RWFETCH)
        printf("dread32: a = %i, r = 0x%08x\r\n", addr, r);
    return r;
}

/* read a 64-bit number from the data space */
int dwrite64(uint32_t addr, uint64_t value)
{
    return (int)!!((*(uint64_t*)&vmem.dspace[addr]) = value);
}

/* read a 64-bit number from the data space */
uint64_t dread64(uint32_t addr)
{
    unsigned long long r = (*(uint64_t*)&vmem.dspace[addr]);
    if (DEBUG_RWFETCH)
        printf("dread64: a = %i, r = 0x%016llx\r\n", addr, r);
    return r;
}

/* grab the next 16-bit number from the instructions space */
uint8_t iread8()
{
    uint8_t r = (*(uint8_t*)&vmem.ispace[vcpu.c_pc]);
    if (DEBUG_RWFETCH)
        printf("iread8: pc = %i, r = 0x%02x\r\n", vcpu.c_pc, r);
    vcpu.c_pc += 1;
    return r;
}

/* grab the next 16-bit number from the instructions space */
uint16_t iread16()
{
    uint16_t r = (*(uint16_t*)&vmem.ispace[vcpu.c_pc]);
    if (DEBUG_RWFETCH)
        printf("iread16: pc = %i, r = 0x%04x\r\n", vcpu.c_pc, r);
    vcpu.c_pc += 2;
    return r;
}

/* grab the next 32-bit number from the instructions space */
uint32_t iread32()
{
    uint32_t r = (*(uint32_t*)&vmem.ispace[vcpu.c_pc]);
    if (DEBUG_RWFETCH)
        printf("iread32: pc = %i, r = 0x%08x\r\n", vcpu.c_pc, r);
    vcpu.c_pc += 4;
    return r;
}

/* grab the next 64-bit number from the instructions space */
uint64_t iread64()
{
    uint64_t r = (*(uint64_t*)&vmem.ispace[vcpu.c_pc]);
    if (DEBUG_RWFETCH)
#ifdef __GNUC__
        printf("iread64: pc = %i, r = 0x%016lx\r\n", vcpu.c_pc, r);
#else
        printf("iread64: pc = %i, r = 0x%016llx\r\n", vcpu.c_pc, r);
#endif
    vcpu.c_pc += 8;
    return r;
}

/* ~~~~~~ STATUS BIT PROCESSING BITS ~~~~~~ */

/* set/clear the negative bit */
int setN(bool st)
{
    if (st)
    {
        vcpu.c_psr |= psr_negative;
    }
    else
    {
        vcpu.c_psr &= ~psr_negative;
    }
    return 0;
}

/* set/clear the zero bit */
int setZ(bool st)
{
    if (st)
    {
        vcpu.c_psr |= psr_zero;
    }
    else
    {
        vcpu.c_psr &= ~psr_zero;
    }
    return 0;
}

/* set/clear the carry bit */
int setC(bool st)
{
    if (st)
    {
        vcpu.c_psr |= psr_carry;
    }
    else
    {
        vcpu.c_psr &= ~psr_carry;
    }
    return 0;
}

/* set/clear the overflow bit */
int setV(bool st)
{
    if (st)
    {
        vcpu.c_psr |= psr_overflow;
    }
    else
    {
        vcpu.c_psr &= ~psr_overflow;
    }
    return 0;
}

/* get the negative bit */
bool getN()
{
    return !!(vcpu.c_psr & psr_negative);
}

/* get the zero bit */
bool getZ()
{
    return !!(vcpu.c_psr & psr_zero);
}

/* get the carry bit */
bool getC()
{
    return !!(vcpu.c_psr & psr_carry);
}

/* get the overflow bit */
bool getV()
{
    return !!(vcpu.c_psr & psr_overflow);
}

/* ~~~~~~ ACTUAL INSTRUCTION PROCESSING BITS ~~~~~~ */

/* test value */
int TST(const uint32_t instr)
{
    uint32_t dv = 0;
    grab_args(instr, &dv, 0);

    setN(dv & TOP_BIT);
    setZ(dv == 0);
    setV(false);
    setC(false);
    return 0;
}

/* addition */
int ADD(const uint32_t instr)
{
    uint32_t da = 0;
    uint32_t sv = 0;
    grab_args(instr, &da, &sv);

    uint32_t dv = dread32(da);
    uint32_t rv = (dv + sv);

    dwrite32(da, rv);

    setN(rv & TOP_BIT);
    setZ(rv == 0);
    setV(!((dv ^ sv) & TOP_BIT) && ((sv ^ rv) & TOP_BIT));
    setC(((uint64_t)dv + (uint64_t)sv) >= (uint64_t)UINT32_MAX);
    return 0;
}

/* subtraction */
int SUB(const uint32_t instr)
{
    uint32_t da = 0;
    uint32_t sv = 0;
    grab_args(instr, &da, &sv);

    uint32_t dv = dread32(da);
    uint32_t rv = (dv - sv);

    dwrite32(da, rv);

    setN(rv & TOP_BIT);
    setZ(rv == 0);
    setV(((dv ^ sv) & TOP_BIT) && !((sv ^ rv) & TOP_BIT));
    setC(((int64_t)dv > (int64_t)sv));
    return 0;
}

/* division */
int DIV(const uint32_t instr)
{
    uint32_t da = 0;
    uint32_t sv = 0;
    grab_args(instr, &da, &sv);

    uint32_t dv = dread32(da);

    // if attempted div by 0
    if (sv == 0)
    {
        setZ(false);
        setV(false);
        setN(false);
        setC(true);
        return 0;
    }

    // if div too int64_t
    if (((int64_t)dv / (int64_t)sv) > (int64_t)UINT32_MAX)
    {
        setZ(false);
        setV(true);
        setN(false);
        setC(false);
        return 0;
    }

    uint32_t rv = (dv / sv);

    dwrite32(da, rv);

    setZ(rv == 0);
    setN(dv & TOP_BIT);
    setV(dv == 0);
    setC(false);
    return 0;
}

/* modulo */
int MOD(const uint32_t instr)
{
    uint32_t da = 0;
    uint32_t sv = 0;
    grab_args(instr, &da, &sv);

    uint32_t dv = dread32(da);

    // if attempted mod by 0
    if (sv == 0)
    {
        setZ(false);
        setV(false);
        setN(false);
        setC(true);
        return 0;
    }

    // if div too big
    if (((int64_t)dv % (int64_t)sv) > (int64_t)UINT32_MAX)
    {
        setZ(false);
        setV(true);
        setN(false);
        setC(false);
        return 0;
    }

    uint32_t rv = (dv % sv);

    dwrite32(da, rv);

    setZ(rv == 0);
    setN(dv & TOP_BIT);
    setV(dv == 0);
    setC(false);
    return 0;
}

/* multiplication */
int MUL(const uint32_t instr)
{
    uint32_t da = 0;
    uint32_t sv = 0;
    grab_args(instr, &da, &sv);

    uint32_t dv = dread32(da);
    uint32_t rv = (dv * sv);

    dwrite32(da, rv);

    setN(rv & TOP_BIT);
    setZ(rv == 0);
    setV(false);
    setC(((uint64_t)dv * (uint64_t)sv) >= (uint64_t)UINT32_MAX);
    return 0;
}

/* increment */
int INC(const uint32_t instr)
{
    uint32_t da = 0;
    grab_args(instr, &da, 0);

    uint32_t dv = dread32(da);
    uint32_t rv = (dv + 1);

    dwrite32(da, rv);

    setN(rv & TOP_BIT);
    setZ(rv == 0);
    setV(rv & TOP_BIT);
    setC(dv == UINT32_MAX);
    return 0;
}

/* decrement */
int DEC(const uint32_t instr)
{
    uint32_t da = 0;
    grab_args(instr, &da, 0);

    uint32_t dv = dread32(da);
    uint32_t rv = (dv - 1);

    dwrite32(da, rv);

    setN(rv & TOP_BIT);
    setZ(rv == 0);
    setV(rv == (UINT32_MAX & ~TOP_BIT));
    setC(dv == 0);
    return 0;
}

/* arithmetic shift */
int ASH(const uint32_t instr)
{
    uint32_t da = 0;
    int32_t sv = 0;
    grab_args(instr, &da, (uint32_t*)&sv);

    uint32_t dv = dread32(da);

    uint32_t rv = 0;
    /* shift left */
    if (sv > 0)
    {
        setV((dv ^ (dv << 1)) & TOP_BIT);
        setC(dv & TOP_BIT);

        rv = (dv << sv);
    }
    /* arith shfit right */
    else
    {
        setV((dv & TOP_BIT) ^ (dv & 1));
        setC(dv & 1);

        int32_t s = -1UL * (dv >> 31);
        rv = ((s ^ dv) >> ((sv * -1) ^ s));
    }

    dwrite32(da, rv);

    setN(rv & TOP_BIT);
    setZ(rv == 0);
    return 0;
}

/* arithmetic shift right by 1 */
int ASR(const uint32_t instr)
{
    uint32_t da = 0;
    grab_args(instr, &da, 0);

    uint32_t dv = dread32(da);

    setC(dv & 1);
    setN(dv & TOP_BIT);
    setV((dv & TOP_BIT) ^ (dv & 1));

    int32_t s = -1UL * (dv >> 31);
    uint32_t rv = ((s ^ dv) >> (1 ^ s));

    dwrite32(da, rv);

    setZ(rv == 0);
    return 0;
}

/* arithmetic shift left by 1 */
int ASL(const uint32_t instr)
{
    uint32_t da = 0;
    grab_args(instr, &da, 0);

    uint32_t dv = dread32(da);

    setC(dv & TOP_BIT);
    setN(dv & (TOP_BIT >> 1));
    setV((dv ^ (dv << 1)) & TOP_BIT);

    uint32_t rv = (dv << 1);

    dwrite32(da, rv);

    setZ(rv == 0);
    return 0;
}

/* 1s compliment */
int COM(const uint32_t instr)
{
    uint32_t da = 0;
    grab_args(instr, &da, 0);
    uint32_t dv = dread32(da);
    uint32_t rv = dv ^ UINT32_MAX;

    dwrite32(da, rv);

    setC(true);
    setN(rv & TOP_BIT);
    setZ(false);
    setV(false);
    return 0;
}

/* Negate - 2s compliment */
int NEG(const uint32_t instr)
{
    uint32_t da = 0;
    grab_args(instr, &da, 0);
    uint32_t dv = dread32(da);
    int32_t rv = (dv * -1) & UINT32_MAX;

    dwrite32(da, rv);

    setC(true);
    setN(rv & TOP_BIT);
    setZ(false);
    setV(false);
    return 0;
}

/* add carry */
int ADC(const uint32_t instr)
{
    uint32_t da = 0;
    grab_args(instr, &da, 0);
    uint32_t dv = dread32(da);
    uint32_t rv = dv + 1;

    if (vcpu.c_psr & psr_carry)
    {
        setN(rv & TOP_BIT);
        setZ(rv == 0);
        setV(dv == (UINT32_MAX & ~TOP_BIT));
        setC(dv == UINT32_MAX);
        dwrite32(da, rv);
    }
    else
    {
        setN(dv & TOP_BIT);
        setZ(dv == 0);
        setV(false);
        setC(false);
    }
    return 0;
}

/* subtract carry */
int SBC(const uint32_t instr)
{
    uint32_t da = 0;
    grab_args(instr, &da, 0);
    int32_t dv = dread32(da);
    int32_t rv = dv - 1;

    if (vcpu.c_psr & psr_carry)
    {
        setN(rv & TOP_BIT);
        setZ(rv == 0);
        setC(dv);
        setV((unsigned int)dv == TOP_BIT);
        dwrite32(da, rv);
    }
    else
    {
        setN(dv & TOP_BIT);
        setZ(dv == 0);
        setV((unsigned int)dv == TOP_BIT);
        setC(true);
    }
    return 0;
}

/* swap byte endianness */
int SWAB(const uint32_t instr)
{
    uint32_t da = 0;
    grab_args(instr, &da, 0);
    uint32_t dv = dread32(da);
    uint32_t rv = ((((dv)&0xFF) << 24) | (((dv)&0xFF00) << 8) | (((dv)&0xFF0000) >> 8) | (((dv)&0xFF000000) >> 24));
    dwrite32(da, rv);
    setN(false);
    setZ(false);
    setV(false);
    setC(false);
    return 0;
}

/* logical not */
int NOT(const uint32_t instr)
{
    uint32_t da = 0;
    grab_args(instr, &da, 0);
    uint32_t dv = dread32(da);
    uint32_t rv = (dv ? 0 : 1);
    dwrite32(da, rv);
    setN(false);
    setZ(rv == 0);
    setV(false);
    setC(false);
    return 0;
}

/* bitwise invert */
int INV(const uint32_t instr)
{
    uint32_t da = 0;
    grab_args(instr, &da, 0);
    uint32_t dv = dread32(da);
    uint32_t rv = ~dv;
    dwrite32(da, rv);
    setN(rv & TOP_BIT);
    setZ(rv == 0);
    setV(false);
    setC(false);
    return 0;
}

/* Bitwise AND */
int BIT(const uint32_t instr)
{
    uint32_t da = 0;
    uint32_t sv = 0;
    grab_args(instr, &da, &sv);
    uint32_t dv = dread32(da);
    uint32_t rv = (dv & sv);

    dwrite32(da, rv);

    setN(rv & TOP_BIT);
    setZ(rv == 0);
    setV(false);
    setC(false);
    return 0;
}

/* Bitwise OR */
int BIS(const uint32_t instr)
{
    uint32_t da = 0;
    uint32_t sv = 0;
    grab_args(instr, &da, &sv);
    uint32_t dv = dread32(da);
    uint32_t rv = (dv | sv);

    dwrite32(da, rv);

    setN(rv & TOP_BIT);
    setZ(rv == 0);
    setV(false);
    setC(false);
    return 0;
}

/* Bitwise XOR */
int XOR(const uint32_t instr)
{
    uint32_t da = 0;
    uint32_t sv = 0;
    grab_args(instr, &da, &sv);
    uint32_t dv = dread32(da);
    uint32_t rv = (dv ^ sv);

    dwrite32(da, rv);

    setN(rv & TOP_BIT);
    setZ(rv == 0);
    setV(false);
    setC(false);
    return 0;
}

/* Bitwise shift left */
int BSL(const uint32_t instr)
{
    uint32_t da = 0;
    uint32_t sv = 0;
    grab_args(instr, &da, &sv);
    uint32_t dv = dread32(da);
    uint32_t rv = (dv << sv);

    dwrite32(da, rv);

    setZ(rv == 0);
    setC(dv & TOP_BIT);
    setN(dv & (TOP_BIT >> 1));
    setV((dv ^ (dv << 1)) & TOP_BIT);
    return 0;
}

/* Bitwise shift right */
int BSR(const uint32_t instr)
{
    uint32_t da = 0;
    uint32_t sv = 0;
    grab_args(instr, &da, &sv);
    uint32_t dv = dread32(da);
    uint32_t rv = (dv);

    while (sv > 0)
    {
        rv = rv >> 1;
        rv &= ~TOP_BIT;
        --sv;
    }

    dwrite32(da, rv);

    setZ(rv == 0);
    setC(dv & 1);
    setN(dv & TOP_BIT);
    setV((dv & TOP_BIT) ^ (dv & 1));
    return 0;
}

/* Bitwise clear (AND INV) */
int BIC(const uint32_t instr)
{
    uint32_t da = 0;
    uint32_t sv = 0;
    grab_args(instr, &da, &sv);
    uint32_t dv = dread32(da);
    uint32_t rv = (dv & ~sv);

    dwrite32(da, rv);

    setN(rv & TOP_BIT);
    setZ(rv == 0);
    setV(false);
    setC(false);
    return 0;
}

/* Clear */
int CLR(const uint32_t instr)
{
    uint32_t da = 0;
    grab_args(instr, &da, 0);

    uint32_t d = getD(instr);
    /* value is an argument in vram */
    if (!isArg(d))
    {
        vcpu.c_reg[d & ARG_BITS] = 0;
    }
    else
    {
        dwrite32(da, 0);
    }

    setN(false);
    setZ(true);
    setV(false);
    setC(false);
    return 0;
}

/* Move */
int MOV(const uint32_t instr)
{
    uint32_t da = 0;
    uint32_t sv = 0;
    grab_args(instr, &da, &sv);

    uint32_t d = getD(instr);
    /* value is an argument in vram */
    if (!isArg(d))
    {
        vcpu.c_reg[d & ARG_BITS] = sv;
    }
    else
    {
        dwrite32(da, sv);
    }

    setN(sv & TOP_BIT);
    setZ(sv == 0);
    setV(false);
    setC(true);
    return 0;
}

/* Move byte (8-bit) */
int MOVB(const uint32_t instr)
{
    uint32_t da = 0;
    uint32_t sv = 0;
    grab_args(instr, &da, &sv);
    uint32_t rv = sv & 0xFF;
    dwrite32(da, rv);

    setN(rv & 0x80);
    setZ(rv == 0);
    setV(false);
    setC(true);
    return 0;
}

/* Move short (16-bit) */
int MOVS(const uint32_t instr)
{
    uint32_t da = 0;
    uint32_t sv = 0;
    grab_args(instr, &da, &sv);
    uint32_t rv = sv & 0xFFFF;
    dwrite32(da, rv);

    setN(rv & 0x8000);
    setZ(rv == 0);
    setV(false);
    setC(true);
    return 0;
}

/* Move Long (32-bit) */
int MOVL(const uint32_t instr)
{
    uint32_t da = 0;
    uint32_t sv = 0;
    grab_args(instr, &da, &sv);
    dwrite32(da, sv);

    setN(sv & TOP_BIT);
    setZ(sv == 0);
    setV(false);
    setC(true);
    return 0;
}

/* Move quad (64-bit) */
int MOVQ(const uint32_t instr)
{
    uint32_t da = 0;
    uint32_t sv = 0;
    grab_args(instr, &da, &sv);

    /* grab second half of 64-bit */
    uint32_t s = getS(instr);
    uint32_t sa = 0;
    uint32_t sv2 = 0;

    /* value is an argument in vram */
    if (isArg(s))
    {
        /* get destination as arg */
        sa = iread32();
    }
    /* must be in a register */
    else
    {
        sa = vcpu.c_reg[s & ARG_BITS];
    }

    /* if it is a pointer, get it from an address instead */
    if (isPtr(s))
    {
        sv2 = dread32(sa);
    }
    /* otherwise value is just the arg */
    else
    {
        sv2 = sa;
    }

    dwrite32(da, sv);
    dwrite32(da + 4, sv2);

    setN(sv & TOP_BIT);
    setZ(sv == 0 && sv2 == 0);
    setV(false);
    setC(true);
    return 0;
}

#include <setjmp.h>
extern jmp_buf sbuf;
int CALL(const unsigned int instr)
{
    uint32_t dv = 0;
    grab_args(instr, &dv, 0);
    vcpu.c_reg[SYSCALL_REG_NUM] = dv;
    longjmp(sbuf, SYSCALL_INT_VEC);
    return 0;
}
