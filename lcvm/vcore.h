/*
BSD 3-Clause License

Copyright (c) 2022, Chloe Lunn
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this
   list of conditions and the following disclaimer.

2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution.

3. Neither the name of the copyright holder nor the names of its
   contributors may be used to endorse or promote products derived from
   this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#if !H_VCORE
#define H_VORE 1

/* debug flags */
#define DEBUG_REG     0
#define DEBUG_MEM     0
#define DEBUG_INSTR   0
#define DEBUG_CPU     0
#define DEBUG_SYSCALL 0
#define DEBUG_RWFETCH 0

#include "mach.h"

/* used for save/recall of the processor state */
typedef LTYPE label_t[LSIZE];

/* virtual processor cpu */
extern struct cpu vcpu;

/* virtual processor fpu */
extern struct fpu vfpu;

/* virtual memory */
extern struct mem vmem;

/* print control registers */
void print_creg();

/* print regular registers */
void print_reg();

/* print the current memory */
void print_mem(unsigned int isize, unsigned int dsize);

/* panic the processor (quit) */
int vpanic();

/* clear the vmem */
int vclear();

/* reset the cpu */
int vreset();

/* step the virtual machine */
int vstep();

/* save the virtual registers */
int vsave(label_t* rsave, int v);

/* restore the virtual registers */
int vrestore(label_t* rsave);

/* swap the core memory */
int vswap(unsigned char* dst, const unsigned char* src, unsigned int dlen, unsigned int slen);

/* processor cycle loop */
int vloop();

/* mark processor as run */
void vrun();
/* mark processor as halt */
void vhalt();
/* mark processor as halt */
void vwait();
#endif
