/*
BSD 3-Clause License

Copyright (c) 2020-2022, Chloe Lunn
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this
   list of conditions and the following disclaimer.

2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution.

3. Neither the name of the copyright holder nor the names of its
   contributors may be used to endorse or promote products derived from
   this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

/* LCVM Instructions/Operations */

/* this file is portable */

#include "itab.h"

#include "cpu.h"
#include "mach.h"
#include "syscall.h"
#include "vcore.h"

#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>

/* grab the argument values from the instruction and addresses */
int grab_args(uint32_t instr, uint32_t* dv, uint32_t* sv)
{
    if (dv != 0)
    {
        uint32_t d = getD(instr);
        uint32_t da = 0;

        /* value is an argument in vram */
        if (isArg(d))
        {
            /* get destination as arg */
            da = iread32();
        }
        /* must be in a register */
        else
        {
            da = vcpu.c_reg[d & ARG_BITS];
        }

        /* if it is a pointer, get it from an address instead */
        if (isPtr(d))
        {
            *dv = dread32(da);
        }
        /* otherwise value is just the  */
        else
        {
            *dv = da;
        }
    }

    if (sv != 0)
    {
        uint32_t s = getS(instr);
        uint32_t sa = 0;

        /* value is an argument in vram */
        if (isArg(s))
        {
            /* get destination as arg */
            sa = iread32();
        }
        /* must be in a register */
        else
        {
            sa = vcpu.c_reg[s & ARG_BITS];
        }

        /* if it is a pointer, get it from an address instead */
        if (isPtr(s))
        {
            *sv = dread32(sa);
        }
        /* otherwise value is just the arg */
        else
        {
            *sv = sa;
        }
    }
    return 0;
}

/* ~~~~~~ RESOLVE AND PROCESS AN INSTRUCTION ~~~~~~ */

int resolve_instruction()
{
    /* load the current instruction */
    vcpu.c_cir = iread32();

    /* for each instruction */
    for (int i = 0; i < NITAB; i++)
    {
        /* if the instruction matches the current instruction */
        if ((itab[i].i_inst & INSTR_BITS) == (vcpu.c_cir & INSTR_BITS))
        {
            /* call that function and return it's error code */
            if (itab[i].i_func)
            {
                if (DEBUG_INSTR)
                    printf("instr: %s (0x%08x)\r\n", itab[i].i_name, vcpu.c_cir);
                return itab[i].i_func(vcpu.c_cir);
            }
            else
            {
                if (DEBUG_INSTR)
                    printf("instr: not found (0x%08x)\r\n", vcpu.c_cir);
                return 1;
            }
        }
    }
}

/* ~~~~~~ DECODE THE OPERAND TRANSPORT LAYER ~~~~~~ */

int decode_trans(uint32_t instr, struct operand* dv, struct operand* sv, struct operand* rv)
{
    if (dv != 0)
    {
        uint32_t d = getD(instr);

        dv->o_val = 0;
        dv->o_size = getL(instr);

        /* value is an argument in vram */
        if (isArg(d))
        {
            dv->o_trans.s_type = SMEM;
            dv->o_val = iread32();
        }
        /* must be in a register */
        else
        {
            dv->o_trans.s_type = SREG;
            dv->o_val = d & ARG_BITS;
        }

        /* if it is a pointer, get it from an address instead */
        if (isPtr(d))
        {
            dv->o_trans.s_type |= SPTR;
        }
    }

    if (sv != 0)
    {
        uint32_t s = getS(instr);

        sv->o_val = 0;
        sv->o_size = getL(instr);

        /* value is an argument in vram */
        if (isArg(s))
        {
            sv->o_trans.s_type = SMEM;
            dv->o_val = iread32();
        }
        /* must be in a register */
        else
        {
            sv->o_trans.s_type = SREG;
            sv->o_val = s & ARG_BITS;
        }

        /* if it is a pointer, get it from an address instead */
        if (isPtr(s))
        {
            sv->o_trans.s_type |= SPTR;
        }
    }

    return 0;
}
