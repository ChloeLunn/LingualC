/*
BSD 3-Clause License

Copyright (c) 2020-2022, Chloe Lunn
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this
   list of conditions and the following disclaimer.

2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution.

3. Neither the name of the copyright holder nor the names of its
   contributors may be used to endorse or promote products derived from
   this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

/* LCVM Instructions/Operations */

/* this file is portable */

#if !H_ITAB
#define H_ITAB 1

#include "cpu.h"

struct itab {
    const char* i_name;          /* string name of instruction */
    unsigned int i_inst;         /* actual instruction machine code mask */
    unsigned char i_nargs;       /* max number of further 32-bit arguments */
    int (*i_func)(unsigned int); /* function to call for this instruction */
};

/* Op codes are in the format 0x0FCLDDSS where:
 *    - F is the op code family
 *    - C is the op code within that family
 *    - L is the result size in bytes (not implemented)
 *    - SS is the source register number
 *    - DD the destination register number
 * If the first nibble is not 0, then that means the instruction is invalid and the processor will panic
 * If the destination register is 7F (i.e. all value bits high), use the next 32-bit value from the memory as the operand
 * If the source register is 7F (i.e. all value bits high), use the next 32-bit value from the memory as the value
 * If the top bit of the destination (DD) is set, take the value in the operand as a pointer to the actual destination data
 * If the top bit of the source (SS) is set, take the value in the operand as a pointer to the actual source data
 * If the operation only requires a single operand, then it is DD and SS is ignored
 */

#define INSTR_SIZE sizeof(unsigned int)

#define INSTR_BITS  0xFFF00000UL
#define SIZE_BITS   0x000F0000UL
#define DEST_BITS   0x0000FF00UL
#define SOURCE_BITS 0x000000FFUL
#define PTR_BITS    0x00000080UL
#define ARG_BITS    0x0000007FUL
#define STAT_BITS   0x0000000FUL
#define TOP_BIT     0x80000000UL
#define BOTTOM_BIT  0x00000001UL
#define ZERO_BITS   0x00000000UL
#define ALL_BITS    0xFFFFFFFFUL

#define NITAB 66

static const struct itab itab[] = {
  /* asm, op code,    nargs */

  /* maths */
  {"add", 0x00100000, 2}, /* addition               D = Address, S = value */
  {"sub", 0x00200000, 2}, /* subtraction            D = Address, S = value */
  {"div", 0x00300000, 2}, /* divide                 D = Address, S = value */
  {"mod", 0x00400000, 2}, /* modulo                 D = Address, S = value */
  {"mul", 0x00500000, 2}, /* multiply               D = Address, S = value */
  {"inc", 0x00600000, 1}, /* increment by 1         D = Address */
  {"dec", 0x00700000, 1}, /* decrement by 1         D = Address */
  {"ash", 0x00800000, 2}, /* arithmetic shift       D = Address, S = value  */
  {"asl", 0x00900000, 1}, /* arithmetic shift left by 1 */
  {"asr", 0x00A00000, 1}, /* arithmetic shift right by 1*/
  {"com", 0x00B00000, 1}, /* 1s compliment */
  {"neg", 0x00C00000, 1}, /* make neg (2s compliment) */
  {"adc", 0x00D00000, 2}, /* addition with carry */
  {"sbc", 0x00E00000, 2}, /* subtraction with carry */
  {"swb", 0x00F00000, 1}, /* swap bytes (endianness swapper) */

  /* bitwise */
  {"not", 0x01000000, 1}, /* if value is 0, set to 1, if value is !=0 set to 0 */
  {"inv", 0x01100000, 1}, /* bitwise invert */
  {"bit", 0x01200000, 2}, /* bitwise AND */
  {"bis", 0x01300000, 2}, /* bitwise OR */
  {"xor", 0x01400000, 2}, /* bitwise XOR */
  {"bsl", 0x01500000, 2}, /* bit shift left */
  {"bsr", 0x01600000, 2}, /* bit shift right */
  {"bic", 0x01700000, 2}, /* bit clear (AND ~VAL)*/

  /* data handling */
  {"clr", 0x02000000, 1, &CLR}, /* clear                            D = address */
  {"mov", 0x02100000, 2, &MOV}, /* move native (32-bit)             D = address, S = value */
  {"movb", 0x02200000, 2},      /* move byte (8-bit) */
  {"movs", 0x02300000, 2},      /* move short (16-bit) */
  {"movl", 0x02400000, 2},      /* move long (32-bit) */
  {"movq", 0x02500000, 2},      /* move quad (64-bit) - puts lsb in argument, and msb in argument + 32-bits */
  {"push", 0x02600000, 1},      /* push value to stack              D = value */
  {"pop", 0x02700000, 1},       /* pop value from stack             D = address */

  /* Branches */
  {"br", 0x03000000, 1},   /* branch always                     D = address */
  {"b", 0x03000000, 1},    /* branch always (arm-like alias)    D = address */
  {"bne", 0x03100000, 1},  /* branch if not zero                D = address */
  {"beq", 0x03300000, 1},  /* branch if zero                    D = address */
  {"bge", 0x03400000, 1},  /* branch if postive or 0 */
  {"blt", 0x03500000, 1},  /* branch if negative and not 0 */
  {"bgt", 0x03600000, 1},  /* branch if positive and not 0 */
  {"ble", 0x03700000, 1},  /* branch if negative or 0 */
  {"bpl", 0x03800000, 1},  /* branch if positive */
  {"bmi", 0x03900000, 1},  /* branch if negative */
  {"bcs", 0x03A00000, 1},  /* branch if carry is set */
  {"bcc", 0x03B00000, 1},  /* branch if carry is clear */
  {"bvs", 0x03C00000, 1},  /* branch if overflow is set */
  {"bvc", 0x03D00000, 1},  /* branch if overflow is clear */
  {"bhi", 0x03E00000, 1},  /* branch if higher */
  {"blos", 0x03F00000, 1}, /* branch if lower or same */
  {"blo", 0x03A00000, 1},  /* branch if lower - alias to bcs */
  {"bhis", 0x03B00000, 1}, /* branch if higher or same - alias to bcc */

  /* Jumps */
  {"jmp", 0x04000000, 1}, /* jump to anywhere */
  {"jsr", 0x04100000, 2}, /* jump to subroutine */
  {"rts", 0x04200000, 1}, /* return to subroutine */

  /* External calls */
  {"call", 0x05000000, 1, &CALL}, /* call a function, D = func num */
  {"int", 0x05100000, 1},         /* call an interrupt  D = vector */

  /* Comparisions */
  {"cmp", 0x06000000, 2}, /* compare two values    D = Value, S = Value */
  {"tst", 0x06100000, 1}, /* test a value  D = Value */

  /* Floating points */
  {"setd", 0x07000000, 0}, /* set double mode */
  {"setf", 0x07100000, 0}, /* set float mode */
  {"movf", 0x07200000, 2}, /* move float (assumes registers 0-4 are floating point registers 0-4 */
  {"ftoi", 0x07300000, 2},

  /* Processor Control */
  {"wfi", 0x00000000, 0},  /* wait for interrupt/instruction (do nothing), sets run to 0 */
  {"rst", 0x0FB00000, 0},  /* reset processor */
  {"ret", 0x0FC00000, 0},  /* return from execution */
  {"halt", 0x0FD00000, 0}, /* halt processor */
  {"unop", 0x0FE00000, 0}, /* no operation or invalid operation */
  {"nop", 0x0FF00000, 0},  /* no operation, but valid operation */
};

#define getS(i)  (((i)&SOURCE_BITS) >> 0)     /* get source byte */
#define getD(i)  (((i)&DEST_BITS) >> 8)       /* get destination byte */
#define getL(i)  (((i)&SIZE_BITS) >> 16)      /* get destination byte */
#define isArg(s) (((s)&ARG_BITS) == ARG_BITS) /* source/dest is in an argument */
#define isPtr(s) (((s)&PTR_BITS))             /* treat source/dest as pointer */

int grab_args(uint32_t instr, uint32_t* dv, uint32_t* sv);

#endif
