/*
BSD 3-Clause License

Copyright (c) 2020-2022, Chloe Lunn
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this
   list of conditions and the following disclaimer.

2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution.

3. Neither the name of the copyright holder nor the names of its
   contributors may be used to endorse or promote products derived from
   this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

/* information pulled from ELF format specification, which can be found here:
 * http://www.skyfree.org/linux/references/ELF_Format.pdf
 *
 * and more recent info on actual use here:
 * https://en.wikipedia.org/wiki/Executable_and_Linkable_Format
 */

/* see https://docs.oracle.com/cd/E23824_01/html/819-0690/glcfv.html for detailed implementation info */

#if !H_ELF
#define H_ELF

#include <stddef.h>

/* put this here temporarily to shut up the linter */
#ifndef __packed
#define __packed
#endif

/* identity info */
struct elf32_ident {
    unsigned char e_magic[4];    /* magic number */
    unsigned char e_class;       /* class (none, 32-bit, 64-bit) */
    unsigned char e_data;        /* data encoding type */
    unsigned char e_version;     /* elf header version */
    unsigned char e_abi;         /* operating system */
    unsigned char e_abi_version; /* abi version number */
    unsigned char e_pad[7];      /* padding to end of identity info */
} __packed;

/* elf file header */
struct elf32_header {
    struct elf32_ident e_ident; /* identity info */
    unsigned short e_type;      /* file type */
    unsigned short e_machine;   /* machine type */
    unsigned int e_version;     /* version number */
    unsigned int e_entry;       /* entry point - where to jmp on program start */
    unsigned int e_phoff;       /* Program header table offset */
    unsigned int e_shoff;       /* Section header table offset */
    unsigned int e_flags;       /* Processor/Machine flags*/
    unsigned short e_ehsize;    /* Size of elf header */
    unsigned short e_phentsize; /* Size of a program header */
    unsigned short e_phnum;     /* Number of program headers */
    unsigned short e_shentsize; /* Size of a Section header*/
    unsigned short e_shnum;     /* Number of section headers */
    unsigned short e_shstrndx;  /* String table index in section header list */
} __packed;

/* e_magic */
static const unsigned char elf32_magic[4] = {0x7F, 'E', 'L', 'F'};

/* e_class */
#define ELF_CLASS_NONE 0 /* unknown/invalid */
#define ELF_CLASS_32   1 /* 32-bit objects */
#define ELF_CLASS_64   2 /* 64-bit objects */

/* e_data */
#define ELF_ENCODE_NONE 0 /* unknown/invalid */
#define ELF_ENCODE_2LSB 1 /* two's complement, little-endian (normal) */
#define ELF_ENCODE_2MSB 2 /* two's complement, big-endian (freak) */

/* e_abi */
#define ELF_OSABI_NONE       0 /* None is SysV */
#define ELF_OSABI_SYSV       0 /* UNIX System V ABI */
#define ELF_OSABI_HPUX       1 /* HP-UX operating system */
#define ELF_OSABI_NETBSD     2 /* NetBSD ABI */
#define ELF_OSABI_LINUX      3 /* Linux ABI */
#define ELF_OSABI_GNU        ELF_OSABI_LINUX /* Gnu Linux */
#define ELF_OSABI_GNUHURD    4 /* Gnu Hurd */
#define ELF_OSABI_SOLARIS    6 /* Solaris ABI */
#define ELF_OSABI_AIX        7 /* AIX */
#define ELF_OSABI_IRIX       8 /* IRIX */
#define ELF_OSABI_FREEBSD    9 /* FreeBSD ABI */
#define ELF_OSABI_TRU64      10 /* Compaq TRU64 UNIX.  */
#define ELF_OSABI_NOVELL     11 /* Novell Modesto.  */
#define ELF_OSABI_OPENBSD    12 /* OpenBSD.  */
#define ELF_OSABI_OPENVMS    13
#define ELF_OSABI_NONSTOP    14
#define ELF_OSABI_AROS       15
#define ELF_OSABI_FENIX      16
#define ELF_OSABI_CLOUD      17
#define ELF_OSABI_OPENVOS    18
#define ELF_OSABI_ARM_AEABI  64 /* ARM EABI */
#define ELF_OSABI_ARM        97 /* ARM */
#define ELF_OSABI_STANDALONE 255 /* Stand-alone (embedded) application */

/* e_abiversion */
#define ELF_ABIVERSION_NONE 0 /* unknown */
/* e_abiversion can be the number of syscalls, feature set, OS version, it's up to the implementation */

/* e_type */
#define ET_NONE   0 /* No file type */
#define ET_REL    1 /* A relocatable file (to be used for static linking) */
#define ET_EXEC   2 /* An executable file (to be executed) */
#define ET_DYN    3 /* A shared object file (to be used for dynamic linking) */
#define ET_CORE   4 /* A core file/dump */
#define ET_LOOS   0xFF00 /* operating system specific */
#define ET_HIOS   0xFFFF /* operating system specific */
#define ET_LOPROC 0xFF00 /* processor specific */
#define ET_HIPROC 0xFFFF /* processor specific */
/* values between ET_LOOS and ET_HIOS are reserved for operating system specific info */
/* values between ET_LOPROC and ET_HIPROC are reserved for processor specific info */

/* e_machine */
#define EM_NONE        0 /* No machine */
#define EM_M32         1 /* AT&T WE 32100 */
#define EM_SPARC       2 /* SPARC */
#define EM_386         3 /* Intel 80386 */
#define EM_68K         4 /* Motorola 68000 */
#define EM_88K         5 /* Motorola 88000 */
#define EM_486         6 /* Intel 80486 */
#define EM_860         7 /* Intel 80860 */
#define EM_MIPS        8 /* MIPS I Architecture */
#define EM_S370        9 /* Amdahl UTS on System/370 */
#define EM_MIPS_RS3_LE 10 /* MIPS RS3000 Little-endian */
#define EM_RS6000      11 /* IBM RS/6000 XXX reserved */
#define EM_PARISC      14 /* Hewlett-Packard PA-RISC */
#define EM_NCUBE       16 /* NCube XXX reserved */
#define EM_VPP500      17 /* Fujitsu VPP500 */
#define EM_SPARC32PLUS 18 /* Enhanced instruction set SPARC */
#define EM_960         19 /* Intel 80960 */
#define EM_PPC         20 /* PowerPC */
#define EM_PPC64       21 /* PowerPC 64-bit */
#define EM_S390        22 /* S390 */
#define EM_SPU         23 /* IBM SPU/SPC */
#define EM_V800        36 /* NEC V800 */
#define EM_FR20        37 /* Fujitsu FR20 */
#define EM_RH32        38 /* TRW RH-32 */
#define EM_RCE         39 /* Motorola RCE */
#define EM_ARM         40 /* ARM (up to ARMv7/aarch32) */
#define EM_ALPHA       41 /* DEC Alpha */
#define EM_SH          42 /* Hitachi Super-H */
#define EM_SPARCV9     43 /* SPARC Version 9 */
#define EM_TRICORE     44 /* Siemens Tricore */
#define EM_ARC         45 /* Argonaut RISC Core */
#define EM_H8_300      46 /* Hitachi H8/300 */
#define EM_H8_300H     47 /* Hitachi H8/300H */
#define EM_H8S         48 /* Hitachi H8S */
#define EM_H8_500      49 /* Hitachi H8/500 */
#define EM_IA_64       50 /* Intel IA-64 */
#define EM_MIPS_X      51 /* Stanford MIPS-X */
#define EM_COLDFIRE    52 /* Motorola Coldfire */
#define EM_68HC12      53 /* Motorola MC68HC12 */
#define EM_MMA         54 /* Fujitsu MMA */
#define EM_PCP         55 /* Seimens PCP */
#define EM_NCPU        56 /* Sony nCPU RISC */
#define EM_NDR1        57 /* Denso NDR1 */
#define EM_STAR        58 /* Motorola Star*Core */
#define EM_ME16        59 /* Toyota ME16 */
#define EM_ST100       60 /* STM ST100 */
#define EM_TINYJ       61 /* Advanced Logic Corp TinyJ */
#define EM_AMD64       62 /* AMD x86-64 */
#define EM_PDSP        63 /* Sony DSP Processor */
#define EM_FX66        66 /* Siemens FX66 microcontroller */
#define EM_ST9PLUS     67 /* STMicroelectronics ST9+ 8/16 mc */
#define EM_ST7         68 /* STMicroelectronics ST7 8 bit mc */
#define EM_68HC16      69 /* Motorola MC68HC16 microcontroller */
#define EM_68HC11      70 /* Motorola MC68HC11 microcontroller */
#define EM_68HC08      71 /* Motorola MC68HC08 microcontroller */
#define EM_68HC05      72 /* Motorola MC68HC05 microcontroller */
#define EM_SVX         73 /* Silicon Graphics SVx */
#define EM_ST19        74 /* STMicroelectronics ST19 8 bit mc */
#define EM_VAX         75 /* DEC VAX */
#define EM_CRIS        76 /* Axis Communications 32-bit embedded processor */
#define EM_JAVELIN     77 /* Infineon Technologies 32-bit embedded processor */
#define EM_FIREPATH    78 /* Element 14 64-bit DSP Processor */
#define EM_ZSP         79 /* LSI Logic 16-bit DSP Processor */
#define EM_MMIX        80 /* Donald Knuth's educational 64-bit processor */
#define EM_HUANY       81 /* Harvard University machine-independent object files */
#define EM_PRISM       82 /* SiTera Prism */
#define EM_AVR         83 /* Atmel AVR 8-bit microcontroller */
#define EM_FR30        84 /* Fujitsu FR30 */
#define EM_D10V        85 /* Mitsubishi D10V */
#define EM_D30V        86 /* Mitsubishi D30V */
#define EM_V850        87 /* NEC v850 */
#define EM_M32R        88 /* Mitsubishi M32R */
#define EM_MN10300     89 /* Matsushita MN10300 */
#define EM_MN10200     90 /* Matsushita MN10200 */
#define EM_PJ          91 /* picoJava */
#define EM_OPENRISC    92 /* OpenRISC 32-bit embedded processor */
#define EM_ARC_A5      93 /* ARC Cores Tangent-A5 */
#define EM_XTENSA      94 /* Tensilica Xtensa Architecture */
#define EM_TMS320      140 /* TI TMS320C6000 */
#define EM_ELBRUS      175 /* MCST Elbrus e2k */
#define EM_ARM64       183 /* ARM 64-bit (ARMv8/aarch64) */
#define EM_TILEPRO     188 /* Tilera TILEPro */
#define EM_TILEGX      191 /* Tilera TILE-Gx */
#define EM_RISCV       243 /* RISC V */
#define EM_BPF         247 /* Berkeley Packet Filter */
#define EM_65C816      257 /* WDC 65C816 */

/* If it is necessary to assign new unofficial EM_* values, please
   pick large random numbers (0x8523, 0xa7f2, etc.) to minimize the
   chances of collision with official or non-GNU unofficial values.  */
#define EM_ALPHA 0x9026
#define EM_C60   0x9c60

#define EM_MINIRISC 0x4D52 /* MiniRisc */
#define EM_Z80      0x5A80 /* Zilog Z80 */
#define EM_LCVM     0x4C43 /* Lingual C Virtual Machine Byte Code */

#define EM_PDP11 0x4949

/* e_version */
#define EV_NONE    0 /* invalid version */
#define EV_CURRENT 1 /* version 1 (current) */

/* program header  */
struct elf32_pheader {
    unsigned int p_type;   /* entry type */
    unsigned int p_offset; /* file offset */
    unsigned int p_vaddr;  /* virtual address */
    unsigned int p_paddr;  /* physical address (reserved, 0) */
    unsigned int p_filesz; /* file size of segment (may be 0) */
    unsigned int p_memsz;  /* memory size of segment (may be 0) */
    unsigned int p_flags;  /* flags */
    unsigned int p_align;  /* memory & file alignment */
} __packed;

/* p_type */
#define PT_NULL    0 /* Program header table entry unused */
#define PT_LOAD    1 /* Loadable program segment */
#define PT_DYNAMIC 2 /* Dynamic linking information */
#define PT_INTERP  3 /* Program interpreter */
#define PT_NOTE    4 /* Auxiliary information */
#define PT_SHLIB   5 /* Reserved, unspecified semantics */
#define PT_PHDR    6 /* Entry for header table itself */
#define PT_TLS     7 /* Thread-local storage template */
#define PT_LOOS    0x60000000 /* Start of OS-specific semantics */
#define PT_HIOS    0x6fffffff /* end of OS-specific semantics */
#define PT_LOPROC  0x70000000 /* Start of processor-specific semantics */
#define PT_HIPROC  0x7fffffff /* end of processor-specific semantics */

/* p_flags */
#define PF_R 0x4 /* Segment is readable */
#define PF_W 0x2 /* Segment is writable */
#define PF_X 0x1 /* Segment is executable */
/* A text segment commonly have PF_X|PF_R, a data segment PF_X|PF_W|PF_R */

/* section header */
struct elf32_sheader {
    unsigned int sh_name;      /* section name (index into strings table) */
    unsigned int sh_type;      /* section type */
    unsigned int sh_flags;     /* section flags */
    unsigned int sh_addr;      /* virtual address */
    unsigned int sh_offset;    /* file offset */
    unsigned int sh_size;      /* section size */
    unsigned int sh_link;      /* link to another */
    unsigned int sh_info;      /* misc info */
    unsigned int sh_addralign; /* memory alignment */
    unsigned int sh_entsize;   /* table entry size */
} __packed;

/* sh_type */
#define SHT_NULL          0 /* inactive */
#define SHT_PROGBITS      1 /* program defined contents */
#define SHT_SYMTAB        2 /* holds symbol table */
#define SHT_STRTAB        3 /* holds string table */
#define SHT_RELA          4 /* holds relocation info with explicit addends */
#define SHT_HASH          5 /* holds symbol hash table */
#define SHT_DYNAMIC       6 /* holds dynamic linking information */
#define SHT_NOTE          7 /* holds information marking */
#define SHT_NOBITS        8 /* holds a section that does not occupy space */
#define SHT_REL           9 /* holds relocation info without explicit addends */
#define SHT_SHLIB         10 /* reserved with unspecified semantics */
#define SHT_DYNSYM        11 /* holds a minimal set of dynamic linking symbols */
#define SHT_INIT_ARRAY    14 /* constructors */
#define SHT_FINI_ARRAY    15 /* deconstructors */
#define SHT_PREINIT_ARRAY 16 /* pre-constructors */
#define SHT_GROUP         17 /* section group */
#define SHT_SYMTAB_SHNDX  18 /* extended section indices */
#define SHT_NUM           19 /* num of defined types */
#define SHT_LOOS          0x60000000 /* Operating system specific range */
#define SHT_HIOS          0x6fffffff /* Operating system specific range */
#define SHT_LOPROC        0x70000000 /* Processor-specific range */
#define SHT_HIPROC        0x7fffffff /* Processor-specific range */
#define SHT_LOUSER        0x80000000 /* Application-specific range */
#define SHT_HIUSER        0xffffffff /* Application-specific range */

/* sh_flags */
#define SHF_WRITE            0x00000001 /* Section contains writable data */
#define SHF_ALLOC            0x00000002 /* Section occupies memory */
#define SHF_EXECINSTR        0x00000004 /* Section contains executable insns */
#define SHF_MERGE            0x00000010 /* might be merged */
#define SHF_STRINGS          0x00000020 /* contains null-terminated strings */
#define SHF_INFO_LINK        0x00000040 /* sh_info containts the SHT index */
#define SHF_LINK_ORDER       0x00000080 /* preserved order after combining */
#define SHF_OS_NONCONFORMING 0x00000100 /* non-standard os-specific handling required */
#define SHF_GROUP            0x00000200 /* section is a member of a group */
#define SHF_TLS              0x00000400 /* thread-local data */
#define SHF_ORDERED          0x04000000 /* special ordering requirement */
#define SHF_EXCLUDE          0x08000000 /* exclude unless referenced or allocated */
#define SHF_MASKOS           0x0FF00000 /* Operating system specific values */
#define SHF_MASKPROC         0xF0000000 /* Processor-specific values */

/* symbol table */
struct elf32_symtab {
    unsigned int st_name;    /* Symbol name (.symtab index) */
    unsigned int st_value;   /* value of symbol */
    unsigned int st_size;    /* size of symbol */
    unsigned char st_info;   /* type / binding attrs */
    unsigned char st_other;  /* unused */
    unsigned short st_shndx; /* section index of symbol */
} __packed;

/* Symbol Table index of the undefined symbol */
#define ELF_SYM_UNDEFINED 0

/* st_info: Symbol Bindings */
#define STB_LOCAL  0 /* local symbol */
#define STB_GLOBAL 1 /* global symbol */
#define STB_WEAK   2 /* weakly defined global symbol */
#define STB_LOOS   10 /* Operating system specific range */
#define STB_HIOS   12
#define STB_LOPROC 13 /* Processor-specific range */
#define STB_HIPROC 15

/* st_info: Symbol Types */
#define STT_NOTYPE         0 /* Type not specified */
#define STT_OBJECT         1 /* Associated with a data object */
#define STT_FUNC           2 /* Associated with a function */
#define STT_SECTION        3 /* Associated with a section */
#define STT_FILE           4 /* Associated with a file name */
#define STT_COMMON         5
#define STT_TLS            6
#define STT_LOOS           10 /* Operating system specific range */
#define STT_HIOS           12
#define STT_LOPROC         13 /* Processor-specific range */
#define STT_VMACH_REGISTER 13
#define STT_HIPROC         15

/* st_other: Register symbols (symbols that init global registers) */
#define STO_VMACH_REG_0    0x00
#define STO_VMACH_REG_1    0x01
#define STO_VMACH_REG_2    0x02
#define STO_VMACH_REG_3    0x03
#define STO_VMACH_REG_4    0x04
#define STO_VMACH_REG_5    0x05
#define STO_VMACH_REG_6    0x06
#define STO_VMACH_REG_7    0x07
#define STO_VMACH_REG_8    0x08
#define STO_VMACH_REG_9    0x09
#define STO_VMACH_REG_10   0x0A
#define STO_VMACH_REG_11   0x0B
#define STO_VMACH_REG_12   0x0C
#define STO_VMACH_REG_SP   0x0D
#define STO_VMACH_REG_LR   0x0E
#define STO_VMACH_REG_PC   0x0F
#define STO_VMACH_REG_PSR  0x10
#define STO_VMACH_REG_IMR  0x11
#define STO_VMACH_REG_CTRL 0x12

/* st_info utility macros */
#define ELF_ST_BIND(info)       ((unsigned int)(info) >> 4)
#define ELF_ST_TYPE(info)       ((unsigned int)(info)&0xf)
#define ELF_ST_INFO(bind, type) ((unsigned char)(((bind) << 4) | ((type)&0xf)))

/*
 * Special section indexes
 */
#define SHN_UNDEF 0 /* Undefined section */

#define SHN_LORESERVE 0xff00 /* Start of Reserved range */
#define SHN_ABS       0xfff1 /*  Absolute symbols */
#define SHN_COMMON    0xfff2 /*  Common symbols */
#define SHN_HIRESERVE 0xffff

#define SHN_LOPROC 0xff00 /* Start of Processor-specific range */
#define SHN_HIPROC 0xff1f
#define SHN_LOOS   0xff20 /* Operating system specific range */
#define SHN_HIOS   0xff3f

#define SHN_MIPS_ACOMMON 0xff00
#define SHN_MIPS_TEXT    0xff01
#define SHN_MIPS_DATA    0xff02
#define SHN_MIPS_SCOMMON 0xff03

/*
 * Relocation Entries
 */
struct elf32_rel {
    unsigned int r_offset; /* where to do it */
    unsigned int r_info;   /* index & type of relocation */
} __packed;

struct elf32_rela {
    unsigned int r_offset; /* where to do it */
    unsigned int r_info;   /* index & type of relocation */
    int r_addend;          /* adjustment value */
} __packed;

/* r_info utility macros */
#define ELF_R_SYM(info)       ((info) >> 8)
#define ELF_R_TYPE(info)      ((info)&0xff)
#define ELF_R_INFO(sym, type) (((sym) << 8) + (unsigned char)(type))

/*
 * Dynamic Section structure array
 */
struct elf32_dyn {
    unsigned int d_tag; /* entry tag value */
    union {
        unsigned int d_ptr;
        unsigned int d_val;
    } d_un;
} __packed;

/* d_tag */
#define DT_NULL         0 /* Marks end of dynamic array */
#define DT_NEEDED       1 /* Name of needed library (DT_STRTAB offset) */
#define DT_PLTRELSZ     2 /* Size, in bytes, of relocations in PLT */
#define DT_PLTGOT       3 /* Address of PLT and/or GOT */
#define DT_HASH         4 /* Address of symbol hash table */
#define DT_STRTAB       5 /* Address of string table */
#define DT_SYMTAB       6 /* Address of symbol table */
#define DT_RELA         7 /* Address of Rela relocation table */
#define DT_RELASZ       8 /* Size, in bytes, of DT_RELA table */
#define DT_RELAENT      9 /* Size, in bytes, of one DT_RELA entry */
#define DT_STRSZ        10 /* Size, in bytes, of DT_STRTAB table */
#define DT_SYMENT       11 /* Size, in bytes, of one DT_SYMTAB entry */
#define DT_INIT         12 /* Address of initialization function */
#define DT_FINI         13 /* Address of termination function */
#define DT_SONAME       14 /* Shared object name (DT_STRTAB offset) */
#define DT_RPATH        15 /* Library search path (DT_STRTAB offset) */
#define DT_SYMBOLIC     16 /* Start symbol search within local object */
#define DT_REL          17 /* Address of Rel relocation table */
#define DT_RELSZ        18 /* Size, in bytes, of DT_REL table */
#define DT_RELENT       19 /* Size, in bytes, of one DT_REL entry */
#define DT_PLTREL       20 /* Type of PLT relocation entries */
#define DT_DEBUG        21 /* Used for debugging; unspecified */
#define DT_TEXTREL      22 /* Relocations might modify non-writable seg */
#define DT_JMPREL       23 /* Address of relocations associated with PLT */
#define DT_BIND_NOW     24 /* Process all relocations at load-time */
#define DT_INIT_ARRAY   25 /* Address of initialization function array */
#define DT_FINI_ARRAY   26 /* Size, in bytes, of DT_INIT_ARRAY array */
#define DT_INIT_ARRAYSZ 27 /* Address of termination function array */
#define DT_FINI_ARRAYSZ 28 /* Size, in bytes, of DT_FINI_ARRAY array*/
#define DT_NUM          29

#define DT_LOOS   0x60000000 /* Operating system specific range */
#define DT_HIOS   0x6fffffff
#define DT_LOPROC 0x70000000 /* Processor-specific range */
#define DT_HIPROC 0x7fffffff

/*
 * Auxiliary Vectors
 */
struct elf_auxinfo {
    unsigned int a_type; /* 32-bit id */
    unsigned int a_v;    /* 32-bit id */
} __packed;

/* a_type */
#define AT_NULL        0 /* Marks end of array */
#define AT_IGNORE      1 /* No meaning, a_un is undefined */
#define AT_EXECFD      2 /* Open file descriptor of object file */
#define AT_PHDR        3 /* &phdr[0] */
#define AT_PHENT       4 /* sizeof(phdr[0]) */
#define AT_PHNUM       5 /* # phdr entries */
#define AT_PAGESZ      6 /* PAGESIZE */
#define AT_BASE        7 /* Interpreter base addr */
#define AT_FLAGS       8 /* Processor flags */
#define AT_ENTRY       9 /* Entry address of executable */
#define AT_DCACHEBSIZE 10 /* Data cache block size */
#define AT_ICACHEBSIZE 11 /* Instruction cache block size */
#define AT_UCACHEBSIZE 12 /* Unified cache block size */

/* Vendor specific */
#define AT_MIPS_NOTELF 10 /* XXX a_val != 0 -> MIPS XCOFF executable */

#define AT_SUN_UID  2000 /* euid */
#define AT_SUN_RUID 2001 /* ruid */
#define AT_SUN_GID  2002 /* egid */
#define AT_SUN_RGID 2003 /* rgid */

/* Solaris kernel specific */
#define AT_SUN_LDELF   2004 /* dynamic linker's ELF header */
#define AT_SUN_LDSHDR  2005 /* dynamic linker's section header */
#define AT_SUN_LDNAME  2006 /* dynamic linker's name */
#define AT_SUN_LPGSIZE 2007 /* large pagesize */

/* Other information */
#define AT_SUN_PLATFORM 2008 /* sysinfo(SI_PLATFORM) */
#define AT_SUN_HWCAP    2009 /* process hardware capabilities */
#define AT_SUN_IFLUSH   2010 /* do we need to flush the instruction cache? */
#define AT_SUN_CPU      2011 /* cpu name */
/* ibcs2 emulation band aid */
#define AT_SUN_EMUL_ENTRY  2012 /* coff entry point */
#define AT_SUN_EMUL_EXECFD 2013 /* coff file descriptor */
/* Executable's fully resolved name */
#define AT_SUN_EXECNAME 2014

/* Note Headers */
struct elf_nhdr {
    unsigned int n_namesz;
    unsigned int n_descsz;
    unsigned int n_type;
} __packed;

#define ELF_NOTE_TYPE_OSVERSION 1

/* NetBSD-specific note type: Emulation name.  desc is emul name string. */
#define ELF_NOTE_NETBSD_TYPE_EMULNAME 2
/* NetBSD-specific note name and description sizes */
#define ELF_NOTE_NETBSD_NAMESZ 7
#define ELF_NOTE_NETBSD_DESCSZ 4
/* NetBSD-specific note name */
#define ELF_NOTE_NETBSD_NAME "NetBSD\0\0"
/* Solaris notes  */
#define ELF_NOTE_SOLARIS "SUNW Solaris"
/* GNU-specific note name and description sizes */
#define ELF_NOTE_GNU_NAMESZ 4
#define ELF_NOTE_GNU_DESCSZ 4
/* GNU-specific note name */
#define ELF_NOTE_GNU_NAME "GNU\0"

/* GNU-specific OS/version value stuff */
#define ELF_NOTE_GNU_OSMASK  (unsigned int)0xff000000
#define ELF_NOTE_GNU_OSLINUX (unsigned int)0x01000000
#define ELF_NOTE_GNU_OSMACH  (unsigned int)0x00000000

#define ELF_NOTE_OS_LINUX    0
#define ELF_NOTE_OS_GNU      1
#define ELF_NOTE_OS_SOLARIS2 2
#define ELF_NOTE_OS_FREEBSD  3

#define ELF_AUX_ENTRIES 8 /* Size of aux array passed to loader */

struct elf_args {
    unsigned int arg_entry;     /* program entry point */
    unsigned int arg_interp;    /* Interpreter load address */
    unsigned int arg_phaddr;    /* program header address */
    unsigned int arg_phentsize; /* Size of program header */
    unsigned int arg_phnum;     /* Number of program headers */
} __packed;

#endif
