/*
BSD 3-Clause License

Copyright (c) 2020-2022, Chloe Lunn
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this
   list of conditions and the following disclaimer.

2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution.

3. Neither the name of the copyright holder nor the names of its
   contributors may be used to endorse or promote products derived from
   this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#include "syscall.h"

#include "testbin.h"
#include "vcore.h"

#include <ctype.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <unistd.h>

int sys_printf(void)
{
    return printf((char*)&vmem.dspace[vcpu.c_reg[0]],
      (char*)&vmem.dspace[vcpu.c_reg[1]], (char*)&vmem.dspace[vcpu.c_reg[2]],
      (char*)&vmem.dspace[vcpu.c_reg[3]], (char*)&vmem.dspace[vcpu.c_reg[4]],
      (char*)&vmem.dspace[vcpu.c_reg[5]], (char*)&vmem.dspace[vcpu.c_reg[6]]);
}
int sys_exit(void)
{
    exit((int)vcpu.c_reg[0]);
    return 1;
}
int sys_yield(void)
{
    return 0;
}
int sys_sleep(void)
{
    return (int)sleep((unsigned int)vcpu.c_reg[0]);
}
int sys_usleep(void)
{
    return usleep((useconds_t)vcpu.c_reg[0]);
}
int sys_nanosleep(void)
{
    return nanosleep((const struct timespec*)&vmem.dspace[vcpu.c_reg[0]], (struct timespec*)&vmem.dspace[vcpu.c_reg[1]]);
}

/* https://sceweb.sce.uhcl.edu/helm/WEBPAGE-Python/documentation/python_tutorial/lib/module-posix.html POSIX, not calls... */
/* https://blog.rchapman.org/posts/Linux_System_Call_Table_for_x86_64/  -- Linux 4.7,  335 calls, (x86-64) */
/* https://marcin.juszkiewicz.com.pl/download/tables/syscalls.html -- Linux 5.17, 450 calls (arm 32-bit) */
/* https://chromium.googlesource.com/chromiumos/docs/+/master/constants/syscalls.md  -- Linux 4.14, 397 calls (arm 32-bit) */
/* https://github.com/torvalds/linux/blob/v5.17/arch/x86/entry/syscalls/syscall_32.tbl  -- Linux 5.17, 450 calls (x86-32) */

/* https://alfonsosiciliano.gitlab.io/posts/2021-01-02-freebsd-system-calls-table.html -- FreeBSD, 576 calls  */

/* V.OS Syscalls table (all should eventually be emulated under LCVM) */
const struct sysent sysent[] = {
  /* ~~~~ V.OS system calls table ~~~~ */
  {0, 1, NOF, "syscall", __syscall}, /*   0 = invalid/none/indirect */

  /* process control */
  {1, 0, AOK, "exit", sys_exit},           /*   1 = void exit(int r) */
  {1, 0, AOK, "fork", NULL},               /*   2 = int fork(void) */
  {4, 1, AOK, "wait", NULL},               /*   3 = int wait(pid_t wpid, int *status, int options, struct rusage *rusage) */
  {1, 1, AOK, "sleep", sys_sleep},         /*   4 = int sleep(int sec) */
  {1, 1, AOK, "usleep", sys_usleep},       /*   5 = int usleep(int usec) */
  {2, 1, AOK, "nanosleep", sys_nanosleep}, /*   6 = int nanosleep(const struct timespec*, struct timespec*) */
  {4, 1, AOK, "ptrace", NULL},             /*   7 = int ptrace(int request, pid_t pid, caddr_t addr, int data) */
  {2, 1, AOK, "kill", NULL},               /*   8 = int kill(pid_t pid, int sig) */
  {2, 1, OLD, "killpg", NULL},             /*   9 = int killpg(pid_t pgrp, int sig); */

  /* scheduler control */
  {0, 1, AOK, "yield", sys_yield},  /*  10 = void yield(void) */
  {3, 1, AOK, "setpriority", NULL}, /*  11 = int setpriority(int which, id_t who, int nice); */
  {2, 1, AOK, "getpriority", NULL}, /*  12 = int getpriority(int which, id_t who); */

  /* file/device control/configuration */
  {4, 1, AOK, "ioctl", NULL},     /*  13 = int ioctl(int fd, int cmd, int arg, int mode) */
  {4, 1, AOK, "fcntl", NULL},     /*  14 = int fcntl(int fd, int cmd, int arg, int mode ) */
  {2, 1, AOK, "pathconf", NULL},  /*  15 = int pathconf(const char *path, int name) */
  {2, 1, AOK, "fpathconf", NULL}, /*  16 = int fpathconf(int fd, int name); */

  /* basic file handling */
  {3, 1, AOK, "open", NULL},   /*  17 = int open(int fd, int flag, int mode) */
  {3, 1, AOK, "read", NULL},   /*  18 = int read(int fd, char* buf, size_t count) */
  {3, 1, AOK, "write", NULL},  /*  19 = int write(int fd, char* buf, size_t count) */
  {1, 1, AOK, "close", NULL},  /*  20 = int close(int fd) */
  {2, 1, AOK, "rename", NULL}, /*  21 = int rename(const char *oldpath, const char *newpath); */

  /* file creation */
  {2, 1, OBS, "creat", NULL},  /*  22 = int creat(char* path, mode_t mode) */
  {1, 1, AOK, "umask"},        /*  23 = mode_t umask(mode_t mask); */
  {3, 1, AOK, "mknod", NULL},  /*  24 = int mknod(char* path, mode_t mode, dev_t dev) */
  {2, 1, AOK, "mkfifo", NULL}, /*  25 = int mkfifo(const char* pathname, mode_t mode); */

  /* duplicate file descriptors */
  {1, 1, AOK, "dup", NULL},  /*  26 = int dup(int oldfd) */
  {2, 1, AOK, "dup2", NULL}, /*  27 = int dup2(int oldfd, int newfd) */
  {3, 1, AOK, "dup3", NULL}, /*  28 = int dup3(int oldfd, int newfd, int flags); */

  /* IO sync or wait for IO sync */
  {1, 1, AOK, "fsync", NULL},     /*  29 = int fsync(int fd); */
  {1, 1, AOK, "fdatasync", NULL}, /*  30 = int fdatasync(int fd); */
  {3, 1, AOK, "poll", NULL},      /*  31 = int poll(struct pollfd *fds, nfds_t nfds, int timeout); */

  /* file permissions */
  {2, 1, AOK, "chmod", NULL},   /*  32 = int chmod(const char* path, mode_t mode) */
  {2, 1, AOK, "lchmod", NULL},  /*  33 = int lchmod(const char* path, mode_t mode) */
  {2, 1, AOK, "fchmod", NULL},  /*  34 = int fchmod(int fd, mode_t mode); */
  {3, 1, AOK, "chown", NULL},   /*  35 = int chown(const char* path, uid_t uid, guid_t guid) */
  {3, 1, AOK, "lchown", NULL},  /*  36 = int lchown(const char* path, uid_t owner, gid_t group); */
  {3, 1, AOK, "fchown", NULL},  /*  37 = int fchown(int fd, uid_t owner, gid_t group); */
  {1, 1, AOK, "revoke", NULL},  /*  38 = int revoke(char* path) */
  {1, 1, AOK, "frevoke", NULL}, /*  39 = int frevoke(int fd) */
  {2, 1, AOK, "flock", NULL},   /*  40 = int flock(int fd, int operation); */

  /* file information/status */
  {2, 1, AOK, "chflags", NULL},  /*  41 = int chflags(char* path, int flags) */
  {2, 1, AOK, "fchflags", NULL}, /*  42 = int fchflags(int fd, int flags) */
  {2, 1, AOK, "utimes", NULL},   /*  43 = int utimes(const char *filename, const struct timeval tv[2]); */
  {2, 1, AOK, "lutimes", NULL},  /*  44 = int lutimes(const char *filename, const struct timeval tv[2]); */
  {2, 1, AOK, "futimes", NULL},  /*  45 = int futimes(int fd, const struct timeval tv[2]); */
  {2, 1, AOK, "stat", NULL},     /*  46 = int stat(const char* pathname, struct stat* statbuf); */
  {2, 1, AOK, "lstat", NULL},    /*  47 = int lstat(const char* pathname, struct stat* statbuf); */
  {2, 1, AOK, "fstat", NULL},    /*  48 = int fstat(int fd, struct stat* statbuf); */

  /* file addressing/size */
  {1, 2, AOK, "size", NULL},      /*  49 = size_t size(const char* path); */
  {1, 2, AOK, "fsize", NULL},     /*  50 = size_t fsize(int fd); */
  {3, 1, AOK, "seek", NULL},      /*  51 = off_t seek(int fd, off_t offset, int where) */
  {3, 1, AOK, "lseek", NULL},     /*  52 = off_t lseek(int fd, off_t offset, int where) */
  {3, 1, AOK, "fseek", NULL},     /*  53 = off_t fseek(FILE* str, off_t offset, int where) */
  {3, 1, AOK, "truncate", NULL},  /*  54 = int truncate(const char *path, off_t length); */
  {3, 1, AOK, "ftruncate", NULL}, /*  55 = int ftruncate(int fd, off_t length); */

  /* links */
  {2, 1, AOK, "link", NULL},     /*  56 = int link(int src, int dst) */
  {1, 1, AOK, "unlink", NULL},   /*  57 = int unlink(int fd) */
  {2, 1, AOK, "symlink", NULL},  /*  58 = int symlink(const char* target, const char* linkpath) */
  {3, 1, AOK, "readlink", NULL}, /*  59 = ssize_t readlink(const char* pathname, char* buf, size_t bufsiz) */

  /* process execution */
  {2, 1, AOK, "execv", NULL},  /*  60 = int execv(char* path, char* args) */
  {3, 1, AOK, "execve", NULL}, /*  61 = int execve(const char* path, const char** argv, const char** envp); */

  /* pipes */
  {1, 1, AOK, "pipe", NULL},  /*  62 = int pipe(int* fds) */
  {2, 1, AOK, "pipe2", NULL}, /*  63 = int pipe2(int pipefd[2], int flags) */

  /* mounting/partition control */
  {0, 0, AOK, "sync", NULL},    /*  64 = void sync(void) */
  {1, 1, AOK, "chroot"},        /*  65 = int chroot(const char *path); */
  {5, 1, AOK, "mount", NULL},   /*  66 = int mount(char* dev_path, char* mount_path, char* type, int flag, void* data) */
  {1, 1, AOK, "umount", NULL},  /*  67 = int umount(char* mount_path, int flag) */
  {2, 1, AOK, "swapon", NULL},  /*  68 = int swapon(const char *path, int swapflags); */
  {1, 1, AOK, "swapoff", NULL}, /*  69 = int swapoff(const char *path); */

  /* filesystem status */
  {3, 1, AOK, "getfsstat", NULL}, /*  70 = int getfsstat(struct statfs *buf, long bufsize, int mode)  */
  {2, 1, AOK, "statfs", NULL},    /*  71 = int statfs(const char *path, struct statfs *buf); */
  {2, 1, AOK, "fstatfs", NULL},   /*  72 = int fstatfs(int fd, struct statfs *buf); */
  {2, 1, AOK, "ustat", NULL},     /*  73 = int ustat(dev_t dev, struct ustat *ubuf); */
  {2, 1, AOK, "quotactl", NULL},  /*  74 = int quotactl(int cmd, const char* special, int id, caddr_t addr); */

  /* directories */
  {1, 1, AOK, "chdir", NULL},         /*  75 = int chdir(char* path) */
  {1, 1, AOK, "fchdir", NULL},        /*  76 = int fchdir(int fd) */
  {2, 1, AOK, "mkdir", NULL},         /*  77 = int mkdir(const char *pathname, mode_t mode); */
  {1, 1, AOK, "rmdir", NULL},         /*  78 = int rmdir(const char *pathname); */
  {3, 1, AOK, "getdirentries", NULL}, /*  79 = ssize_t getdirentries(int fd, char* buf, size_t nbytes, off_t* basep) */
  {3, 1, AOK, "getdents", NULL},      /*  80 = ssize_t getdents(int fd, void *dirp, size_t count) */
  {0, 1, AOK, "getdtablesize", NULL}, /*  81 = int getdtablesize(void) */

  /* p/g/u id stuff */
  {0, 1, AOK, "getpid", NULL},    /*  82 = pid_t getpid(void) -- get process id */
  {0, 1, AOK, "getppid", NULL},   /*  83 = pid_t getppid(void) -- get parent process id */
  {1, 1, AOK, "getpgid", NULL},   /*  84 = pid_t getpgid(pid_t) */
  {1, 1, AOK, "setgid", NULL},    /*  85 = int setgid(gid_t gid); */
  {0, 1, AOK, "getgid", NULL},    /*  86 = gid_t getgid(void) */
  {1, 1, AOK, "setegid", NULL},   /*  87 = int setegid(gid_t gid); */
  {0, 1, AOK, "getegid", NULL},   /*  88 = gid_t getegid(void) */
  {1, 1, AOK, "setuid", NULL},    /*  89 = int setuid(uid_t) */
  {0, 1, AOK, "getuid", NULL},    /*  90 = uid_t getuid(void) */
  {1, 1, AOK, "seteuid", NULL},   /*  91 = int seteuid(uid_t) */
  {0, 1, AOK, "geteuid", NULL},   /*  92 = uid_t geteuid(void) */
  {0, 1, AOK, "setpgrp", NULL},   /*  93 = pid_t setpgrp(void); */
  {0, 1, AOK, "getpgrp", NULL},   /*  94 = pid_t getpgrp(void); */
  {0, 1, AOK, "setsid", NULL},    /*  95 = pid_t setsid(void); */
  {2, 1, AOK, "getgroups", NULL}, /*  96 = int getgroups(int gidsetsize, gid_t* grouplist) */
  {2, 1, AOK, "setgroups", NULL}, /*  97 = int setgroups(int gidsetsize, gid_t* grouplist) */

  /* user/system info name handling */
  {6, 1, AOK, "sysctl", NULL},        /*  98 = int sysctl(const int* name, u_int namelen, void* oldp, size_t* oldlenp, const void* newp, size_t newlen) */
  {2, 1, AOK, "getlogin", NULL},      /*  99 = char* getlogin(void) */
  {1, 1, AOK, "setlogin", NULL},      /* 100 = int setlogin(char* name) */
  {1, 1, AOK, "uname", NULL},         /* 101 = int uname(struct utsname *buf) */
  {0, 1, OLD, "gethostid", NULL},     /* 102 = int gethostid(void) */
  {0, 1, OLD, "sethostid", NULL},     /* 103 = int sethostid(int hostid) */
  {0, 1, AOK, "gethostname", NULL},   /* 104 = int gethostname(char *name, size_t len); */
  {1, 1, AOK, "sethostname", NULL},   /* 105 = int sethostname(const char *name, size_t len) */
  {0, 1, AOK, "getdomainname", NULL}, /* 106 = int getdomainname(char *name, size_t len); */
  {1, 1, AOK, "setdomainname", NULL}, /* 107 = int setdomainname(const char *name, size_t len) */

  /* sockets */
  {3, 1, AOK, "socket", NULL},     /* 108 = int socket(int domain, int type, int protocol) */
  {4, 1, AOK, "socketpair", NULL}, /* 109 = int socketpair(int domain, int type, int protocol, int sv[2]) */
  {3, 1, AOK, "bind", NULL},       /* 110 = int bind(int sockfd, const struct sockaddr* addr, socklen_t addrlen) */
  {2, 1, AOK, "access", NULL},     /* 111 = int access(char* path, int mode) */
  {3, 1, AOK, "connect", NULL},    /* 112 = int connect(int sockfd, const struct sockaddr* addr, socklen_t addrlen) */
  {2, 1, AOK, "listen", NULL},     /* 113 = int listen(int sockfd, int backlog) */
  {3, 1, AOK, "accept", NULL},     /* 114 = int accept(int s, struct sockaddr* addr, socklen_t* addrlen) */
  {2, 1, AOK, "shutdown", NULL},   /* 115 = int shutdown(int sockfd, int how) */
  {3, 1, AOK, "getpeername"},      /* 116 = int getpeername(int s, struct sockaddr* name, socklen_t* namelen) */
  {3, 1, AOK, "getsockname"},      /* 117 = int getsockname(int s, struct sockaddr* name, socklen_t* namelen) */
  {5, 1, AOK, "setsockopt", NULL}, /* 118 = int setsockopt(int sockfd, int level, int optname, void* optval, socklen_t optlen) */
  {5, 1, AOK, "getsockopt", NULL}, /* 119 = int getsockopt(int sockfd, int level, int optname, void* optval, socklen_t* optlen) */
  {4, 1, OLD, "send", NULL},       /* 120 = ssize_t send(int sockfd, const void *buf, size_t len, int flags) */
  {6, 1, AOK, "sendto", NULL},     /* 121 = ssize_t sendto(int sockfd, const void* buf, size_t len, int flags, const struct sockaddr* dest_addr, socklen_t addrlen) */
  {4, 1, OLD, "recv", NULL},       /* 122 = ssize_t recv(int sockfd, void *buf, size_t len, int flags) */
  {6, 1, AOK, "recvfrom", NULL},   /* 123 = ssize_t recvfrom(int s, void* buf, size_t len, int flags, struct sockaddr* from, socklen_t * fromlen) */

  /* msg socket stuff */
  {3, 1, AOK, "recvmsg", NULL}, /* 124 = ssize_t recvmsg(int s, struct msghdr* msg, int flags) */
  {3, 1, AOK, "sendmsg", NULL}, /* 125 = ssize_t sendmsg(int s, struct msghdr* msg, int flags) */
  {3, 1, AOK, "msgctl", NULL},  /* 126 = int msgctl(int msqid, int cmd, struct msqid_ds *buf) */
  {3, 1, AOK, "msgget", NULL},  /* 127 = int msgget(key_t key, int msgflg) */
  {3, 1, AOK, "msgsnd", NULL},  /* 128 = int msgsnd(int msqid, const void *msgp, size_t msgsz, int msgflg) */
  {3, 1, AOK, "msgrcv", NULL},  /* 129 = ssize_t msgrcv(int msqid, void *msgp, size_t msgsz, long msgtyp, int msgflg) */

  /* memory info */
  {0, 1, AOK, "getpagesize", NULL}, /* 130 = int getpagesize(void) */
  {3, 1, AOK, "msync", NULL},       /* 131 = int msync(void *addr, size_t length, int flags) */
  {6, 1, AOK, "mmap", NULL},        /* 132 = void *mmap(void *addr, size_t length, int prot, int flags, int fd, off_t offset) */
  {5, 1, AOK, "mremap", NULL},      /* 133 = void *mremap(void *old_address, size_t old_size, size_t new_size, int flags, void *new_address) */
  {2, 1, AOK, "munmap", NULL},      /* 134 = int munmap(void *addr, size_t length) */
  {3, 1, AOK, "mprotect", NULL},    /* 135 = int mprotect(void *addr, size_t len, int prot) */
  {3, 1, AOK, "madvise", NULL},     /* 136 = int madvise(void *addr, size_t length, int advice) */
  {3, 1, AOK, "mincore", NULL},     /* 137 = int mincore(void *addr, size_t length, unsigned char *vec) */
  {1, 1, AOK, "brk", NULL},         /* 138 = int brk(caddr_t addr) */
  {1, 1, AOK, "sbrk", NULL},        /* 139 = caddr_t sbrk(int incr) */
  {1, 1, AOK, "sstk", NULL},        /* 140 = caddr_t sstk(int incr) */
  {3, 1, AOK, "mlock", NULL},       /* 141 = int mlock(const void *addr, size_t len, unsigned int flags) */
  {2, 1, AOK, "munlock", NULL},     /* 142 = int munlock(const void *addr, size_t len) */

  {0, 1, AOK, "vfork", NULL},   /* 143 = vfork */
  {3, 1, AOK, "vread", NULL},   /* 144 = vread */
  {3, 1, AOK, "vwrite", NULL},  /* 145 = vwrite */
  {3, 1, AOK, "vadvise", NULL}, /* 146 = vadvise */
  {0, 1, AOK, "vhangup", NULL}, /* 147 = vhangup */
  {0, 0, AOK, "vlimit", NULL},  /* 148 = vlimit */
  {3, 1, AOK, "readv", NULL},   /* 149 = readv */
  {3, 1, AOK, "writev", NULL},  /* 150 = writev */
  {0, 0, AOK, "vtrace", NULL},  /* 151 = vtrace */

  /* signals */
  {1, 1, AOK, "sigreturn", NULL},    /* 152 = sigreturn */
  {0, 1, AOK, "sigvec", NULL},       /* 153 = sigvec */
  {0, 1, AOK, "sigblock", NULL},     /* 154 = sigblock */
  {0, 1, AOK, "sigsetmask", NULL},   /* 155 = sigsetmask */
  {1, 1, AOK, "sigreturn", NULL},    /* 156 = sigreturn */
  {1, 1, AOK, "sigsuspend", NULL},   /* 157 = sigsuspend */
  {2, 1, AOK, "sigstack", NULL},     /* 158 = sigstack */
  {2, 1, AOK, "sigwait", NULL},      /* 159 = sigwait */
  {4, 1, AOK, "sigaction", NULL},    /* 160 = sigaction */
  {4, 1, AOK, "sigprocmask", NULL},  /* 161 = int sigprocmask(????) */
  {1, 1, AOK, "sigpending", NULL},   /* 162 = sigpending */
  {2, 1, AOK, "signaltstack", NULL}, /* 163 = int signaltstack(const stack_t* ss, stack_t* oss) */

  /* resource usage */
  {2, 1, AOK, "getrusage", NULL}, /* 164 = getrusage */
  {0, 1, AOK, "setreuid", NULL},  /* 165 = (old setreuid) */
  {0, 1, AOK, "setregid", NULL},  /* 166 = (old setregid) */
  {2, 1, AOK, "getrlimit", NULL}, /* 167 = getrlimit */
  {2, 1, AOK, "setrlimit", NULL}, /* 168 = setrlimit */

  /* power management */
  {1, 1, AOK, "reboot", NULL}, /*  169 = int reboot(int how) */

  /* clocks and times */
  {2, 1, AOK, "clock_gettime", NULL},   /* 170 = clock_gettime */
  {2, 1, AOK, "clock_settime", NULL},   /* 171 = clock_settime */
  {2, 1, AOK, "clock_getres", NULL},    /* 172 = clock_getres */
  {4, 1, AOK, "clock_nanosleep", NULL}, /* 173 = clock_nanosleep */
  {2, 1, AOK, "gettimeofday", NULL},    /* 174 = gettimeofday */
  {2, 1, AOK, "settimeofday", NULL},    /* 175 = settimeofday */
  {2, 1, AOK, "adjtime", NULL},         /* 176 = adjtime */
  {3, 1, AOK, "setitimer", NULL},       /* 177 = setitimer */
  {2, 1, AOK, "getitimer", NULL},       /* 178 = getitimer */

  /* ntp */
  {1, 1, AOK, "ntp_gettime", NULL}, /* 179 = ntp_gettime */
  {1, 1, AOK, "ntp_adjtime", NULL}, /* 180 = ntp_adjtime */

  {4, 1, AOK, "pread", NULL},   /* 181 = pread */
  {4, 1, AOK, "pwrite", NULL},  /* 182 = pwrite */
  {4, 1, AOK, "preadv", NULL},  /* 183 = preadv */
  {4, 1, AOK, "pwritev", NULL}, /* 184 = pwritev */

  {1, 1, AOK, "rdglob", NULL}, /* 185 = read from global */
  {2, 1, AOK, "wrglob", NULL}, /* 186 = write to global */

  /* terminal/io (most from libk) stuff */
  {1, 1, AOK, "putc", NULL},         /* 187 = putc */
  {1, 1, AOK, "putchar", NULL},      /* 188 = putchar */
  {1, 1, AOK, "puts", NULL},         /* 189 = puts */
  {6, 1, AOK, "printf", sys_printf}, /* 190 = test printf */
};

const int nsysent = sizeof(sysent) / sizeof(struct sysent);

/* do a system call from virtual registers */
int __syscall(void)
{
    if (nsysent >= vcpu.c_reg[SYSCALL_REG_NUM] && sysent[vcpu.c_reg[SYSCALL_REG_NUM]].s_fun)
    {
        if (DEBUG_SYSCALL)
        {
            printf("syscall: call num = %u ", vcpu.c_reg[SYSCALL_REG_NUM]);
            printf("= %s\r\n", sysent[vcpu.c_reg[SYSCALL_REG_NUM]].s_name);
        }

        /* set r0 to the return value */
        vcpu.c_reg[0] = sysent[vcpu.c_reg[SYSCALL_REG_NUM]].s_fun();

        /* clear other low registers */
        memset(&vcpu.c_reg[1], 0, sizeof(unsigned int) * (NLOWREG - 1));
    }
    else
    {
        printf("syscall: fault. call num = %u ", vcpu.c_reg[SYSCALL_REG_NUM]);
        printf(" not found.\r\n");
        print_creg();
        print_reg();
        print_mem(vmem.isize * sizeof(unsigned int), vmem.dsize);
        return 1;
    }
    return 0;
}
