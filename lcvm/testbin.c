
/* this file is a small elf binary file broken up into data to be written to the memory for testing */

const int tsize = 11;

/* instructions */
const unsigned int text[] = {
  0x0214007F, /* ; mov   r0, STRING1 */
  0x00000010, /* ; STRING1 = 16 */
  0x0214017F, /* ; mov   r1, STRING0 */
  0x00000000, /* ; STRING0 = 0 */
  0x05007F00, /* ; call  printf */
  0x00000000, /* ; printf = 0 */
  0x02000000, /* ; clr   r0 */
  0x05007F00, /* ; call  exit */
  0x00000001, /* ; exit = 1 */
  0x0FC00000, /* ; ret   r0 */
  0,
};

const int rodsize = 24;

/* initialised read-only data */
const unsigned char rodata[] = {
  /* STRING0: i == 0 */
  'H',   //-0-
  'e',   // 1
  'l',   // 2
  'l',   // 3
  'o',   //-4
  ',',   // 5
  ' ',   // 6
  'W',   // 7
  'o',   //-8-
  'r',   // 9
  'l',   // A
  'd',   // B
  '!',   //-C
  '\r',  // D
  '\n',  // E
  '\0',  // F
  /* STRING1: i == 16 */
  '%',   //-10-
  's',   // 11
  '\0',  // 12
  0,     // 13
  0,     //-14
  0,     // 15
  0,     // 16
  0      // 17
};

const int dsize = 0;

/* initialised read-write data */
unsigned char data[] = {};

/* uninitialised data */
const int bss_size = 0;

/* no need for an array here, as it's loaded to 0 based on size and doesn't actually include data */
