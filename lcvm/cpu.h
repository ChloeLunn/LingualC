/*
BSD 3-Clause License

Copyright (c) 2020-2022, Chloe Lunn
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this
   list of conditions and the following disclaimer.

2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution.

3. Neither the name of the copyright holder nor the names of its
   contributors may be used to endorse or promote products derived from
   this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

/* LCVM Instructions/Operations */

#if !H_INSTR
#define H_INSTR 1

#include <stdint.h>

struct transport {
    int s_type; /* type of source */
    int s_val;  /* source val (address or reg num) */
};

#define SREG (1)
#define SBUS (2)
#define SMEM (4)
#define SPTR (8)

struct operand {
    int o_size;               /* size of operand in bytes */
    uint64_t o_val;           /* operand value */
    struct transport o_trans; /* transport layer info */
};

/* write an 8-bit number to dspace */
int dwrite8(unsigned int a, uint8_t v);
/* read an 8-bit number from dspace */
uint8_t dread8(unsigned int a);

/* write a 16-bit number to dspace */
int dwrite16(unsigned int a, uint16_t v);
/* read a 16-bit number from dspace */
uint16_t dread16(unsigned int a);

/* write a 32-bit number to dspace */
int dwrite32(unsigned int a, uint32_t v);
/* read a 32-bit number from dspace */
uint32_t dread32(unsigned int a);

/* write a 64-bit number to dspace */
int dwrite64(unsigned int a, uint64_t v);
/* read a 64-bit number from dspace */
uint64_t dread64(unsigned int a);

/* grab the next 8-bit number from the instructions space */
uint8_t iread8();
/* grab the next 16-bit number from the instructions space */
uint16_t iread16();
/* grab the next 32-bit number from the instructions space */
uint32_t iread32();
/* grab the next 64-bit number from the instructions space */
uint64_t iread64();

int resolve_trans(struct operand* dv, struct operand* sv);

/* mov */
int MOV(unsigned int instr);
/* clear */
int CLR(unsigned int instr);
/* sys call */
int CALL(unsigned int instr);

#endif
