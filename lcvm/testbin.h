
/* this file is a small elf binary file broken up into data to be written to the memory for testing */

extern const int tsize;
extern const unsigned int text[];

extern const int rodsize;
extern const unsigned char rodata[];

extern const int dsize;
extern unsigned int data[];

extern const int bss_size;
