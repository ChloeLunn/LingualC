/*
BSD 3-Clause License

Copyright (c) 2022, Chloe Lunn
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this
   list of conditions and the following disclaimer.

2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution.

3. Neither the name of the copyright holder nor the names of its
   contributors may be used to endorse or promote products derived from
   this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include "vcore.h"

#include "../tools/hexdump.h"
#include "cpu.h"     /* instructions */
#include "itab.h"    /* instructions table */
#include "mach.h"    /* machine info */
#include "syscall.h" /* syscall vectors and sysent structs */

#include <ctype.h>  /* isprint */
#include <setjmp.h> /* virtual interrupts */
#include <stdio.h>  /* prints */
#include <stdlib.h> /* exit */
#include <string.h> /* memclr, memcpy */

/* virtual processor cpu */
struct cpu vcpu;

/* virtual processor fpu */
struct fpu vfpu;

/* virtual memory */
struct mem vmem = {
  .ispace = &vmem.core[IADDR],
  .dspace = &vmem.core[DADDR],
};

/* syscall jump buffer (used to emulate interrupts) */
jmp_buf sbuf;

void print_creg()
{
    printf("ci = 0x%08x; pc = %u; sp = %u; lr = %u; psr = 0x%08x\r\n", vcpu.c_cir, vcpu.c_pc, vcpu.c_sp, vcpu.c_lr, vcpu.c_psr);
}

void print_reg()
{
    for (int i = 0; i < NLOWREG + NHIGHREG; i++)
    {
        printf("vcpu reg %i = %i\r\n", i, vcpu.c_reg[i]);
    }
}

void print_mem(unsigned int isize, unsigned int dsize)
{
    puts("ispace dump: ");
    hexdump(vmem.ispace, isize);
    puts("dspace dump: ");
    hexdump(vmem.dspace, dsize);
}

/* panic the processor */
int vpanic()
{
    if (DEBUG_CPU)
        puts("vcore: panic");
    exit(1);
    return 1;
}

/* clear the memory */
int vclear()
{
    if (DEBUG_CPU)
        puts("vcore: clearing memory");
    /* clear memory */
    memset(vmem.core, 0, UCORE);
    return 0;
}

/* reset the cpu, clear registers, and halt. */
int vreset()
{
    if (DEBUG_CPU)
        puts("vcore: resetting cpu");

    /* clear cpu */
    memset(&vcpu, 0, sizeof(struct cpu));
    /* clear fpu */
    memset(&vfpu, 0, sizeof(struct fpu));

    /* halt (already done, but be explicit...) */
    vhalt();

    return 0;
}

/* step the virtual machine */
int vstep()
{
    if (DEBUG_CPU)
    {
        puts("vcore: stepping processor");
    }

    /* if processor should run */
    if ((vcpu.c_ctr & CC_RUN) && !(vcpu.c_ctr & CC_WFI))
    {
        /* load the current instruction */
        vcpu.c_cir = iread32();

        /* for each instruction */
        for (int i = 0; i < NITAB; i++)
        {
            /* if the instruction matches the current instruction */
            if ((itab[i].i_inst & INSTR_BITS) == (vcpu.c_cir & INSTR_BITS))
            {
                /* call that function and return it's error code */
                if (itab[i].i_func)
                {
                    if (DEBUG_INSTR)
                        printf("instr: %s (0x%08x)\r\n", itab[i].i_name, vcpu.c_cir);
                    return itab[i].i_func(vcpu.c_cir);
                }
                else
                {
                    if (DEBUG_INSTR)
                        printf("instr: not found (0x%08x)\r\n", vcpu.c_cir);
                    return 1;
                }
            }
        }

        /* if we get here, the operation was unknown, so error */
        return 1;
    }
    /* processor is waiting, so just return 0 */
    return 0;
}

/* save the virtual registers */
int vsave(label_t* rsave, int v)
{
    memcpy(*rsave, &vcpu.c_reg, NREGTOTAL * sizeof(unsigned int));
    *rsave[NREGTOTAL + 0] = 1; /* dirty flag */
    *rsave[NREGTOTAL + 1] = v; /* value to return on restore */
    return 0;
}

/* restore the virtual registers */
int vrestore(label_t* rsave)
{
    /* no contents to label */
    if (!(*rsave[NREGTOTAL + 0]))
    {
        return 1;
    }
    /* otherwise, copy label into registers */
    memcpy(&vcpu.c_reg, *rsave, NREGTOTAL * sizeof(unsigned int));
    /* return the saved return value */
    return *rsave[NREGTOTAL + 1];
}

/* swap the core memory */
int vswap(unsigned char* dst, const unsigned char* src, unsigned int dlen, unsigned int slen)
{
    /* if there is a destination, use it */
    if (dst != NULL)
    {
        /* swap memory out, to destination */
        for (int i = 0; i < dlen && i < UCORE; i++)
        {
            dst[i] = vmem.core[i];
        }
    }

    /* if there is a source, use it */
    if (src != NULL)
    {
        /* can't swap in, not enough core */
        if (slen > UCORE)
        {
            return -1;
        }

        /* swap memory in, from source */
        for (int i = 0; i < slen && i < UCORE; i++)
        {
            vmem.core[i] = src[i];
        }
    }

    return 0;
}

/* virtual CPU cycle loop */
int vloop()
{
    int vec = setjmp(sbuf);
    if (vec && (vcpu.c_ctr & CC_RUN))
    {
        if (vec == SYSCALL_INT_VEC)
        {
            if (__syscall())
            {
                return 1;
            }
        }
    }

    if (DEBUG_REG)
    {
        print_reg();
    }
    if (DEBUG_MEM)
    {
        print_mem(vmem.isize * sizeof(unsigned int), vmem.dsize);
    }

    /* if step returns 1, that means something went wrong, so return fault */
    if (vstep())
    {
        printf("vstep: fault.\r\n");
        print_creg();
        print_reg();
        print_mem(vmem.isize * sizeof(unsigned int), vmem.dsize);
        return 1;
    }

    return 0;
}

/* mark processor as run */
void vrun()
{
    /* set processor to run */
    vcpu.c_ctr = vcpu.c_ctr | CC_RUN;
}

/* mark processor as halt */
void vhalt()
{
    /* set processor to halt */
    vcpu.c_ctr = vcpu.c_ctr & ~CC_RUN;
}

/* mark processor as halt */
void vwait()
{
    /* set processor to wait for interrupt */
    vcpu.c_ctr = vcpu.c_ctr | CC_WFI;
}
