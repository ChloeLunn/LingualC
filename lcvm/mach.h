/*
BSD 3-Clause License

Copyright (c) 2020-2022, Chloe Lunn
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this
   list of conditions and the following disclaimer.

2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution.

3. Neither the name of the copyright holder nor the names of its
   contributors may be used to endorse or promote products derived from
   this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

/* LCVM CPU Definitions */

#if !H_LCVM_MACH
#define H_LCVM_MACH 1

/* ~~~~ CPU REGISTERS ~~~~ */

#define NLOWREG  8 /* low (user/arg) registers */
#define NHIGHREG 5 /* high (os/code) registers */

#define NCONTREG 3 /* control (e.g. stack pointer) registers */

/* control registers */
#define NI_STACKPOINT 13
#define NI_LINKREG    14
#define NI_PROGCOUNT  15

/* number of special registers */
#define NSPECREG 4

/* indexes into register array for special registers */
#define NI_PROCSTAT 16
#define NI_INTMASK  17
#define NI_CONTROL  18
#define NI_INSTRUCT 19

/* number of registers in the program controllable array */
#define NREGISTERS (NLOWREG + NHIGHREG + NCONTREG)

/* complete number of registers (20) */
#define NREGTOTAL (NREGISTERS + NSPECREG)

/* lcvm processor registers - somewhat arm-y */
struct cpu {
    unsigned int c_reg[NREGTOTAL]; /* internal register storage */
#define c_sp  c_reg[NI_STACKPOINT] /* regular stack pointer */
#define c_lr  c_reg[NI_LINKREG]    /* link register (prog count on JSRs) */
#define c_pc  c_reg[NI_PROGCOUNT]  /* program counter */
#define c_psr c_reg[NI_PROCSTAT]   /* processor status register */
#define c_imr c_reg[NI_INTMASK]    /* interrupt mask register */
#define c_ctr c_reg[NI_CONTROL]    /* control register */
#define c_cir c_reg[NI_INSTRUCT]   /* current instruction being executed */
};

#define CC_RUN (1 << 0)
#define CC_WFI (1 << 1)

#define REG_R (1 << 0)
#define REG_W (1 << 1)

/* they're all read/write because we only have the single operating mode */
static const int reginfo[NREGTOTAL] = {
  REG_R | REG_W, /* R0  - argument 0 - return value */
  REG_R | REG_W, /* R1  - argument 1 */
  REG_R | REG_W, /* R2  - argument 2 */
  REG_R | REG_W, /* R3  - argument 3 */
  REG_R | REG_W, /* R4  - argument 4 */
  REG_R | REG_W, /* R5  - argument 5 */
  REG_R | REG_W, /* R6  - argument 6 */
  REG_R | REG_W, /* R7  - Syscall number */
  REG_R | REG_W, /* R8  - High side 0 */
  REG_R | REG_W, /* R9  - High side 1 */
  REG_R | REG_W, /* R10 - High side 2 */
  REG_R | REG_W, /* R11 - High side 3 */
  REG_R | REG_W, /* R12 - High side 4 */
  REG_R | REG_W, /* SP  - Stack Pointer */
  REG_R | REG_W, /* LR  - Link Register */
  REG_R | REG_W, /* PC  - Program Counter */
  REG_R | REG_W, /* PSR - Processor Status Register */
  REG_R | REG_W, /* IMR - Interrupt Mask Register (not implemented) */
  REG_R,         /* CTR - Control Register */
  REG_R,         /* CIR - Current Instruction Register */
};

/* status register bits */
enum
{
    psr_negative = (1 << 0), /* result was negative */
    psr_carry = (1 << 1),    /* resulted in carry */
    psr_zero = (1 << 2),     /* resulted in zero */
    psr_overflow = (1 << 3), /* resulted in overflow */
};

#define LSIZE (NREGTOTAL + 2)
#define LTYPE int

/* ~~~~ FLOATING POINT PROCESSING ~~~~ */

/* lcvm virtual fpu */
struct fpu {
    float lf_reg[NREGISTERS];   /* floating point registers */
    double lf_dreg[NREGISTERS]; /* double float registers */
    int lf_fps;                 /* fpu status */
};

/* ~~~~ MEMORY ARRAY ~~~~ */

/* core memory allowed for the virtual machine (16KiB) */
#define UCORE (unsigned long)(1024 * 16)
/* size of instructions core */
#define ISIZE (UCORE / 2)
#define IADDR (0)
/* size of data core (inc stack) */
#define DSIZE (UCORE / 2)
#define DADDR (ISIZE)

struct mem {
    unsigned char core[UCORE]; /* core memory */
    unsigned char* ispace;     /* pointer to instructions space */
    unsigned char* dspace;     /* pointer to data space */
    unsigned int isize;        /* size of instruction space */
    unsigned int dsize;        /* size of data space */
};

/* ~~~~ Interrupt Jump Table entry ~~~~ */

struct irq {
    unsigned short vec; /* vector it should be executed on */
    unsigned short pri; /* priority (lower takes priority) */
    unsigned int lr;    /* PC when int is called (jump back on return) */
    unsigned int pc;    /* PC to jump to */
};

/* ~~~~ COMPLETE MACHINE ~~~~ */

/* Complete machine as a single struct */
struct mach {
    unsigned int flag; /* machine flags */
    struct cpu vcpu;
    struct fpu vfpu;
    struct irq virq[8];
    struct mem vmem;
};

#define MF_NOSYS (1 << 0) /* don't allow sys calls */
#define MF_NOIRQ (1 << 1) /* don't allow interrupts (exl sys calls) */

#endif
