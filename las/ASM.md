LAS Assembly Syntax
===================

The LAS assembler syntax is based on a hybrid between ARM and PDP-11 (MACRO-11) assembly, with simplifications to improve parsing and code management.

We follow the RFC meanings of may, can, should, and must here. 

Please note that if passing the file to LAS using stdin file streams or pipes, some syntax around ignoring new lines in comments or strings may not function as you expect, it is assumed that if passing in as stdin then you will have already run the file through the LPP pre-processor to remove comments, handle macros and includes, etc.

Output files:
-------------

LAS is primarily designed to produce Little-endian, 2s compliement, ELF binary files; but a.out support may be added at some point. At the moment, however, the output files are all designed to be used with the LCVM loader/virtual processor and the LCC/LPP/LAS/LLD/LID/LDYLD programs, so no compatibility with other software is even considered let alone offered. 

As a result of this, because the ELF files are not going to be used with other programs, they may not fully meet the ELF standard and as such we have decided that they should be called "PXE" (pronounced 'pixie') files instead to reduce the chances of assuming compatibility. This stands for "Probably eXecutable: ELF?".

There is an assembly instruction (.flatpack) which will tell the assembler to output the file as a flat binary and ignore section control instructions.
If no section control assembly instructions are given at all, then the default output will be flat binary data, even if it is packaged inside the data section of a PXE file.

The current project will produce LTL (load time locatable) code, however support for PDC/PIC will be added at some point. So that ventually, LAS will support six main types of outputed data:

1. A complete PXE executable as LTL (load time locatable) code - this only works if there are no symbols that need to be externally statically linked - but this may still work if missing functions can be dynamically linked.
2. A complete PXE executable as PIC (position independent code) - as 1
3. An incomplete PXE executable as LTL code - this will then need external linking to other object files as the library locator failed to find dynamically linkable code
4. A complete a.out executable as LTL - we do not support PIC or incomplete a.out files.
5. As flat binary data
6. As flat binary data in hex code/text format (intel format)


Assembler process (depends on output and source options)
--------------------------------------------------------

If the LPP (pre processor) is enabled or the output from the preprocessor is what is passed to LAS then this will handle preprocessor information, comments, macros, etc. except for a few LAS-specific tasks, which the internal pre processor will handle as it gets to them.

The LAS assembler then produces binary segments in a single parse to a temporary unlinked PXE file which only exists in memory and which contains extra sections that declare unlinked information in a format suitable for our linkers.

This is then run through a small, internal static linker to ensure that labels, symbols, and functions are correctly linked to each other inside the program. 

If any symbols aren't still linked then the binary output is produced in a temporary directory, and run through the static and dynamic linkers. These will find any functions that are requested to be static and link them statically, and if it finds suitable dynamic libraries for other symbols then it will generate the information sections in the PXE format for those before stripping our special unlinked sections.

The final process is to convert these linked sections into the final binary file. If using PXE or a.out then it will create these and if using a flat binary then it will fix the linkage so that the final file is flat (see formats above). 

Depending on the code loading options (PDC, PIC, LTL) required, the binary is suitably "massaged" to allow for this.

Case sensitivity:
-----------------

LAS is case sensitive in all operations, therefore you must make sure that all instances of a name in the code you write has the same case everywhere a word/definition/instrution is used.

If the platform you are writing for has rtab/itab/atab contents that are uppercase, then you must follow and use upper case, and v.v. for lowercase.

It is convention that items that are given an explicit name use lower case, e.g. for functions or sections, items that are automatically generated labels use upper case, e.g. STRING0 for a string, and it is convention that definitions either as a pre-processor macro, or as .equ operations use upper case.

Of course, at the end of that, as long as you obey that the system is case sensitive, then it doesn't particularly matter.


Space: The final front-end
--------------------------

In LAS Assembly, white-space such as tabs and spaces are used to separate terms in instructions and new lines (CRLF or just LF) are used to separate instructions. Blank lines are ignored.

The amount of white-space between terms/operands/arguments in an instruction does not matter, so you may use multiple tabs or spaces to align code and comments. As long as there is at least single space between each item in an instruction.

Lines may start with as many tabs or spaces as you required, however it is convention to indent code lines with a tab or 2-4 spaces and to not indent labels.

To ignore white-space, e.g. for a term that needs to contain spaces (strings) or calculations (label maths), you can use brackets `(` `)`, quotes `"`, and apostrophes `'`. 

Items in brackets (`(`) should be used to enforce order-of-operations on assembler maths, e.g. `((LABEL1 - LABEL 2) * LABEL3)`.

Items in quotes (`"`) will be taken to be strings where each character is an ASCII encoded 8-bit value (7-bit ASCII with top bit padded to 0).

Items in apostrophies (`'`) are used to include the white-space in the argument, but they do not automatically say anything about the type of data or encoding.

A line ending (CRLF or LF) will be taken as the closing for brackets, quotes, and apostrophies. If you require multiple lines in strings, these should be encoded with escaped characters (`\r\n` or `\n`).

A line ending with a trailing `\` character will signify that the new line symbols (CRLF or LF) should be ignored and instead the next source code line is part of the same previous line. The `\` must be the last non-newline character on that line.

Note that this DOES NOT add a newline to a string, so you should still use the `\r\n` or `\n` in the string if you wish to include a new line at that point.


Source Code Comments: ASM-style comments
----------------------------------------

LAS supports two native assembly ways of inserting comments into assembly source code.

For **single line** (or end of line) comments, you may use `#`, `;`. These comments start at the symbol and continue until the new line (CFLF or LF) symbol, which is then considered the end of comment so that the next line is not commented. A single line comment may be modified to be multi line by using a trailing `\` (see section on white space, above). 

A comment must be separated/delimited from other terms or instructions by a preceding space, and uses a trailing space to determine if the symbol is to be taken as a comment marker or as part of the next term. i.e. the line `ins  dest, source # comment` has a comment after the source term, but the lines `ins  dest, source#` and `ins  dest, #source` include the '#' as part of the argument 'source' and do not contain a comment.

If enabled using a program argument (or by re-compiling with different default behaviour) then LAS also supports old PDP-11 UNIX style `/` comments. Note that enabling these will break/disable inline label division. Unlike GAS or some other assemblers, these comments may be both single line or end of line, and follow the same rules as end of line above.

```
; semi-colon comment
# hash comment
/ unix comment

```

```
; multi line  \
	made from \
	single line
```

Source Code Comments: C-style comments
--------------------------------------

LAS also supports C-style comments.

For **single line** (or end of line) comments, you may use `//`, and these follow the same rules as other single line comments above.

For **multi line** (or inline) comments, you may use ANSI 89 C style `/*` and `*/` pairs. These comments can be anywhere including over multiple lines and the comment is only ended by a `*/`. Brackets (and even instructions, values, and label names) can contain these multi-line comments, however comment markers are included into terms that are in quotes or apostrophes. i.e. `(/* comment */1)` is processed as `(1)` but `"/*comment*/1"` is still processed as `"/*comment*/1"`

```
// c single-line comment
```

```
// multi line \
   made from a \ 
   single line
```

```
/* multi
line
ansi
comment
*/
```

Assembler Comments
------------------

LAS also supports the use of assembler single line (or end of line) comments using `@` symbols. 

These work the same as single line source code comments above, however they aren't completely ignored during assembly. They are instead included into the "Comments" section of the PXE binary along with the line number and source from that line. These are sometimes used by debuggers.

There is a program argument which will allow treating '@' comments the same as regular line comments (i.e. do not put them in the binary), and compiling as a flat binary will also cause these to be treated the same as regular single line comments.

Errors and Warnings
-------------------

Unlike some assemblers, LAS attempts to include usable error reporting that includes not only a description of the error causing the unclean exit, but also the source code line number the error occured on.

Some known errors are:

1. Too Many Arguments (too many arguments given for the instruction given)
2. Too Few Arguments (too few arguments given for the instruction given)
3. Invalid Syntax (something broke syntax rules, but not sure what)
4. Invalid Instruction (the argument on the code line does not count)
5. Invalid Label Name (the label name includes invalid information)
6. Invalid Size Modifier (the size modifier given was invalid)
7. Internal Process Error (Something inside the assembler broke and we can't handle it)

Numbers and Values
------------------

Any term or argument beginning with a numerical digit ('0' through '9') is assumed to be a number. Thus, all non-numbers must start with a letter character first. i.e. `0label` is invalid, but `label0` is okay.

Numbers may be denoted in a variety of different numerical bases:

Binary values must be followed by a 'b', e.g. `0110b`

Decimal values can be followed by a 'd' or no value, e.g `50` or `50d`

Octal values must be followed by an 'o', e.g `377o`

Hexadecimal values must be followed by an 'h', e.g `0F4A3h`. Hex numbers are NOT case sensitive.

If you wish to state a custom base you may do this by following the number immediatley with a '.' and then the number to use as the base. e.g. `01201.3` to denote trinary. Custom bases may only be in the range 2 to 16 (binary to hexadecimal).

Invalid numbers will cause an assembly error.


Op-Code or Processor Instructions
---------------------------------

These are your processor instructions like MOV and ADD. 

Operations with two (or more) operands must be in the order Destination, Source. 

Operands may be reserved words (typically register names), number values (decimal, hex, and octal are supported), label names, or system call or library function names.

These are defined in the itab (instructions table) file. Please see the equivalent specification document for your platform for more information.


Data Size Modifiers
-------------------

Some instructions, as part of the same term as the instruction, may also be passed a size modifier to change the processed size of operands from native 32-bits to other sizes or types (related: see casts in C).

A size modifier is done by immediatley following an instruction by a '.' and a character indicating the size or type to process that instruction with. e.g. `add.s` to do an addition as shorts (16-bits).

The symbols used for size are:

`n` for native (likely 32-bits). If the size modifier is missing, this is automatically assumed.

`b` for bytes (8-bits).

`c` for characters. This modifier is typically only used for RES and DEF assembler instructions as it is virtually identical to `b`.

`s` for shorts (16-bits)

`l` for longs (32-bits)

`q` for quads (64-bits)

Note that `n` is not always 32-bits, if the alignement has been changed (usually though a `.align` assembly instruction) then the native size will be altered to match this, and some instructions may operate on different sizes by default as well.

Some platforms may not implement all of these, may not support them for all instructions, or may even contain more size modifier options. Please see the specifications on those platforms, e.g. [LCVM.md](./LCVM.md), for more information.

### Example:

If we have a generic MOV instruction (LCVM assembly) `mov  r0, 0ff4h` this says to move the hex value 0xFF4 into the contents of register r0.

However, we can modify that MOV instruction using a size modifier to say that it should actually only do the move as a single byte: `mov.b  r0, 0ff4h`. Thus the value in r0 when finished is actually only 0xF4.

This example is rather pointless as we know that the 0xFF4 is a constant and doesn't need to include the 0xF00 which will always removed, however it may become useful if you wish to cast a longer number stored at a memory address or other register and cast/constrain it to a smaller number of bits when moving it, say `mov.b  r0, [0ff4h]` which now says to move the contents at *address* 0x0FF4 as a byte. 


Labels
------

Labels are used to provide addresses or variable names during assembly, they are typically left-aligned (no white space between start of line and label name/tag) and by convention are upper-case for variables and lower-case for functions.
For example ```VAR0:``` for a label that points to the address for a variable number 0, and ```main:``` for the label which points to the main function

The text of a label may sometimes be referred to as a tag. You must not give a label the same name as a reserved word for your platform. See the specification for your platform for more information on which words are reserved.

To declare/define a label, put the name/tag for the label at the start of the line and follow it by a colon (':'). A label definition must be the first item in that source code line.

To use a label, simply use the name of the label in place of a numerical value or reserved word.

A label must start with a non-numerical character. You must avoid using non-character symbols inside of a label as these are used as other symbols with the exception of an underscore '_' or a preceding dot ('.').

A preceding dot is, however, still considered part of the label name, so if it is used then it must be present in all instances of that label. i.e. ".main" is different to "main".

By convention, a preceding '.' is used for system or compiler-generated labels, and items named by the programmer should not use the preceding '.'.

BE AWARE: LABEL MATHS IS INCOMPLETE AND WILL NOT FUNCTION

## System calls

System calls are performed using the ```call``` instruction, this saves the current program status and position, jumps (via soft interrupt) to the operating system, performs the call, and returns execution back to where the program left.

The low-side registers are used to pass arguments, thus a maximum of 8 32-bit arguments can be passed. Arguments smaller than 32-bits will still take a full 32-bit register, and thus be promoted to 32-bits.

The return value of the system call is placed in register r0. All other low-side registers (regardless of use in the system call) will be wiped, with the exception that r1 may be used if returning a 64-bit value. As mentioned above, the r0 register will contain the least-significant 32-bits, and r1 the most significant 32-bits. As a result, you can test for size of return value by checking the presence of a non-zero value in r1.

## Addresses to Values

To tell the compiler you wish to pass the value at an address you must instead pass the argument wrapped with '[' ']'.
There is an exception with registers, where the braces indicate that you wish to pass the value stored at the address stored in that register as the register itself cannot be a memory address, so we assume all references to a register apply to the contents of that register.

e.g. ```[r0]``` treats the contents of r0 as an address, which it then uses to grab and pass the value of. Alternatively: ```[sp]``` would grab the value stored at the address currently in the stack pointer

This can be used on labels e.g. ```[VAR0]``` would pass the value stored at the address of label 'VAR0'

This can be used on numerical constants, e.g. ```[050h]``` to pass the value at address 0x050.

Each argument can only be passed through the value option once, so to do it twice you will need to push the first pass to the stack and then grab it through ```[sp]```

Operations where both the destination and source are the contents of the stack, will operate directly on the stack in order (a la stack machine style)


Assembler instructions
----------------------

Assembler instructions are lines of code that do not get executed or modified as variables by the executing processor, but are instead instructions given to the assembler program (LAS) to tell it how to operate.

By convention, assembler instructions start with a "." to show that they aren't processor instructions and you will see that in those built into LAS.

### Source control



### Definitions

In order to store some data in a predefined format there are a set of assembler instructions to do this.

```.db``` for define bytes, ```.dc``` for define character string with null-termination, ```.ds``` for define 16-bit words, ```.dl``` for 32-bit values, and ```.dq``` for 64-bit values. 

There is also a generic define `.def` which takes size modifiers 

These take a series of arguments, each one being the predefined value to use. Alignment between each separate stored value always defaults to the native 32-bit alignment, however when using multiple arguments on a storage instruction, alignment is controlled by the instruction.

e.g. ```.db   0, 1, 2, 3``` would 8-bit align four 8-bit values, 0 1 2 and 3 in that order in the binary section, but the code:

```
.db 0
.db 1
```

Would align them to 32-bits, even if the value itself is only 8-bits.

Padding bytes added for alignment are always places to the RIGHT of the value, so for the ```.db 1``` the binary file's 32-bit would be 1 0 0 0. This is so that conversion between 8-bit and 32-bit values is simplified (at least for unsigned values)

Values are always stored in a little-endian format, with 2's compliment for signed values. 

For strings, you can use ```.db``` or ```.dc``` like: ```.dc "Hello, World!\r\n"```.

Using .dc will insert a null-terminated string into the binary and using .db will not null terminate. For single characters/bytes or arguments in apostrophes, both instructions behave the same.

Note that .dc uses the native encoding for the file, assembler, and system. This should always be ascii or UTF-8.

### Binary format/sections

To specify the sections in the final binary file, the assembly writer will include instructions that specify that everything after the instruction should be stored in X section.

The assembler pre-defines a set of these as their own instruction:

```.data``` for pre-initialised read/write data

```.rodata``` for pre-initialised read-only data (note that read-only is a *suggestion* and does not have to be followed)

```.bss``` for uninitialised data

```.text``` for pre-initialised read-only instructions data. (note that some assemblers include read-only strings in the .text section)

```.note``` for binary file notes

```.flatpack``` for a flat data binary file (all other section info is ignored and information is simply concatenated).

To define a custom section, or to define the above more explicitly, you may use the instruction ```.section``` followed by the name you wish to use for that section. e.g. ```.section .note```

(see figure A-1, Book I, page A-2, of https://refspecs.linuxbase.org/elf/elf.pdf for complete list of pre-defined sections)

Please note that the assembler may automatically add data to the above sections, or change where sections are to be loaded if it needs to. 

There are two further instructions that can be used in helping out the binary format. 

```.org``` this sets the origin/offset within a section to the value in it's argument. Some old assemblers required a ```.org 0``` at the begining of each section, but this is now implied.
It can however be used for creating space for an array.

```.globl``` this adds the following label to the global symbols table in the binary file (sometimes called a manifest). e.g. ```.globl main``` to add the main function.

For functions that need to be called externally when launching the executable (including main or start) this is mandatory.
