/*
BSD 3-Clause License

Copyright (c) 2020-2022, Chloe Lunn
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this
   list of conditions and the following disclaimer.

2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution.

3. Neither the name of the copyright holder nor the names of its
   contributors may be used to endorse or promote products derived from
   this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

/* LCVM Assembler Error Numbers */

#if !H_LAS
#define H_LAS 1

#ifndef __lingualc
#include "../lctoc/lctoc.h"
#endif

#include "ptab.h"

/* if c is any of the chars in q */
int isany(char c, str q);

/* if any c in s is any of the chars in q */
int strany(str s, str q);

/* if any c in s is any of the chars in q */
int strnone(str s, str q);

struct bin_pos current_position;

int size_to_int(const str size, const str sizes, const int ptr size_vals);

int str_to_radix(str var);

int calc_padding(int size);

int deposit(int size, big value);

int create_unlinked(str name, int size);

#define EENDOFFILE  (-1)  /* error end of file/stream */
#define ENOERROR    (0)   /* no error */
#define EARGS2BIG   (1)   /* args too big */
#define EARGS2SMALL (2)   /* args too small */
#define EBADSYNTAX  (3)   /* some kind of syntax error */
#define EBADINSTR   (4)   /* unknown instruction */
#define EBADLABEL   (5)   /* bad label name */
#define EBADSIZE    (6)   /* bad size modifier */
#define EUNLINKED   (7)   /* something is unlinked */
#define ENOIMP      (8)   /* not implemented yet */
#define EDUPLINK    (9)   /* duplicate links or labels */
#define EINTERNAL   (100) /* some kind of internal error */

static const char* errstr[] = {
  [ENOERROR] = "no error",
  [EARGS2BIG] = "too many args",
  [EARGS2SMALL] = "too few args",
  [EBADSYNTAX] = "invalid syntax",
  [EBADINSTR] = "invalid instruction",
  [EBADLABEL] = "invalid label name",
  [EBADSIZE] = "invalid size modifier",
  [EINTERNAL] = "internal process error",
  [EUNLINKED] = "failed to resolve links",
  [ENOIMP] = "feature not implemented yet",
  [EDUPLINK] = "duplicate links or labels",
  [(unsigned char) EENDOFFILE] = "end of file",
};

#endif
