/*
BSD 3-Clause License

Copyright (c) 2020-2022, Chloe Lunn
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this
   list of conditions and the following disclaimer.

2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution.

3. Neither the name of the copyright holder nor the names of its
   contributors may be used to endorse or promote products derived from
   this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

/* LCVM Assembler Instructions */

#if !H_ATAB
#define H_ATAB 1

#include "limits.h"
//
#include "itab.h"

/* valid assembler instruction size options (must always at least contain 'n') */
static char asizes[] = "nbslq";

static int asize_vals[] = {NATIVE_ALIGN, sizeof(u_char), sizeof(u_small), sizeof(u_int), sizeof(u_big)};

/* assembler instructions table entry */
struct atab {
    const char* a_name;    /* string name of instruction */
    unsigned char a_margs; /* min number of arguments */
    unsigned char a_nargs; /* max number of arguments */
    int a_flag;            /* flags about type of asm */
};

#define ASEC 0x001 /* instruction creates/changes the section */
#define ADEF 0x002 /* instruction creates a definition */
#define ARES 0x004 /* instruction creates a reservation */
#define APOS 0x008 /* instruction changes current position info */
#define ABIN 0x010 /* instruction changes binary format*/
#define ASYM 0x020 /* instruction creates a symbol definition */

#define ASIZ 0x100 /* instruction takes size modifiers */

#define NATAB 48

static const struct atab atab[] = {
  /* define instructions (usually .data or .rodata) */
  {".db", 1, 255, ADEF},         /* define byte (8-bit) */
  {".ds", 1, 255, ADEF},         /* define short (16-bit) */
  {".drw", 1, 255, ADEF},        /* alias for .ds */
  {".dl", 1, 255, ADEF},         /* define long  (32-bit) */
  {".dq", 1, 255, ADEF},         /* define quad (64-bit) */
  {".dc", 1, 255, ADEF},         /* define chars/strings (8-bit aligned, null-terminated character list) */
  {".def", 1, 255, ADEF | ASIZ}, /* generic define, default to alignment but allows .b/.s/.l/.q modifiers */

  /* reserve space instructions (usually .bss) */
  {".rb", 1, 1, ARES},         /* reserve space for X bytes (8-bits) */
  {".rs", 1, 1, ARES},         /* reserve space for X shorts (16-bits) */
  {".rw", 1, 1, ARES},         /* alias for .rs */
  {".rl", 1, 1, ARES},         /* reserve space for X longs (32-bits) */
  {".rq", 1, 1, ARES},         /* reserve space for quads (64-bits) */
  {".rc", 1, 1, ARES},         /* reserve space for X characters (8-bits) */
  {".res", 1, 1, ARES | ASIZ}, /* generic reserve, default to alignment but allows .b/.s/.l/.q modifiers */

  /* binary format instructions */
  {".align", 1, 1, APOS},    /* set alignment to x bits*/
  {".end", 0, 0, APOS},      /* treat this as the end of this logical source file */
  {".equ", 2, 2, ASYM},      /* attach symbol name to value */
  {".file", 0, 1, ASYM},     /* start of new logical source file, with name */
  {".flatpack", 0, 0, ABIN}, /* compile as flat binary and ignore elf sections */
  {".globl", 1, 1, ASYM},    /* add label to global manifest */
  {".org", 1, 1, APOS},      /* set origin (follows alignment as multiplier) */
  {".sec", 1, 1, ASEC},      /* alias for .section */
  {".section", 1, 1, ASEC},  /* section */
  {".type", 1, 2, ASYM},     /* type def for .globl instructions */

  /* Predefined user sections (may be included in asm source) */
  {".bss", 0, 0, ASEC},     /* uninitialised read-write data */
  {".comment", 0, 0, ASEC}, /* comment section */
  {".data", 0, 0, ASEC},    /* initialised read-write data */
  {".data1", 0, 0, ASEC},   /* initialised read-write data (second part) */
  {".fini", 0, 0, ASEC},    /* code to execute at exit */
  {".init", 0, 0, ASEC},    /* code to execute before main */
  {".rodata", 0, 0, ASEC},  /* initialised read-only data */
  {".rodata1", 0, 0, ASEC}, /* initialised read-only data (second part) */
  {".text", 0, 0, ASEC},    /* executable text / machine code */
  {".line", 0, 0, ASEC},    /* line number info for symbolic debugging */
  {".note", 0, 0, ASEC},    /* file notes */

  /* Predefined non-user sections (should not be included in asm source) */
  {".debug", 0, 0, ASEC},    /* debug information */
  {".dynamic", 0, 0, ASEC},  /* dynamic linking information */
  {".dynstr", 0, 0, ASEC},   /* dynamic string linking table */
  {".dynsym", 0, 0, ASEC},   /* dynamic linking symbol table */
  {".got", 0, 0, ASEC},      /* global offset table */
  {".hash", 0, 0, ASEC},     /* symbol hash table */
  {".interp", 0, 0, ASEC},   /* path to interpreter to use to execute file */
  {".plt", 0, 0, ASEC},      /* procedure linking tabßle */
  {".shstrtab", 0, 0, ASEC}, /* section header table names */
  {".strtab", 0, 0, ASEC},   /* string table */
  {".symtab", 0, 0, ASEC},   /* symbol table */
  {".rel", 1, 1, ASEC},      /* relocation information */
  {".rela", 1, 1, ASEC},     /* relocation information */
};

#endif
