LCVM Assembly Codes
===================

LCVM was influenced by ARMv6 and PDP-11, but is not compatible with either. 

This document only outlines the processor operations/instructions, formatting for those in LAS Assembly, and machine code structure.

Please see the document [ASM.md](./ASM.md) for details on the syntax required for LAS.

## non-op reserved words

General purpose "low-side" registers, denoted by ```r0``` to ```r7```, these are used to pass arguments to a call (system or otherwise). r7 is the syscall number argument itself.

General purpose "high-side" registers denoted by ```r8``` to ```r12```, can be used for anything in code, global values to keep between calls, for example.

Special program registers, denoted by ```sp``` for stack pointer, ```pc``` for program counter, ```lr``` for program counter upon return from subroutine.

All registers are 32-bit. Operations which operate at 64-bits will use the passed register as the least significant 32-bits and the register + 1 as the most-significant 32-bits.

## System calls

System calls are performed using the ```call``` instruction, this saves the current program status and position, jumps (via soft interrupt) to the operating system, performs the call, and returns execution back to where the program left.

The low-side registers are used to pass arguments, thus a maximum of 8 32-bit arguments can be passed. Arguments smaller than 32-bits will still take a full 32-bit register, and thus be promoted to 32-bits.

The return value of the system call is placed in register r0. All other low-side registers (regardless of use in the system call) will be wiped, with the exception that r1 may be used if returning a 64-bit value. As mentioned above, the r0 register will contain the least-significant 32-bits, and r1 the most significant 32-bits. As a result, you can test for size of return value by checking the presence of a non-zero value in r1.

## Numbers and sizes

Any token beginning with a numerical value is assumed to be a number. Decimal numbers can be followed by a 'd' or no value (.e.g ```50``` or ```50d```), octal values by an 'o' (```377o```), and hex values by an 'h' (```F4A3h```)

To manually specify the size of the numbers the instruction will operate as, you may follow the instruction with the size separated using a '.', the size is then denoted by the letter b for 8-bit, s for 16-bit, l for 32-bit, and q for 64-bit. e.g. ```add.b  r0, 54h```. If no size is specified, then the number will follow the native size (32-bits)

The ```mov``` operation has 5 different types built into the operation for these sizes, see below. 

## Addresses to Values

To tell the compiler you wish to pass the value at an address you must instead pass the argument wrapped with '[' ']'.
There is an exception with registers, where the braces indicate that you wish to pass the value stored at the address stored in that register as the register itself cannot be a memory address, so we assume all references to a register apply to the contents of that register.

e.g. ```[r0]``` treats the contents of r0 as an address, which it then uses to grab and pass the value of. Alternatively: ```[sp]``` would grab the value stored at the address currently in the stack pointer

This can be used on labels e.g. ```[VAR0]``` would pass the value stored at the address of label 'VAR0'

This can be used on numerical constants, e.g. ```[050h]``` to pass the value at address 0x050.

Each argument can only be passed through the value option once, so to do it twice you will need to push the first pass to the stack and then grab it through ```[sp]```

Operations where both the destination and source are the contents of the stack, will operate directly on the stack in order (a la stack machine style)

## ```clr```

The clear instruction is probably the simplest instruction and simply sets the value at an address to 0. It assumes 32-bit values, and so you may want to ensure you instead use ```movq  ADDRESS, 0``` if you wish to clear a 64-bit value.

#### example:

```
    clr r1 ; clear the register 1
```


## ```mov```

The LCVM Assembly language contains 4 variations of the mov command depending on the size of the data to be moved.
But each follows the same basic structure.

```
    mov  ADDRESS, VALUE
```

The valid mov options are: ```movb``` for 8-bit values, ```movs``` for 16-bit values, ```mov``` or ```movl``` for 32-bit values, and ```movq``` for 64-bit values.

If the address or value are constants, they will be included following the instruction in the instr section of memory. All values are stored as 32-bit aligned little-endian. 

If they refer to registers then these will be included in the 32-bits of instruction.

#### example:

```
    movq  r0, [r2] ; move the value stored at the address in r2 to r0 
```


## ```call```

The call op allows you to call a function from assembly code. Arguments are passed to the called function via the low-side registers. Functions that are written in the assembly must therefore mov the values off the registers, however for external calls (e.g. to the system's printf function), this is handled for you and the call returns with registers cleared.

Registers r0->r6 are used for arguments, with r0 being used for return values and r7 is used for the system call number. If a 64-bit value is passed or returned, then the lower register number will contain the lower 32-bits and the next register will contain the higher 32-bits. 

If the label for the call is found inside the assembly source file(s) then this is created as a link to the memory address of that in the executable, however if it is external then this is linked dynamically at run time. If it is a system call, then the call is given the system call number to use.

The executable contains a manifest of functions (see ".globl") so that separate binary files can be linked together during compile. 

CALL is a shortcut function in that it is functionally identical to the following code snippet but in a single operation:

For system calls:
```
    mov    r7, call_number
    int    sys_call_vector
```

For function calls:

```
   jsr    call_location, pc
```

The assembler will automatically adjust all internal calls to the latter snippet on compile.

#### examples:

```
    ...
    call    printf
    clr     r0
    call    exit
```

The above code calls printf, then clears r0 (the first call argument) and calls exit which ends the program.

## ```push``` and ```pop```

Push and Pop are opposites. Push moves a value onto the current stack position and then increments the stack pointer passed it. Pop moves a value from the stack position and then decrements the pointer to before it.

## ```dec``` and ```inc```

These are the simplest maths options in LCVM ASM, and they decrement and increment by 1 respectively.
