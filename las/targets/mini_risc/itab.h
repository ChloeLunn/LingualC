/*
BSD 3-Clause License

Copyright (c) 2020-2022, Chloe Lunn
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this
   list of conditions and the following disclaimer.

2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution.

3. Neither the name of the copyright holder nor the names of its
   contributors may be used to endorse or promote products derived from
   this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

/* Mini RISC */

#if !H_ITAB
#define H_ITAB 1

#include "las.h"
#include "limits.h"
#include "rtab.h"

static int arch_elf = 0x4D52;

static str arch_name = "MiniRISC";

/* valid size options (must always at least contain 'n') */
static char sizes[] = "nbslq";

#define MIN_ALIGN    (1)
#define MAX_ALIGN    (8)
#define NATIVE_ALIGN (4) /* native alignement is 4-bytes (32-bits) */

typedef unsigned int tag_t; /* type to use when inserting a temporary tag value */

static int size_vals[] = {NATIVE_ALIGN, sizeof(u_char), sizeof(u_small), sizeof(u_int), sizeof(u_big)};

#define MIN_RADIX    (2)
#define MAX_RADIX    (16)
#define NATIVE_RADIX (10) /* native radix to use for numbers */

#define INSTRUCTION_WIDTH (4) /* width of the instructions in bytes */

/* processor instruction table entry */
struct itab {
    const char* i_name;    /* string name of instruction */
    unsigned int i_inst;   /* Base opcode (actual word length) */
    unsigned char i_nargs; /* max number of further 32-bit arguments */
    int i_flag;            /* flags about type of instruction (optional) */
};

/* Op codes for LCVM are in the format 0xFCCLDDSS where:
 *    - F is the op code family
 *    - C is the op code within that family
 *    - L is the op size in bytes (0 = native, 32-bit/4-byte)
 *    - SS is the source register number
 *    - DD the destination register number
 * If the destination register is 7F (i.e. all value bits high), use the next 32-bit value from the memory as the operand
 * If the source register is 7F (i.e. all value bits high), use the next 32-bit value from the memory as the value
 * If the top bit of the destination (DD) is set, take the value in the operand as a pointer to the actual destination data
 * If the top bit of the source (SS) is set, take the value in the operand as a pointer to the actual source data
 * If the operation only requires a single operand, then it is DD and SS is ignored
 */

#define IMATH 0x001 /* maths instruction */
#define ILOG  0x002 /* logical instruction */
#define IVAR  0x004 /* variable control instruction */
#define IBRA  0x008 /* branch instruction */
#define IJMP  0x010 /* jump instruction */
#define ICMP  0x020 /* comparison instruction */
#define IFLT  0x040 /* floating point instruction (Not implemented) */
#define IPRC  0x080 /* processor control instruction */

#define ISIZ 0x100 /* instruction accepts size modifiers */
#define IMAC 0x200 /* instruction may be replaced by macro */
#define IPIC 0x400 /* instruction is position independent code */

#define NITAB 66

static const struct itab itab[NITAB] = {
  /* asm, op code,  nargs, flags */

  /* maths */
  {"add", 0x01000000, 2, IMATH | ISIZ}, /* addition               D = Address, S = value */
  {"sub", 0x02000000, 2, IMATH | ISIZ}, /* subtraction            D = Address, S = value */
  {"div", 0x03000000, 2, IMATH | ISIZ}, /* divide                 D = Address, S = value */
  {"mul", 0x05000000, 2, IMATH | ISIZ}, /* multiply               D = Address, S = value */
  {"asl", 0x06000000, 2, IMATH | ISIZ}, /* arithmetic shift left */
  {"asr", 0x06800000, 2, IMATH | ISIZ}, /* arithmetic shift right */
  {"adc", 0x08000000, 2, IMATH | ISIZ}, /* addition with carry */
  {"sbc", 0x09000000, 2, IMATH | ISIZ}, /* subtraction with carry */

  /* bitwise */
  {"not", 0x0A000000, 1, ILOG},        /* if value is 0, set to 1, if value is !=0 set to 0 */
  {"inv", 0x0A800000, 1, ILOG | ISIZ}, /* bitwise invert */
  {"and", 0x0B00000, 2, ILOG | ISIZ},  /* bitwise AND */
  {"or", 0x0B800000, 2, ILOG | ISIZ},  /* bitwise OR */
  {"xor", 0x0C00000, 2, ILOG | ISIZ},  /* bitwise XOR */
  {"bsl", 0x0D000000, 2, ILOG | ISIZ}, /* bit shift left */
  {"bsr", 0x0D800000, 2, ILOG | ISIZ}, /* bit shift right */

  /* data handling */
  {"mov", 0x10000000, 2, IVAR | ISIZ}, /* move native (32-bit)             D = address, S = value */

  /* Branches */
  {"b", 0x3000000, 1, IBRA | IPIC},     /* branch always                     D = address */
  {"bl", 0x3C00000, 1, IBRA | IPIC},    /* branch with link */
  {"bne", 0x03100000, 1, IBRA | IPIC},  /* branch if not zero                D = address */
  {"beq", 0x03300000, 1, IBRA | IPIC},  /* branch if zero                    D = address */
  {"bge", 0x03400000, 1, IBRA | IPIC},  /* branch if postive or 0 */
  {"blt", 0x03500000, 1, IBRA | IPIC},  /* branch if negative and not 0 */
  {"bgt", 0x03600000, 1, IBRA | IPIC},  /* branch if positive and not 0 */
  {"ble", 0x03700000, 1, IBRA | IPIC},  /* branch if negative or 0 */
  {"bpl", 0x03800000, 1, IBRA | IPIC},  /* branch if positive */
  {"bmi", 0x03900000, 1, IBRA | IPIC},  /* branch if negative */
  {"bcs", 0x03A00000, 1, IBRA | IPIC},  /* branch if carry is set */
  {"bcc", 0x03B00000, 1, IBRA | IPIC},  /* branch if carry is clear */
  {"bvs", 0x03C00000, 1, IBRA | IPIC},  /* branch if overflow is set */
  {"bvc", 0x03D00000, 1, IBRA | IPIC},  /* branch if overflow is clear */
  {"bhi", 0x03E00000, 1, IBRA | IPIC},  /* branch if higher */
  {"blos", 0x03F00000, 1, IBRA | IPIC}, /* branch if lower or same */
  {"blo", 0x03A00000, 1, IBRA | IPIC},  /* branch if lower - alias to bcs */
  {"bhis", 0x03B00000, 1, IBRA | IPIC}, /* branch if higher or same - alias to bcc */

  /* Processor Control */
  {"int", 0xF0000000, 1, IJMP | IPIC}, /* call an interrupt  D = vector */
  {"wfi", 0xF1000000, 0, IPRC | IPIC}, /* wait for interrupt/instruction (do nothing), sets run to 0 */
  {"rst", 0xF2000000, 0, IPRC | IPIC}, /* reset processor */
  {"nop", 0x00000000, 0, IPRC | IPIC}, /* no operation, but valid operation */
};

#define INSTR_BITS  0xFFF00000UL
#define SIZE_BITS   0x000F0000UL
#define DEST_BITS   0x0000FF00UL
#define SOURCE_BITS 0x000000FFUL
#define PTR_BITS    0x00000080UL
#define ARG_BITS    0x0000007FUL
#define STAT_BITS   0x0000000FUL
#define TOP_BIT     0x80000000UL
#define BOTTOM_BIT  0x00000001UL
#define ZERO_BITS   0x00000000UL
#define ALL_BITS    0xFFFFFFFFUL

/*
  returns pointer to a list of int to add to binary (in order), first int is return code, second is length of following array
  null means add nothing
  argument str in = instruction name
  argument str size = requested op size
  argument int argc = number of string arguments being passed
  argument char argv[][] = list of string arguments
*/
int ptr ibuild(str in, str size, int argc, char argv[MAX_NARGS][MAX_ARG_STR])
{
    int ptr res = malloc(1 mul sizeof(int));

    for (int i = 0; i < NITAB; inc i)
    {
        beq(strcmp(in, itab[i].i_name))
        {
            /* if not enough args */
            if (argc < itab[i].i_nargs)
            {
                res[0] = EARGS2SMALL;
                return res;
            }

            /* if too many arguments */
            if (argc > itab[i].i_nargs)
            {
                res[0] = EARGS2BIG;
                return res;
            }

            printf("instr = %s, size = %s\r\n", in, size);

            int na = argc;
            for (int i = 0; i < na; inc i)
            {
                ble(strlen(argv[i]))
                {
                    dec na;
                }
                else printf("arg #%i = %s\r\n", i, argv[i]);
            }

            int ins = itab[i].i_inst;
            int len = 1;

            /* set dest as pointer flag */
            if (strany(argv[0], "["))
            {
                ins = ins or (PTR_BITS << 8);
            }
            /* set source as pointer flag */
            if (strany(argv[1], "]"))
            {
                ins = ins or (PTR_BITS << 0);
            }

            // create_unlinked(argv[0]);

            printf("Result instr = %8x\r\n", ins);

            res = realloc(res, (len + 1) * sizeof(int));
            res[1] = len;

            res[0] = ENOERROR;
            return res;
        }
    }
    return NULL;
}

#endif
