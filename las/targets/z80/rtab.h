/*
BSD 3-Clause License

Copyright (c) 2020-2022, Chloe Lunn
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this
   list of conditions and the following disclaimer.

2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution.

3. Neither the name of the copyright holder nor the names of its
   contributors may be used to endorse or promote products derived from
   this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

/* Z80 Reserved Words */

#if !H_RTAB
#define H_RTAB 1

/* ~~~~~~~~~~~~~~~ General reserved words ~~~~~~~~~~~~~~~ */

struct rword {
    const char* rw_word; /* reserved word */
    int rw_flag;         /* flags */
    int rw_val;          /* value, if appropriate */
};

#define RTYPE 0x001 /* is a type name */
#define RREG  0x002 /* is a register name */
#define RSIZE 0x004 /* is a size name */
#define RFMT  0x008 /* is a special escaped format */

/*
   RESERVED WORD TABLE:
   This table defines special reserved words used during assembly,
   and is used to tell which words to NOT consider as labels, macros,
   definitions, etc.

   The atab and itab contents are also considered reserved words and
   are processed in the same way, so this table is for things like
   register names and types, and the other tables are for instructions.
*/

#define NRTAB 28

struct rword rtab[] = {
  /* 8080 comptatible registers */
  {"a", RREG, 7},  /* A - 8-bit accumulator */
  {"b", RREG, 0},  /* B - 8-bit  */
  {"c", RREG, 1},  /* C - 8-bit */
  {"d", RREG, 2},  /* D - 8-bit */
  {"e", RREG, 3},  /* E - 8-bit */
  {"h", RREG, 4},  /* H - 8-bit */
  {"l", RREG, 5},  /* L - 8-bit */
  {"af", RREG, 0}, /* AF - 16-bit accumulator/flags */
  {"bc", RREG, 0}, /* BC - 16-bit data/address */
  {"de", RREG, 1}, /* DE - 16-bit data/address */
  {"hl", RREG, 2}, /* HL - 16-bit accumulator/address */
  {"sp", RREG, 3}, /* SP - 16-bit Stack Pointer */
  {"pc", RREG, 0}, /* PC - 16-bit Program Counter */

  /* Z80 registers */
  {"ix", RREG, 0}, /* IX - 16-bit index/offset */
  {"iy", RREG, 0}, /* IY - 16-bit index/offset */
  {"i", RREG, 0},  /* I - Interrupt vector register */
  {"r", RREG, 0},  /* R - DRAM refresh counter */

  /* shadow/alternate register set */
  {"$a", RREG, 7},  /* A - 8-bit accumulator */
  {"$b", RREG, 0},  /* B - 8-bit  */
  {"$c", RREG, 1},  /* C - 8-bit */
  {"$d", RREG, 2},  /* D - 8-bit */
  {"$e", RREG, 3},  /* E - 8-bit */
  {"$h", RREG, 4},  /* H - 8-bit */
  {"$l", RREG, 5},  /* L - 8-bit */
  {"$af", RREG, 0}, /* AF - 16-bit accumulator/flags */
  {"$bc", RREG, 0}, /* BC - 16-bit data/address */
  {"$de", RREG, 1}, /* DE - 16-bit data/address */
  {"$hl", RREG, 2}, /* HL - 16-bit accumulator/address */
};

#endif
