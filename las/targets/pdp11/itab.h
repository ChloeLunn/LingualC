/*
BSD 3-Clause License

Copyright (c) 2020-2022, Chloe Lunn
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this
   list of conditions and the following disclaimer.

2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution.

3. Neither the name of the copyright holder nor the names of its
   contributors may be used to endorse or promote products derived from
   this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

/* PDP11 Instructions/Operations for LAS */

#if !H_ITAB
#define H_ITAB 1

#include "las.h"
#include "limits.h"
#include "rtab.h"

static int arch_elf = 0x4949;

static str arch_name = "PDP11";

/* valid size options (must always at least contain 'n') */
static char sizes[] = "nbs";

#define MIN_ALIGN    (1)
#define MAX_ALIGN    (2)
#define NATIVE_ALIGN (2) /* native alignement is 4-bytes (32-bits) */

typedef unsigned short tag_t; /* type to use when inserting a temporary tag value */

static int size_vals[] = {NATIVE_ALIGN, sizeof(u_char), sizeof(u_small)};

#define MIN_RADIX    (2)
#define MAX_RADIX    (16)
#define NATIVE_RADIX (10) /* native radix to use for numbers */

#define INSTRUCTION_WIDTH (2) /* width of the instructions in bytes */

/* processor instruction table entry */
struct itab {
    const char* i_name;    /* string name of instruction */
    unsigned int i_inst;   /* Base opcode (actual word length) */
    unsigned char i_nargs; /* max number of further 32-bit arguments */
    int i_flag;            /* flags about type of instruction (optional) */
};

#define IMATH 0x001 /* maths instruction */
#define ILOG  0x002 /* logical instruction */
#define IVAR  0x004 /* variable control instruction */
#define IBRA  0x008 /* branch instruction */
#define IJMP  0x010 /* jump instruction */
#define ICMP  0x020 /* comparison instruction */
#define IFLT  0x040 /* floating point instruction (Not implemented) */
#define IPRC  0x080 /* processor control instruction */

#define ISIZ 0x100 /* instruction accepts size modifiers */
#define IMAC 0x200 /* instruction may be replaced by macro */
#define IPIC 0x400 /* instruction is position independent code */

#define NITAB 67

static const struct itab itab[] = {
  /* asm, op code,  nargs, flags */

  /* maths */
  {"add", 0060000, 2, IMATH | ISIZ},  /* addition               D = Address, S = value */
  {"sub", 0160000, 2, IMATH | ISIZ},  /* subtraction            D = Address, S = value */
  {"div", 0071000, 2, IMATH | ISIZ},  /* divide                 D = Address, S = value */
  {"mul", 0070000, 2, IMATH | ISIZ},  /* multiply               D = Address, S = value */
  {"inc", 0005200, 1, IMATH | ISIZ},  /* increment by 1         D = Address */
  {"dec", 0005300, 1, IMATH | ISIZ},  /* decrement by 1         D = Address */
  {"ash", 0072000, 2, IMATH | ISIZ},  /* arithmetic shift       D = Address, S = value  */
  {"asl", 0006300, 1, IMATH | ISIZ},  /* arithmetic shift left by 1 */
  {"asr", 0006200, 1, IMATH | ISIZ},  /* arithmetic shift right by 1*/
  {"com", 0005100, 1, IMATH | ISIZ},  /* 1s compliment */
  {"neg", 0005400, 1, IMATH | ISIZ},  /* make neg (2s compliment) */
  {"adc", 0005500, 2, IMATH | ISIZ},  /* addition with carry */
  {"sbc", 0005600, 2, IMATH | ISIZ},  /* subtraction with carry */
  {"swab", 0000300, 1, IMATH | ISIZ}, /* swap bytes (endianness swapper) */

  /* bitwise */
  {"bit", 0030000, 2, ILOG | ISIZ}, /* bitwise AND */
  {"bis", 0050000, 2, ILOG | ISIZ}, /* bitwise OR */
  {"xor", 0074000, 2, ILOG | ISIZ}, /* bitwise XOR */
  {"rol", 0006100, 2, ILOG | ISIZ}, /* bit shift left */
  {"ror", 0006000, 2, ILOG | ISIZ}, /* bit shift right */
  {"bic", 0040000, 2, ILOG | ISIZ}, /* bit clear (AND ~VAL)*/

  /* data handling */
  {"clr", 0005000, 1, IVAR | ISIZ}, /* clear                            D = address */
  {"mov", 0110000, 2, IVAR | ISIZ}, /* move native (32-bit)             D = address, S = value */
  {"mtpi", 0006600, 2, IVAR},       /* move byte (8-bit) */
  {"mfpi", 0006500, 2, IVAR},       /* move short (16-bit) */
  {"mtpd", 0106600, 2, IVAR},       /* move long (32-bit) */
  {"mfpd", 0106500, 2, IVAR},       /* move quad (64-bit) - puts lsb in argument, and msb in argument + 32-bits */
  /* Branches */
  {"br", 0000400, 1, IBRA | IPIC},   /* branch always                     D = address */
  {"bne", 0001000, 1, IBRA | IPIC},  /* branch if not zero                D = address */
  {"beq", 0001400, 1, IBRA | IPIC},  /* branch if zero                    D = address */
  {"bge", 0002000, 1, IBRA | IPIC},  /* branch if postive or 0 */
  {"blt", 0002400, 1, IBRA | IPIC},  /* branch if negative and not 0 */
  {"bgt", 0003000, 1, IBRA | IPIC},  /* branch if positive and not 0 */
  {"ble", 0003400, 1, IBRA | IPIC},  /* branch if negative or 0 */
  {"bpl", 0100000, 1, IBRA | IPIC},  /* branch if positive */
  {"bmi", 0100400, 1, IBRA | IPIC},  /* branch if negative */
  {"bcs", 0103400, 1, IBRA | IPIC},  /* branch if carry is set */
  {"bcc", 0103000, 1, IBRA | IPIC},  /* branch if carry is clear */
  {"bvs", 0102400, 1, IBRA | IPIC},  /* branch if overflow is set */
  {"bvc", 0102000, 1, IBRA | IPIC},  /* branch if overflow is clear */
  {"bhi", 0101000, 1, IBRA | IPIC},  /* branch if higher */
  {"blos", 0101400, 1, IBRA | IPIC}, /* branch if lower or same */
  {"blo", 0103400, 1, IBRA | IPIC},  /* branch if lower - alias to bcs */
  {"bhis", 0103000, 1, IBRA | IPIC}, /* branch if higher or same - alias to bcc */

  /* Jumps */
  {"jmp", 0000100, 1, IJMP}, /* jump to anywhere */
  {"jsr", 0004000, 2, IJMP}, /* jump to subroutine */
  {"rts", 0000200, 1, IJMP}, /* return to subroutine */
  {"spl", 0000230, 1, IJMP}, /* return to subroutine */

  /* External calls */
  {"emt", 0104000, 1, IJMP | IPIC},  /* call an interrupt  D = vector */
  {"iot", 0000004, 1, IJMP | IPIC},  /* call an interrupt  D = vector */
  {"bpt", 0000003, 1, IJMP | IPIC},  /* call an interrupt  D = vector */
  {"trap", 0104400, 1, IJMP | IPIC}, /* call an interrupt  D = vector */

  /* Comparisions */
  {"cmp", 0020000, 2, ICMP | ISIZ | IPIC}, /* compare two values    D = Value, S = Value */
  {"tst", 0005700, 1, ICMP | ISIZ | IPIC}, /* test a value  D = Value */

  /* Processor Control */
  {"wait", 0000001, 0, IPRC | IPIC},       /* wait for interrupt/instruction (do nothing), sets run to 0 */
  {"reset", 0000005, 0, IPRC | IPIC},      /* reset processor */
  {"rti", 0000002, 0, IPRC | IPIC | IJMP}, /* return from execution */
  {"rtt", 0000006, 0, IPRC | IPIC | IJMP}, /* return from execution */
  {"halt", 0000000, 0, IPRC | IPIC},       /* halt processor */
  {"mfpt", 0000007, 0, IPRC | IPIC},       /* halt processor */
  {"nop", 0000240, 0, IPRC | IPIC},        /* no operation, but valid operation */
};

#define INSTR_BITS  0xFFF00000UL
#define SIZE_BITS   0x000F0000UL
#define DEST_BITS   0x0000FF00UL
#define SOURCE_BITS 0x000000FFUL
#define PTR_BITS    0x00000080UL
#define ARG_BITS    0x0000007FUL
#define STAT_BITS   0x0000000FUL
#define TOP_BIT     0x80000000UL
#define BOTTOM_BIT  0x00000001UL
#define ZERO_BITS   0x00000000UL
#define ALL_BITS    0xFFFFFFFFUL

int ibuild(str in, str size, int argc, char argv[MAX_NARGS][MAX_ARG_STR])
{
    // int calced_size = size_to_int(size, sizes, size_vals);

    // for (int i = 0; i < NITAB; inc i)
    // {
    //     beq(strcmp(in, itab[i].i_name))
    //     {
    //         /* if not enough args */
    //         if (argc < itab[i].i_nargs)
    //         {
    //             return EARGS2SMALL;
    //         }

    //         /* if too many arguments */
    //         if (argc > itab[i].i_nargs)
    //         {
    //             return EARGS2BIG;
    //         }

    //         /* copy in base instruction format */
    //         int ins = itab[i].i_inst;

    //         /* copy in the size */
    //         ins = ins or ((calced_size << 16) & SIZE_BITS);

    //         int a1set = 0;
    //         big a1;

    //         int a2set = 0;
    //         big a2;

    //         /* ~~~~~ Destination / Arg 1 Set ~~~~~ */

    //         if (itab[i].i_nargs >= 1)
    //         {
    //             /* set dest as pointer flag */
    //             if (strany(argv[0], "["))
    //             {
    //                 ins = ins or (PTR_BITS << 8);
    //             }
    //             /* if this starts with a digit, then handle as a number */
    //             if (isdigit(argv[0][0]))
    //             {
    //                 str end = NULL;
    //                 a1 = strtoll(argv[1], adr end, str_to_radix(argv[1]));
    //                 a1set = 1;
    //                 ins = ins or (ARG_BITS << 8);
    //             }
    //             else
    //             {
    //                 /* check for a reserved word */
    //                 for (int r = 0; r < NRTAB; r++)
    //                 {
    //                     /* found */
    //                     beq(strcmp(rtab[r].rw_word, argv[0]))
    //                     {
    //                         /* is a register */
    //                         if (rtab[r].rw_flag == RREG)
    //                         {
    //                             ins = ins or ((rtab[r].rw_val & ARG_BITS) << 8);
    //                             a1set = 3;
    //                             break;
    //                         }
    //                         return EBADINSTR;
    //                     }
    //                 }
    //                 /* if we didn't find it, flag it as unlinked */
    //                 beq(a1set)
    //                 {
    //                     a1set = 4;
    //                     ins = ins or (ARG_BITS << 8);
    //                 }
    //             }
    //         }

    //         /* ~~~~~ Source / Arg 2 Set ~~~~~ */

    //         if (itab[i].i_nargs >= 2)
    //         {
    //             /* set source as pointer flag */
    //             if (strany(argv[1], "]"))
    //             {
    //                 ins = ins or (PTR_BITS << 0);
    //             }
    //             /* if this starts with a digit, then handle as a number */
    //             if (isdigit(argv[1][0]))
    //             {
    //                 str end = NULL;
    //                 a2 = strtoll(argv[1], adr end, str_to_radix(argv[1]));
    //                 a2set = 1;
    //                 ins = ins or (ARG_BITS << 0);
    //             }
    //             else
    //             {
    //                 /* check for a reserved word */
    //                 for (int r = 0; r < NRTAB; r++)
    //                 {
    //                     /* found */
    //                     beq(strcmp(rtab[r].rw_word, argv[1]))
    //                     {
    //                         /* is a register */
    //                         if (rtab[r].rw_flag == RREG)
    //                         {
    //                             ins = ins or ((rtab[r].rw_val & ARG_BITS) << 0);
    //                             a2set = 3;
    //                             break;
    //                         }
    //                         return EBADINSTR;
    //                     }
    //                 }
    //                 /* if we didn't find it, flag it as unlinked */
    //                 beq(a2set)
    //                 {
    //                     a2set = 4;
    //                     ins = ins or (ARG_BITS << 0);
    //                 }
    //             }
    //         }

    //         /* deposit the instruction */
    //         deposit(INSTRUCTION_WIDTH, ins);

    //         /* ~~~~~ Destination / Arg 1 Deposit ~~~~~ */

    //         if (itab[i].i_nargs >= 1)
    //         {
    //             /* deposit dest arg 1 set*/
    //             if (a1set == 4)
    //             {
    //                 /* create and unlinked label, and then reserve it's space with 0s */
    //                 create_unlinked(argv[0], calced_size);
    //                 deposit(calced_size, 0);
    //             }
    //             else if (a1set == 1)
    //             {
    //                 deposit(calced_size, a1);
    //             }
    //         }

    //         /* ~~~~~ Source / Arg 2 Deposit ~~~~~ */

    //         if (itab[i].i_nargs >= 2)
    //         {
    //             if (a2set == 4)
    //             {
    //                 /* create and unlinked label, and then reserve it's space with 0s */
    //                 create_unlinked(argv[1], calced_size);
    //                 deposit(calced_size, 0);
    //             }
    //             else if (a2set == 1)
    //             {
    //                 deposit(calced_size, a2);
    //             }
    //         }
    //         return ENOERROR;
    //     }
    // }
    return EBADINSTR;
}

#endif
