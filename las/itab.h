/*
BSD 3-Clause License

Copyright (c) 2020-2022, Chloe Lunn
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this
   list of conditions and the following disclaimer.

2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution.

3. Neither the name of the copyright holder nor the names of its
   contributors may be used to endorse or promote products derived from
   this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

/* LCVM Instructions/Operations */

#if !H_ITAB
#define H_ITAB 1

#include "las.h"
#include "limits.h"
#include "rtab.h"

static int arch_elf = 0x4C43;

static str arch_name = "LCVM";

/* valid size options (must always at least contain 'n') */
static char sizes[] = "nbslq";

#define MIN_ALIGN    (1)
#define MAX_ALIGN    (8)
#define NATIVE_ALIGN (4) /* native alignement is 4-bytes (32-bits) */

typedef unsigned int tag_t; /* type to use when inserting a temporary tag value */

static int size_vals[] = {NATIVE_ALIGN, sizeof(u_char), sizeof(u_small), sizeof(u_int), sizeof(u_big)};

#define MIN_RADIX    (2)
#define MAX_RADIX    (16)
#define NATIVE_RADIX (10) /* native radix to use for numbers */

#define INSTRUCTION_WIDTH (4) /* width of the instructions in bytes */

/* processor instruction table entry */
struct itab {
    const char* i_name;    /* string name of instruction */
    unsigned int i_inst;   /* Base opcode (actual word length) */
    unsigned char i_nargs; /* max number of further 32-bit arguments */
    int i_flag;            /* flags about type of instruction (optional) */
};

/* Op codes for LCVM are in the format 0x0FCLDDSS where:
 *    - F is the op code family
 *    - C is the op code within that family
 *    - L is the op size in bytes (0 = native, 32-bit/4-byte)
 *    - SS is the source register number
 *    - DD the destination register number
 * If the first nibble is not 0, then that means the instruction is invalid and the processor will panic
 * If the destination register is 7F (i.e. all value bits high), use the next 32-bit value from the memory as the operand
 * If the source register is 7F (i.e. all value bits high), use the next 32-bit value from the memory as the value
 * If the top bit of the destination (DD) is set, take the value in the operand as a pointer to the actual destination data
 * If the top bit of the source (SS) is set, take the value in the operand as a pointer to the actual source data
 * If the operation only requires a single operand, then it is DD and SS is ignored
 */

#define IMATH 0x001 /* maths instruction */
#define ILOG  0x002 /* logical instruction */
#define IVAR  0x004 /* variable control instruction */
#define IBRA  0x008 /* branch instruction */
#define IJMP  0x010 /* jump instruction */
#define ICMP  0x020 /* comparison instruction */
#define IFLT  0x040 /* floating point instruction (Not implemented) */
#define IPRC  0x080 /* processor control instruction */

#define ISIZ 0x100 /* instruction accepts size modifiers */
#define IMAC 0x200 /* instruction may be replaced by macro */
#define IPIC 0x400 /* instruction is position independent code */

#define NITAB 67

static const struct itab itab[] = {
  /* asm, op code,  nargs, flags */

  /* maths */
  {"add", 0x00100000, 2, IMATH | ISIZ}, /* addition               D = Address, S = value */
  {"sub", 0x00200000, 2, IMATH | ISIZ}, /* subtraction            D = Address, S = value */
  {"div", 0x00300000, 2, IMATH | ISIZ}, /* divide                 D = Address, S = value */
  {"mod", 0x00400000, 2, IMATH | ISIZ}, /* modulo                 D = Address, S = value */
  {"mul", 0x00500000, 2, IMATH | ISIZ}, /* multiply               D = Address, S = value */
  {"inc", 0x00600000, 1, IMATH | ISIZ}, /* increment by 1         D = Address */
  {"dec", 0x00700000, 1, IMATH | ISIZ}, /* decrement by 1         D = Address */
  {"ash", 0x00800000, 2, IMATH | ISIZ}, /* arithmetic shift       D = Address, S = value  */
  {"asl", 0x00900000, 1, IMATH | ISIZ}, /* arithmetic shift left by 1 */
  {"asr", 0x00A00000, 1, IMATH | ISIZ}, /* arithmetic shift right by 1*/
  {"com", 0x00B00000, 1, IMATH | ISIZ}, /* 1s compliment */
  {"neg", 0x00C00000, 1, IMATH | ISIZ}, /* make neg (2s compliment) */
  {"adc", 0x00D00000, 2, IMATH | ISIZ}, /* addition with carry */
  {"sbc", 0x00E00000, 2, IMATH | ISIZ}, /* subtraction with carry */
  {"swb", 0x00F00000, 1, IMATH | ISIZ}, /* swap bytes (endianness swapper) */

  /* bitwise */
  {"not", 0x01000000, 1, ILOG},        /* if value is 0, set to 1, if value is !=0 set to 0 */
  {"inv", 0x01100000, 1, ILOG | ISIZ}, /* bitwise invert */
  {"bit", 0x01200000, 2, ILOG | ISIZ}, /* bitwise AND */
  {"bis", 0x01300000, 2, ILOG | ISIZ}, /* bitwise OR */
  {"xor", 0x01400000, 2, ILOG | ISIZ}, /* bitwise XOR */
  {"bsl", 0x01500000, 2, ILOG | ISIZ}, /* bit shift left */
  {"bsr", 0x01600000, 2, ILOG | ISIZ}, /* bit shift right */
  {"bic", 0x01700000, 2, ILOG | ISIZ}, /* bit clear (AND ~VAL)*/

  /* data handling */
  {"clr", 0x02000000, 1, IVAR | ISIZ},  /* clear                            D = address */
  {"mov", 0x02100000, 2, IVAR | ISIZ},  /* move native (32-bit)             D = address, S = value */
  {"movb", 0x02200000, 2, IVAR},        /* move byte (8-bit) */
  {"movs", 0x02300000, 2, IVAR},        /* move short (16-bit) */
  {"movl", 0x02400000, 2, IVAR},        /* move long (32-bit) */
  {"movq", 0x02500000, 2, IVAR},        /* move quad (64-bit) - puts lsb in argument, and msb in argument + 32-bits */
  {"push", 0x02600000, 1, IVAR | ISIZ}, /* push value to stack              D = value */
  {"pop", 0x02700000, 1, IVAR | ISIZ},  /* pop value from stack             D = address */

  /* Branches */
  {"br", 0x03000000, 1, IBRA | IPIC},   /* branch always                     D = address */
  {"b", 0x03000000, 1, IBRA | IPIC},    /* branch always (arm-like alias)    D = address */
  {"bl", 0x03000000, 1, IBRA | IPIC},   /* branch always (arm-like alias)    D = address */
  {"bne", 0x03100000, 1, IBRA | IPIC},  /* branch if not zero                D = address */
  {"beq", 0x03300000, 1, IBRA | IPIC},  /* branch if zero                    D = address */
  {"bge", 0x03400000, 1, IBRA | IPIC},  /* branch if postive or 0 */
  {"blt", 0x03500000, 1, IBRA | IPIC},  /* branch if negative and not 0 */
  {"bgt", 0x03600000, 1, IBRA | IPIC},  /* branch if positive and not 0 */
  {"ble", 0x03700000, 1, IBRA | IPIC},  /* branch if negative or 0 */
  {"bpl", 0x03800000, 1, IBRA | IPIC},  /* branch if positive */
  {"bmi", 0x03900000, 1, IBRA | IPIC},  /* branch if negative */
  {"bcs", 0x03A00000, 1, IBRA | IPIC},  /* branch if carry is set */
  {"bcc", 0x03B00000, 1, IBRA | IPIC},  /* branch if carry is clear */
  {"bvs", 0x03C00000, 1, IBRA | IPIC},  /* branch if overflow is set */
  {"bvc", 0x03D00000, 1, IBRA | IPIC},  /* branch if overflow is clear */
  {"bhi", 0x03E00000, 1, IBRA | IPIC},  /* branch if higher */
  {"blos", 0x03F00000, 1, IBRA | IPIC}, /* branch if lower or same */
  {"blo", 0x03A00000, 1, IBRA | IPIC},  /* branch if lower - alias to bcs */
  {"bhis", 0x03B00000, 1, IBRA | IPIC}, /* branch if higher or same - alias to bcc */

  /* Jumps */
  {"jmp", 0x04000000, 1, IJMP}, /* jump to anywhere */
  {"jsr", 0x04100000, 2, IJMP}, /* jump to subroutine */
  {"rts", 0x04200000, 1, IJMP}, /* return to subroutine */

  /* External calls */
  {"call", 0x05000000, 1, IJMP | IMAC | IPIC}, /* call a function, D = func num */
  {"int", 0x05100000, 1, IJMP | IPIC},         /* call an interrupt  D = vector */

  /* Comparisions */
  {"cmp", 0x06000000, 2, ICMP | ISIZ | IPIC}, /* compare two values    D = Value, S = Value */
  {"tst", 0x06100000, 1, ICMP | ISIZ | IPIC}, /* test a value  D = Value */

  /* Floating points */
  {"setd", 0x07000000, 0, IFLT | ISIZ}, /* set double mode */
  {"setf", 0x07100000, 0, IFLT | ISIZ}, /* set float mode */
  {"movf", 0x07200000, 2, IFLT | ISIZ}, /* move float (assumes registers 0-4 are floating point registers 0-4 */
  {"ftoi", 0x07300000, 2, IFLT | ISIZ},

  /* Processor Control */
  {"wfi", 0x00000000, 0, IPRC | IPIC},        /* wait for interrupt/instruction (do nothing), sets run to 0 */
  {"rst", 0x0FB00000, 0, IPRC | IPIC},        /* reset processor */
  {"ret", 0x0FC00000, 0, IPRC | IPIC | IJMP}, /* return from execution */
  {"halt", 0x0FD00000, 0, IPRC | IPIC},       /* halt processor */
  {"unop", 0x0FE00000, 0, IPRC | IPIC},       /* no operation or invalid operation */
  {"nop", 0x0FF00000, 0, IPRC | IPIC},        /* no operation, but valid operation */
};

#define INSTR_BITS  0xFFF00000UL
#define SIZE_BITS   0x000F0000UL
#define DEST_BITS   0x0000FF00UL
#define SOURCE_BITS 0x000000FFUL
#define PTR_BITS    0x00000080UL
#define ARG_BITS    0x0000007FUL
#define STAT_BITS   0x0000000FUL
#define TOP_BIT     0x80000000UL
#define BOTTOM_BIT  0x00000001UL
#define ZERO_BITS   0x00000000UL
#define ALL_BITS    0xFFFFFFFFUL

int ibuild(str in, str size, int argc, char argv[MAX_NARGS][MAX_ARG_STR])
{
    int calced_size = size_to_int(size, sizes, size_vals);

    for (int i = 0; i < NITAB; inc i)
    {
        beq(strcmp(in, itab[i].i_name))
        {
            /* if not enough args */
            if (argc < itab[i].i_nargs)
            {
                return EARGS2SMALL;
            }

            /* if too many arguments */
            if (argc > itab[i].i_nargs)
            {
                return EARGS2BIG;
            }

            /* copy in base instruction format */
            int ins = itab[i].i_inst;

            /* copy in the size */
            ins = ins or ((calced_size << 16) & SIZE_BITS);

            int a1set = 0;
            big a1;

            int a2set = 0;
            big a2;

            /* ~~~~~ Destination / Arg 1 Set ~~~~~ */

            if (itab[i].i_nargs >= 1)
            {
                /* set dest as pointer flag */
                if (strany(argv[0], "["))
                {
                    ins = ins or (PTR_BITS << 8);
                }
                /* if this starts with a digit, then handle as a number */
                if (isdigit(argv[0][0]))
                {
                    str end = NULL;
                    a1 = strtoll(argv[1], adr end, str_to_radix(argv[1]));
                    a1set = 1;
                    ins = ins or (ARG_BITS << 8);
                }
                else
                {
                    /* check for a reserved word */
                    for (int r = 0; r < NRTAB; r++)
                    {
                        /* found */
                        beq(strcmp(rtab[r].rw_word, argv[0]))
                        {
                            /* is a register */
                            if (rtab[r].rw_flag == RREG)
                            {
                                ins = ins or ((rtab[r].rw_val & ARG_BITS) << 8);
                                a1set = 3;
                                break;
                            }
                            return EBADINSTR;
                        }
                    }
                    /* if we didn't find it, flag it as unlinked */
                    beq(a1set)
                    {
                        a1set = 4;
                        ins = ins or (ARG_BITS << 8);
                    }
                }
            }

            /* ~~~~~ Source / Arg 2 Set ~~~~~ */

            if (itab[i].i_nargs >= 2)
            {
                /* set source as pointer flag */
                if (strany(argv[1], "]"))
                {
                    ins = ins or (PTR_BITS << 0);
                }
                /* if this starts with a digit, then handle as a number */
                if (isdigit(argv[1][0]))
                {
                    str end = NULL;
                    a2 = strtoll(argv[1], adr end, str_to_radix(argv[1]));
                    a2set = 1;
                    ins = ins or (ARG_BITS << 0);
                }
                else
                {
                    /* check for a reserved word */
                    for (int r = 0; r < NRTAB; r++)
                    {
                        /* found */
                        beq(strcmp(rtab[r].rw_word, argv[1]))
                        {
                            /* is a register */
                            if (rtab[r].rw_flag == RREG)
                            {
                                ins = ins or ((rtab[r].rw_val & ARG_BITS) << 0);
                                a2set = 3;
                                break;
                            }
                            return EBADINSTR;
                        }
                    }
                    /* if we didn't find it, flag it as unlinked */
                    beq(a2set)
                    {
                        a2set = 4;
                        ins = ins or (ARG_BITS << 0);
                    }
                }
            }

            /* deposit the instruction */
            deposit(INSTRUCTION_WIDTH, ins);

            /* ~~~~~ Destination / Arg 1 Deposit ~~~~~ */

            if (itab[i].i_nargs >= 1)
            {
                /* deposit dest arg 1 set*/
                if (a1set == 4)
                {
                    /* create and unlinked label, and then reserve it's space with 0s */
                    create_unlinked(argv[0], calced_size);
                    deposit(calced_size, 0);
                }
                else if (a1set == 1)
                {
                    deposit(calced_size, a1);
                }
            }

            /* ~~~~~ Source / Arg 2 Deposit ~~~~~ */

            if (itab[i].i_nargs >= 2)
            {
                if (a2set == 4)
                {
                    /* create and unlinked label, and then reserve it's space with 0s */
                    create_unlinked(argv[1], calced_size);
                    deposit(calced_size, 0);
                }
                else if (a2set == 1)
                {
                    deposit(calced_size, a2);
                }
            }
            return ENOERROR;
        }
    }
    return EBADINSTR;
}

#endif
