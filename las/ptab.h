/*
BSD 3-Clause License

Copyright (c) 2020-2022, Chloe Lunn
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this
   list of conditions and the following disclaimer.

2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution.

3. Neither the name of the copyright holder nor the names of its
   contributors may be used to endorse or promote products derived from
   this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

/* LCVM internal structs and parsing parts */

#if !H_PTAB
#define H_PTAB 1

/* current binary position data */
struct bin_pos {
    int bp_line;   /* source code line */
    int bp_secidx; /* current section index */
    int bp_offidx; /* current offset in section index */
    int bp_align;  /* current alignment */
};

/* ~~~~~~~~~~~~~~~ Parsing information ~~~~~~~~~~~~~~~ */

/* item in line delimiter characters */
static char delimeters[] = ", \t";

/* line delimiter chars */
static char newlines[] = "\n";

/* characters to always ignore */
static char ignores[] = "\r";

/* escape characters */
static char escapes[] = "\\";

/* non-ignored, non-newline, white space chars */
static char whitespaces[] = " \t";

/* non-alphanum tokens allowed in arguments */
static char atokens[] = "[]_.@%#$";

/* non-alphanum tokens allowed in instructions */
static char itokens[] = "_.";

/* non-alphanum tokens allowed when in quotes or brackets */
static char qtokens[] = "()\"\'+-*/";

/* size split tokens */
static char stokens[] = ".";

/* label postfix tokens */
static char ltokens[] = ":";

/* ~~~~~~~~~~~~~~~ Comment options ~~~~~~~~~~~~~~~ */

static char comments[] = ";#";   /* regular comments */
static char ucomments[] = ";#/"; /* comments inc. UNIX single '/' comments */
static char acomments[] = "@";   /* special assembly comments */

/* LAS also supports C-style comments, but these aren't customisable */

/* ~~~~~~~~~~~~~~~ Parsing Structs ~~~~~~~~~~~~~~~ */

/* section information */
struct section {
    int s_idx;                /* index of section in final elf */
    char s_name[MAX_ARG_STR]; /* section name */
    unsigned char* s_data;    /* pointer to this section's data */
    int s_size;               /* current size of section */
};

/* location of a label definition */
struct label_def {
    int l_secidx;              /* index of section this label is in */
    int l_offidx;              /* offset of this label in the section */
    char l_tag[MAX_LABEL_STR]; /* label tag/name */
};

/* location of an unlinked value */
struct unlinked {
    int u_linked;
    int u_secidx;              /* index of section this label is in */
    int u_offidx;              /* offset of this label in the section */
    int u_size;                /* size of space to add unlinked */
    char u_tag[MAX_LABEL_STR]; /* label tag/name */
};

/* contents of a symbol definition */
struct symbol_def {
    int s_secidx;              /* index of section this symbol is in */
    int s_offidx;              /* offset of this symbol in section */
    char s_arg[MAX_LABEL_STR]; /* value of symbol (string) */
    int s_val;                 /* value of symbol (number) */
    char s_type[MAX_ARG_STR];  /* type of symbol */
};

#endif
