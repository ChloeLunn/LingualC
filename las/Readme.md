LAS - The Lingual Assembler
===========================

Portability:
------------

LAS is the Lingual Assembler, originally designed for use as part of the compilation of Lingual C, the focus of the project has shifted slightly so that now part of the aim is to ensure that you can rebuild LAS to target different platforms with varying ease.

Whilst the assembler format is designed to be the same between platforms, the supported instructions and functions to build those instructions into unique formats is built into the design using the itab (instructions table).

To port the code to a different platform, you should only require to create new itab, rtab, and syscall files that target your platform.


Binary Format:
--------------

LAS is primarily designed to produce Little-endian, 2s compliement, ELF binary files; but a.out support may be added at some point.

There is an assembly instruction (.flatpack) which will tell the assembler to output the file as a flat binary and ignore section control instructions.
If no section control assembly instructions are given at all, then the default output will be a flat binary.

The current project will produce Position *Dependent* code, however support for PIC may be added at some point.

LCVM:
-----

The version of itab and rtab (and thus las) in this folder targets the LCVM: Lingual C Virtual Machine.

This is a simple virtualised 32-bit processor that can run userspace applications on any platform the LCVM is built for.

The LCVM is mono-threaded and as such does not require PIC for loading into different address spaces, hence the lack of support for PIC in this assembler.

The full spec for LCVM instructions/operations can be found in [LCVM.md](./LCVM.md)

Assembler process:
------------------

The LAS is for most simple programs self-contained and operates its own simple pre-processor, linker, and binary output generator. 

For more complicated programs other linking or pre-processing may be required and this is handled by other programs. Please see [ASM.md](./ASM.md) for more information.

Assembly Syntax:
----------------

The assembler syntax is based on a hybrid between ARM and PDP-11 (MACRO-11) assembly, with simplifications to improve parsing and code management.

White-space is used to separate terms, commas are ignored but are useful for reading. 

To ignore white-space you can use brackets `(` `)`, quotes `"`, and apostrophes `'`. 

All instructions must be on their own line. You cannot ignore newlines using the above.

The full spec for assembly syntax can be found in [ASM.md](./ASM.md)

Generally speaking there are three main 'families' of instruction:

### Assembler Instructions

These start with a "." prefix and are for controlling the assembler and binary format. E.g. `.org`, `.db`, or `.section`

### Processor Operations

These are your processor instructions like MOV and ADD. 

Operations with two (or more) operands must be in the order Destination, Source. 

Operands may be reserved words (typically register names), number values (decimal, hex, and octal are supported), label names, or system call or library function names.

The full spec for LCVM instructions/operations can be found in [LCVM.md](./LCVM.md)

### Label definitions

A label must be the first thing on a line and is followed with a `:` to indicate that it should be taken as a label.

A label is a way to mark an address (typically a variable or position in code) using a friendly name and does not stay in the final binary file unless unlinked or manually added to the manifest using an instruction such as .globl

The text name of a label is sometimes called a tag.
