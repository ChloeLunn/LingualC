
# Lingual C

Lingual C (aka LC) is a simplified C-like programming language that changes some compiler-handled C components for explicit control, and changes some symbol-based tokens for lingual/literal English words.

It's a bit like someone blurred the lines between C and assembly. Oh wait, I'm someone...

Lingual C was designed for easy compilation either to regular C, or to the Lingual C virtual machine (LCVM) assembly language.

This project includes a LC virtual machine, LCC compiler for that virtual machine, LAS assembler, and an LC to C translator along with some example projects.

Until the LCC compiler and LAS assembler are finished, you can compile with a c compiler by including the lctoc header. We suggest you guard it so that when the LCC compiler is finished it will be an easy transition.

```
#ifndef __lingualc
#include "lctoc.h"
#endif
```

The lctoc header also defines the ```__lingualc``` pre-processor flag.

Currently this is how the .lc files that make up this project are compiled, but the goal is to become self-hosting/compiling.

## Usage

LC is a bit like C-- (Cmm / C Minus Minus) but takes more inspiration from the B programming language and K&R C.

Because of this, the programmer has more control over where things come from or should be stored over regular C where the compiler handles that in the background.

Like B, all variables are assumed to be globals unless defined as local

### Reserved words/tokens/types:

#### Types:

  * char (8-bit number)
  * small (16-bit number)
  * int (native-width number - likely 32-bit)
  * big (double-native-width number - likely 64-bit)
  * function (not a reserved word, just a type)
  * ptr (pointer to [adr of value in var])
  * str (ptr to a string literal (char* in c))

Note that there are no structs. If you use the C compiler to compile Lingual C trick, then you may be able to use these, but they're not part of the spec.

An unsigned version of the above is done in the BSD style of ```u_...``` e.g. ```u_int``` for unsigned int. Note that pointers and strings cannot be unsigned.

#### Type modifiers:

  * register (suggest this number should be in a virtual CPU register)
  * local (variable should be on stack and only lasts as long as code block)
  * ext (thing is in some other file)
  * lib (thing is in a library to be linked, a bit like extern)
  * sys (thing is part of the operating system, a bit like extern)
  * adr (address of [val stored in var])
  * val (value of [var stored at adr in ptr])

#### Reserved words:

  * true
  * false
  * if
  * else
  * endif 
    * Unlike some languages you do not have to place an endif at the end of your if, however the explicit labeling of the end may help speed up compiler optimisation.
  * while
  * return
  * break

Note the lack of switches, do-whiles, and fors. If you use the C compiler to compile Lingual C trick, then you may be able to use these, but they're not part of the spec.

#### Operators:

Same as C, except for pointers which use the type modifiers above.
Some operators may also be spelled out in English:

e.g. add (+), equals (==), assign (=), not (!), inv (~), sub (-), div (/), mod (%), mul (*), and (&), or (|), xor (^), etc.

Please note that the LC to C translator does not handle these properly in comparisons, you will need to take this into account.

There are also explicit operators for branches (ifs) such as bne (branch not equal to 0), and ble (branch less-than or equal to 0)

You would use them like so:

```
bne(var)
{
  ...
}
```

which is functionally identical to 

```
if(var != 0)
{
  ...
}
```

But because of its explicit hinting, it allows the compiler to optimise easier.

There are also two-operator versions like blo for branch lower than, which is used like so:

```
blo(var1, var2)
{
  ...
}
```

Where the code block is only executed if var1 is lower and not equal to var2, functionally identical to:

```
if (var1 < var2)
{
  ...
}
```
